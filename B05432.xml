<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B05432">
 <teiHeader xml:id="B05432-B05432-header">
  <fileDesc>
   <titleStmt>
    <title>A proclamation adjourning the Parliament from the eighteenth day of April, to the ninth day of May 1695.</title>
    <author>Scotland. Privy Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B05432 of text R183308 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing S1565). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B05432</idno>
    <idno type="STC">Wing S1565</idno>
    <idno type="STC">ESTC R183308</idno>
    <idno type="EEBO-CITATION">52529237</idno>
    <idno type="OCLC">ocm 52529237</idno>
    <idno type="VID">178963</idno>
    <idno type="PROQUESTGOID">2240860494</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. B05432)</note>
    <note>Transcribed from: (Early English Books Online ; image set 178963)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2775:34)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A proclamation adjourning the Parliament from the eighteenth day of April, to the ninth day of May 1695.</title>
      <author>Scotland. Privy Council.</author>
      <author>Scotland. Sovereign (1694-1702 : William II)</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by the heirs and successors of Andrew Anderson, Printer to his most excellent Majesty,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>Anno Dom. 1695.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Initial letter.</note>
      <note>Signed at end: Gilb. Eliot, Cls. Sti. Concilii.</note>
      <note>Dated at end: Given under Our Signet at Edinburgh, the seventeenth day of April, and of Our Reign the seventh year, 1695.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Scotland. -- Parliament -- Early works to 1800.</term>
     <term>Scotland -- Politics and government -- 1689-1745 -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A proclamation : adjourning the Parliament from the eighteenth day of April, to the ninth day of May 1695.</ep:title>
    <ep:author>Scotland. Privy Council</ep:author>
    <ep:publicationYear>1695</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>441</ep:wordCount>
    <ep:defectiveTokenCount>1</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>22.68</ep:defectRate>
    <ep:finalGrade>C</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 22.68 defects per 10,000 words puts this text in the C category of texts with between 10 and 35 defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-10</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-12</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2009-02</date>
    <label>Scott Lepisto</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-02</date>
    <label>Scott Lepisto</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B05432-B05432-text">
  <body xml:id="B05432-e0">
   <div type="proclamation" xml:id="B05432-e10">
    <pb facs="tcp:178963:1" rend="simple:additions" xml:id="B05432-001-a"/>
    <head xml:id="B05432-e20">
     <hi xml:id="B05432-e30">
      <w lemma="a" pos="sy" xml:id="B05432-001-a-0010">A</w>
      <w lemma="proclamation" pos="n1" xml:id="B05432-001-a-0020">PROCLAMATION</w>
     </hi>
     <pc rend="follows-hi" xml:id="B05432-001-a-0030">,</pc>
     <w lemma="adjourn" pos="vvg" xml:id="B05432-001-a-0040">Adjourning</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-0050">the</w>
     <w lemma="parliament" pos="n1" xml:id="B05432-001-a-0060">Parliament</w>
     <w lemma="from" pos="acp" xml:id="B05432-001-a-0070">from</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-0080">the</w>
     <w lemma="eighteen" pos="ord" xml:id="B05432-001-a-0090">eighteenth</w>
     <w lemma="day" pos="n1" xml:id="B05432-001-a-0100">day</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-0110">of</w>
     <w lemma="April" pos="nn1" rend="hi" xml:id="B05432-001-a-0120">April</w>
     <pc xml:id="B05432-001-a-0130">,</pc>
     <w lemma="to" pos="acp" xml:id="B05432-001-a-0140">to</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-0150">the</w>
     <w lemma="nine" pos="ord" xml:id="B05432-001-a-0160">ninth</w>
     <w lemma="day" pos="n1" xml:id="B05432-001-a-0170">day</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-0180">of</w>
     <hi xml:id="B05432-e50">
      <w lemma="May" pos="nn1" xml:id="B05432-001-a-0190">May</w>
      <w lemma="1695." pos="crd" xml:id="B05432-001-a-0200">1695.</w>
     </hi>
     <pc rend="follows-hi" unit="sentence" xml:id="B05432-001-a-0210"/>
    </head>
    <opener xml:id="B05432-e60">
     <signed xml:id="B05432-e70">
      <w lemma="WILLIAM" pos="nn1" rend="hi" xml:id="B05432-001-a-0220">WILLIAM</w>
      <w lemma="by" pos="acp" xml:id="B05432-001-a-0230">by</w>
      <w lemma="the" pos="d" xml:id="B05432-001-a-0240">the</w>
      <w lemma="grace" pos="n1" xml:id="B05432-001-a-0250">Grace</w>
      <w lemma="of" pos="acp" xml:id="B05432-001-a-0260">of</w>
      <w lemma="GOD" pos="nn1" xml:id="B05432-001-a-0270">GOD</w>
      <pc xml:id="B05432-001-a-0280">,</pc>
      <w lemma="king" pos="n1" xml:id="B05432-001-a-0290">King</w>
      <w lemma="of" pos="acp" xml:id="B05432-001-a-0300">of</w>
      <hi xml:id="B05432-e90">
       <w lemma="Great-Britane" pos="nn1" xml:id="B05432-001-a-0310">Great-Britane</w>
       <pc xml:id="B05432-001-a-0320">,</pc>
       <w lemma="France" pos="nn1" xml:id="B05432-001-a-0330">France</w>
      </hi>
      <w lemma="and" pos="cc" xml:id="B05432-001-a-0340">and</w>
      <w lemma="Ireland" pos="nn1" rend="hi" xml:id="B05432-001-a-0350">Ireland</w>
      <pc xml:id="B05432-001-a-0360">,</pc>
      <w lemma="defender" pos="n1" xml:id="B05432-001-a-0370">Defender</w>
      <w lemma="of" pos="acp" xml:id="B05432-001-a-0380">of</w>
      <w lemma="the" pos="d" xml:id="B05432-001-a-0390">the</w>
      <w lemma="faith" pos="n1" xml:id="B05432-001-a-0400">Faith</w>
      <pc xml:id="B05432-001-a-0410">,</pc>
     </signed>
     <salute xml:id="B05432-e110">
      <w lemma="to" pos="prt" xml:id="B05432-001-a-0420">to</w>
      <w lemma="our" pos="po" xml:id="B05432-001-a-0430">Our</w>
      <w lemma="lion" pos="n1" reg="Lion" xml:id="B05432-001-a-0440">Lyon</w>
      <w lemma="king" pos="n1" xml:id="B05432-001-a-0450">King</w>
      <w lemma="at" pos="acp" xml:id="B05432-001-a-0460">at</w>
      <w lemma="arm" pos="n2" xml:id="B05432-001-a-0470">Arms</w>
      <pc xml:id="B05432-001-a-0480">,</pc>
      <w lemma="and" pos="cc" xml:id="B05432-001-a-0490">and</w>
      <w lemma="his" pos="po" xml:id="B05432-001-a-0500">his</w>
      <w lemma="brethren" pos="n2" reg="Brethrens" xml:id="B05432-001-a-0510">Brethren</w>
      <w lemma="herald" pos="n2" reg="Heralds" xml:id="B05432-001-a-0520">Heraulds</w>
      <pc xml:id="B05432-001-a-0530">,</pc>
      <w lemma="macer" pos="n2" xml:id="B05432-001-a-0540">Macers</w>
      <w lemma="of" pos="acp" xml:id="B05432-001-a-0550">of</w>
      <w lemma="our" pos="po" xml:id="B05432-001-a-0560">Our</w>
      <w lemma="privy" pos="j" xml:id="B05432-001-a-0570">Privy</w>
      <w lemma="council" pos="n1" xml:id="B05432-001-a-0580">Council</w>
      <pc xml:id="B05432-001-a-0590">,</pc>
      <w lemma="pursuivant" pos="n2" reg="Pursuivants" xml:id="B05432-001-a-0600">Pursevants</w>
      <pc xml:id="B05432-001-a-0610">,</pc>
      <w lemma="messenger" pos="n2" xml:id="B05432-001-a-0620">Messengers</w>
      <w lemma="at" pos="acp" xml:id="B05432-001-a-0630">at</w>
      <w lemma="arm" pos="n2" xml:id="B05432-001-a-0640">Arms</w>
      <pc xml:id="B05432-001-a-0650">,</pc>
      <w lemma="our" pos="po" xml:id="B05432-001-a-0660">Our</w>
      <w lemma="sheriff" pos="n2" xml:id="B05432-001-a-0670">Sheriffs</w>
      <w lemma="in" pos="acp" xml:id="B05432-001-a-0680">in</w>
      <w lemma="that" pos="d" xml:id="B05432-001-a-0690">that</w>
      <w lemma="part" pos="n1" xml:id="B05432-001-a-0700">part</w>
      <pc xml:id="B05432-001-a-0710">,</pc>
      <w lemma="conjunct" pos="av-j" xml:id="B05432-001-a-0720">conjunctly</w>
      <w lemma="and" pos="cc" xml:id="B05432-001-a-0730">and</w>
      <w lemma="several" pos="av-j" xml:id="B05432-001-a-0740">severally</w>
      <pc xml:id="B05432-001-a-0750">,</pc>
      <w lemma="special" pos="av-j" xml:id="B05432-001-a-0760">specially</w>
      <w lemma="constitute" pos="vvi" xml:id="B05432-001-a-0770">constitute</w>
      <pc xml:id="B05432-001-a-0780">,</pc>
      <w lemma="greeting" pos="n1" xml:id="B05432-001-a-0790">Greeting</w>
      <pc xml:id="B05432-001-a-0800">;</pc>
     </salute>
    </opener>
    <p xml:id="B05432-e120">
     <w lemma="forasmuch" pos="av" xml:id="B05432-001-a-0810">Forasmuch</w>
     <w lemma="as" pos="acp" xml:id="B05432-001-a-0820">as</w>
     <pc xml:id="B05432-001-a-0830">,</pc>
     <w lemma="we" pos="pns" xml:id="B05432-001-a-0840">We</w>
     <w lemma="by" pos="acp" xml:id="B05432-001-a-0850">by</w>
     <w lemma="our" pos="po" xml:id="B05432-001-a-0860">Our</w>
     <w lemma="proclamation" pos="n1" xml:id="B05432-001-a-0870">Proclamation</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-0880">of</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-0890">the</w>
     <w lemma="date" pos="n1" xml:id="B05432-001-a-0900">date</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-0910">the</w>
     <w lemma="nine" pos="ord" xml:id="B05432-001-a-0920">ninth</w>
     <w lemma="day" pos="n1" xml:id="B05432-001-a-0930">day</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-0940">of</w>
     <w lemma="March" pos="nn1" rend="hi" xml:id="B05432-001-a-0950">March</w>
     <w lemma="last" pos="ord" xml:id="B05432-001-a-0960">last</w>
     <w lemma="bypass" pos="vvn" xml:id="B05432-001-a-0970">bypast</w>
     <pc xml:id="B05432-001-a-0980">,</pc>
     <w lemma="with" pos="acp" xml:id="B05432-001-a-0990">with</w>
     <w lemma="advice" pos="n1" xml:id="B05432-001-a-1000">Advice</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-1010">of</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-1020">the</w>
     <w lemma="lord" pos="n2" xml:id="B05432-001-a-1030">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-1040">of</w>
     <w lemma="our" pos="po" xml:id="B05432-001-a-1050">Our</w>
     <w lemma="privy" pos="j" xml:id="B05432-001-a-1060">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05432-001-a-1070">Council</w>
     <pc xml:id="B05432-001-a-1080">,</pc>
     <w lemma="do" pos="vvd" xml:id="B05432-001-a-1090">did</w>
     <w lemma="adjourn" pos="vvi" xml:id="B05432-001-a-1100">Adjourn</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-1110">the</w>
     <w lemma="current" pos="j" xml:id="B05432-001-a-1120">Current</w>
     <w lemma="parliament" pos="n1" xml:id="B05432-001-a-1130">Parliament</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-1140">of</w>
     <w lemma="this" pos="d" xml:id="B05432-001-a-1150">this</w>
     <w lemma="our" pos="po" xml:id="B05432-001-a-1160">Our</w>
     <w lemma="ancient" pos="j" reg="Ancient" xml:id="B05432-001-a-1170">Antient</w>
     <w lemma="kingdom" pos="n1" xml:id="B05432-001-a-1180">Kingdom</w>
     <w lemma="to" pos="acp" xml:id="B05432-001-a-1190">to</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-1200">the</w>
     <w lemma="eighteen" pos="ord" xml:id="B05432-001-a-1210">eighteenth</w>
     <w lemma="day" pos="n1" xml:id="B05432-001-a-1220">day</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-1230">of</w>
     <w lemma="April" pos="nn1" rend="hi" xml:id="B05432-001-a-1240">April</w>
     <w lemma="then" pos="av" xml:id="B05432-001-a-1250">then</w>
     <w lemma="next" pos="ord" xml:id="B05432-001-a-1260">next</w>
     <w lemma="to" pos="prt" xml:id="B05432-001-a-1270">to</w>
     <w lemma="come" pos="vvi" xml:id="B05432-001-a-1280">come</w>
     <pc xml:id="B05432-001-a-1290">,</pc>
     <w lemma="now" pos="av" xml:id="B05432-001-a-1300">now</w>
     <w lemma="instant" pos="j" xml:id="B05432-001-a-1310">instant</w>
     <pc xml:id="B05432-001-a-1320">;</pc>
     <w lemma="and" pos="cc" xml:id="B05432-001-a-1330">And</w>
     <w lemma="whereas" pos="cs" xml:id="B05432-001-a-1340">whereas</w>
     <w lemma="we" pos="pns" xml:id="B05432-001-a-1350">We</w>
     <w lemma="be" pos="vvb" xml:id="B05432-001-a-1360">are</w>
     <w lemma="inform" pos="vvn" xml:id="B05432-001-a-1370">informed</w>
     <pc xml:id="B05432-001-a-1380">,</pc>
     <w lemma="that" pos="cs" xml:id="B05432-001-a-1390">that</w>
     <w lemma="such" pos="d" xml:id="B05432-001-a-1400">such</w>
     <w lemma="frequent" pos="j" xml:id="B05432-001-a-1410">frequent</w>
     <w lemma="adjournment" pos="n2" xml:id="B05432-001-a-1420">Adjournments</w>
     <w lemma="have" pos="vvz" xml:id="B05432-001-a-1430">hath</w>
     <w lemma="beget" pos="vvn" xml:id="B05432-001-a-1440">begot</w>
     <w lemma="a" pos="d" xml:id="B05432-001-a-1450">an</w>
     <w lemma="opinion" pos="n1" xml:id="B05432-001-a-1460">Opinion</w>
     <w lemma="that" pos="cs" xml:id="B05432-001-a-1470">that</w>
     <w lemma="our" pos="po" xml:id="B05432-001-a-1480">Our</w>
     <w lemma="parliament" pos="n1" xml:id="B05432-001-a-1490">Parliament</w>
     <w lemma="be" pos="vvz" xml:id="B05432-001-a-1500">is</w>
     <w lemma="not" pos="xx" xml:id="B05432-001-a-1510">not</w>
     <w lemma="to" pos="prt" xml:id="B05432-001-a-1520">to</w>
     <w lemma="meet" pos="vvi" xml:id="B05432-001-a-1530">meet</w>
     <pc xml:id="B05432-001-a-1540">,</pc>
     <w lemma="which" pos="crq" xml:id="B05432-001-a-1550">which</w>
     <w lemma="make" pos="vvz" xml:id="B05432-001-a-1560">makes</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-1570">the</w>
     <w lemma="member" pos="n2" xml:id="B05432-001-a-1580">Members</w>
     <w lemma="more" pos="avc-d" xml:id="B05432-001-a-1590">more</w>
     <w lemma="slow" pos="j" xml:id="B05432-001-a-1600">slow</w>
     <w lemma="to" pos="prt" xml:id="B05432-001-a-1610">to</w>
     <w lemma="come" pos="vvi" xml:id="B05432-001-a-1620">come</w>
     <w lemma="at" pos="acp" xml:id="B05432-001-a-1630">at</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-1640">the</w>
     <w lemma="time" pos="n1" xml:id="B05432-001-a-1650">time</w>
     <w lemma="appoint" pos="vvn" xml:id="B05432-001-a-1660">appointed</w>
     <pc xml:id="B05432-001-a-1670">;</pc>
     <w lemma="and" pos="cc" xml:id="B05432-001-a-1680">And</w>
     <w lemma="therefore" pos="av" xml:id="B05432-001-a-1690">therefore</w>
     <w lemma="a" pos="d" xml:id="B05432-001-a-1700">a</w>
     <w lemma="timeous" pos="j" xml:id="B05432-001-a-1710">timeous</w>
     <w lemma="positive" pos="j" xml:id="B05432-001-a-1720">positive</w>
     <w lemma="advertisement" pos="n1" xml:id="B05432-001-a-1730">Advertisement</w>
     <w lemma="be" pos="vvz" xml:id="B05432-001-a-1740">is</w>
     <w lemma="necessary" pos="j" xml:id="B05432-001-a-1750">necessary</w>
     <pc xml:id="B05432-001-a-1760">,</pc>
     <w lemma="we" pos="pns" xml:id="B05432-001-a-1770">We</w>
     <w lemma="have" pos="vvb" xml:id="B05432-001-a-1780">have</w>
     <w lemma="think" pos="vvn" xml:id="B05432-001-a-1790">thought</w>
     <w lemma="fit" pos="j" xml:id="B05432-001-a-1800">fit</w>
     <w lemma="that" pos="cs" xml:id="B05432-001-a-1810">that</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-1820">the</w>
     <w lemma="adjournment" pos="n1" xml:id="B05432-001-a-1830">Adjournment</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-1840">of</w>
     <w lemma="our" pos="po" xml:id="B05432-001-a-1850">Our</w>
     <w lemma="say" pos="j-vn" xml:id="B05432-001-a-1860">said</w>
     <w lemma="current" pos="n1" xml:id="B05432-001-a-1870">current</w>
     <w lemma="parliament" pos="n1" xml:id="B05432-001-a-1880">Parliament</w>
     <w lemma="shall" pos="vmb" xml:id="B05432-001-a-1890">shall</w>
     <w lemma="be" pos="vvi" xml:id="B05432-001-a-1900">be</w>
     <w lemma="continue" pos="vvn" xml:id="B05432-001-a-1910">continued</w>
     <w lemma="from" pos="acp" xml:id="B05432-001-a-1920">from</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-1930">the</w>
     <w lemma="say" pos="j-vn" xml:id="B05432-001-a-1940">said</w>
     <w lemma="eighteen" pos="ord" xml:id="B05432-001-a-1950">eighteenth</w>
     <w lemma="day" pos="n1" xml:id="B05432-001-a-1960">day</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-1970">of</w>
     <w lemma="April" pos="nn1" rend="hi" xml:id="B05432-001-a-1980">April</w>
     <w lemma="instant" pos="j" xml:id="B05432-001-a-1990">instant</w>
     <pc xml:id="B05432-001-a-2000">,</pc>
     <w lemma="to" pos="prt" xml:id="B05432-001-a-2010">to</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-2020">the</w>
     <w lemma="nine" pos="ord" xml:id="B05432-001-a-2030">ninth</w>
     <w lemma="day" pos="n1" xml:id="B05432-001-a-2040">day</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-2050">of</w>
     <w lemma="May" pos="nn1" rend="hi" xml:id="B05432-001-a-2060">May</w>
     <w lemma="next" pos="ord" xml:id="B05432-001-a-2070">next</w>
     <w lemma="to" pos="prt" xml:id="B05432-001-a-2080">to</w>
     <w lemma="come" pos="vvi" xml:id="B05432-001-a-2090">come</w>
     <pc xml:id="B05432-001-a-2100">;</pc>
     <w lemma="at" pos="acp" xml:id="B05432-001-a-2110">At</w>
     <w lemma="which" pos="crq" xml:id="B05432-001-a-2120">which</w>
     <w lemma="time" pos="n1" xml:id="B05432-001-a-2130">time</w>
     <pc xml:id="B05432-001-a-2140">,</pc>
     <w lemma="we" pos="pns" xml:id="B05432-001-a-2150">We</w>
     <w lemma="be" pos="vvb" xml:id="B05432-001-a-2160">are</w>
     <w lemma="full" pos="av-j" xml:id="B05432-001-a-2170">fully</w>
     <w lemma="resolve" pos="vvn" xml:id="B05432-001-a-2180">resolved</w>
     <w lemma="our" pos="po" xml:id="B05432-001-a-2190">Our</w>
     <w lemma="say" pos="j-vn" xml:id="B05432-001-a-2200">said</w>
     <w lemma="parliament" pos="n1" xml:id="B05432-001-a-2210">Parliament</w>
     <w lemma="shall" pos="vmb" xml:id="B05432-001-a-2220">shall</w>
     <w lemma="meet" pos="vvi" xml:id="B05432-001-a-2230">meet</w>
     <pc xml:id="B05432-001-a-2240">:</pc>
     <w lemma="therefore" pos="av" xml:id="B05432-001-a-2250">Therefore</w>
     <w lemma="we" pos="pns" xml:id="B05432-001-a-2260">We</w>
     <w lemma="with" pos="acp" xml:id="B05432-001-a-2270">with</w>
     <w lemma="advice" pos="n1" xml:id="B05432-001-a-2280">Advice</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-2290">of</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-2300">the</w>
     <w lemma="lord" pos="n2" xml:id="B05432-001-a-2310">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-2320">of</w>
     <w lemma="our" pos="po" xml:id="B05432-001-a-2330">Our</w>
     <w lemma="privy" pos="j" xml:id="B05432-001-a-2340">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05432-001-a-2350">Council</w>
     <pc xml:id="B05432-001-a-2360">,</pc>
     <w lemma="do" pos="vvb" xml:id="B05432-001-a-2370">do</w>
     <w lemma="hereby" pos="av" xml:id="B05432-001-a-2380">hereby</w>
     <w lemma="adjourn" pos="vvi" xml:id="B05432-001-a-2390">Adjourn</w>
     <w lemma="our" pos="po" xml:id="B05432-001-a-2400">Our</w>
     <w lemma="say" pos="j-vn" xml:id="B05432-001-a-2410">said</w>
     <w lemma="current" pos="n1" xml:id="B05432-001-a-2420">current</w>
     <w lemma="parliament" pos="n1" xml:id="B05432-001-a-2430">Parliament</w>
     <w lemma="until" pos="acp" xml:id="B05432-001-a-2440">until</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-2450">the</w>
     <w lemma="say" pos="j-vn" xml:id="B05432-001-a-2460">said</w>
     <w lemma="nine" pos="ord" xml:id="B05432-001-a-2470">ninth</w>
     <w lemma="day" pos="n1" xml:id="B05432-001-a-2480">day</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-2490">of</w>
     <w lemma="May" pos="nn1" rend="hi" xml:id="B05432-001-a-2500">May</w>
     <pc xml:id="B05432-001-a-2510">,</pc>
     <w lemma="next" pos="ord" xml:id="B05432-001-a-2520">next</w>
     <w lemma="ensue" pos="vvg" xml:id="B05432-001-a-2530">ensuing</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-2540">the</w>
     <w lemma="date" pos="n1" xml:id="B05432-001-a-2550">date</w>
     <w lemma="hereof" pos="av" xml:id="B05432-001-a-2560">hereof</w>
     <pc xml:id="B05432-001-a-2570">,</pc>
     <w lemma="require" pos="vvg" xml:id="B05432-001-a-2580">requiring</w>
     <w lemma="all" pos="d" xml:id="B05432-001-a-2590">all</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-2600">the</w>
     <w lemma="member" pos="n2" xml:id="B05432-001-a-2610">Members</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-2620">of</w>
     <w lemma="our" pos="po" xml:id="B05432-001-a-2630">Our</w>
     <w lemma="say" pos="j-vn" xml:id="B05432-001-a-2640">said</w>
     <w lemma="current" pos="n1" xml:id="B05432-001-a-2650">current</w>
     <w lemma="parliament" pos="n1" xml:id="B05432-001-a-2660">Parliament</w>
     <w lemma="to" pos="prt" xml:id="B05432-001-a-2670">to</w>
     <w lemma="attend" pos="vvi" reg="attended" xml:id="B05432-001-a-2680">attend</w>
     <w lemma="precise" pos="av-j" xml:id="B05432-001-a-2690">precisely</w>
     <w lemma="at" pos="acp" xml:id="B05432-001-a-2700">at</w>
     <w lemma="that" pos="d" xml:id="B05432-001-a-2710">that</w>
     <w lemma="day" pos="n1" xml:id="B05432-001-a-2720">day</w>
     <pc xml:id="B05432-001-a-2730">▪</pc>
     <w lemma="in" pos="acp" xml:id="B05432-001-a-2740">in</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-2750">the</w>
     <w lemma="usual" pos="j" xml:id="B05432-001-a-2760">usual</w>
     <w lemma="way" pos="n1" xml:id="B05432-001-a-2770">way</w>
     <pc xml:id="B05432-001-a-2780">,</pc>
     <w lemma="and" pos="cc" xml:id="B05432-001-a-2790">and</w>
     <w lemma="under" pos="acp" xml:id="B05432-001-a-2800">under</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-2810">the</w>
     <w lemma="certification" pos="n2" xml:id="B05432-001-a-2820">Certifications</w>
     <w lemma="contain" pos="vvn" xml:id="B05432-001-a-2830">contained</w>
     <w lemma="in" pos="acp" xml:id="B05432-001-a-2840">in</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-2850">the</w>
     <w lemma="several" pos="j" xml:id="B05432-001-a-2860">several</w>
     <w lemma="act" pos="n2" xml:id="B05432-001-a-2870">Acts</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-2880">of</w>
     <w lemma="parliament" pos="n1" xml:id="B05432-001-a-2890">Parliament</w>
     <w lemma="make" pos="vvd" xml:id="B05432-001-a-2900">made</w>
     <w lemma="thereanent" pos="av" xml:id="B05432-001-a-2910">thereanent</w>
     <pc unit="sentence" xml:id="B05432-001-a-2920">.</pc>
     <w lemma="our" pos="po" xml:id="B05432-001-a-2930">OUR</w>
     <w lemma="will" pos="n1" xml:id="B05432-001-a-2940">WILL</w>
     <w lemma="be" pos="vvz" xml:id="B05432-001-a-2950">IS</w>
     <w lemma="herefore" pos="av" xml:id="B05432-001-a-2960">HEREFORE</w>
     <pc xml:id="B05432-001-a-2970">,</pc>
     <w lemma="and" pos="cc" xml:id="B05432-001-a-2980">and</w>
     <w lemma="we" pos="pns" xml:id="B05432-001-a-2990">We</w>
     <w lemma="charge" pos="vvb" xml:id="B05432-001-a-3000">charge</w>
     <w lemma="you" pos="pn" xml:id="B05432-001-a-3010">you</w>
     <w lemma="strict" pos="av-j" xml:id="B05432-001-a-3020">strictly</w>
     <pc xml:id="B05432-001-a-3030">,</pc>
     <w lemma="and" pos="cc" xml:id="B05432-001-a-3040">and</w>
     <w lemma="command" pos="n1" xml:id="B05432-001-a-3050">Command</w>
     <pc xml:id="B05432-001-a-3060">,</pc>
     <w lemma="that" pos="cs" xml:id="B05432-001-a-3070">that</w>
     <w lemma="incontinent" pos="j" xml:id="B05432-001-a-3080">incontinent</w>
     <w lemma="their" pos="po" reg="their" xml:id="B05432-001-a-3090">thir</w>
     <w lemma="our" pos="po" xml:id="B05432-001-a-3100">Our</w>
     <w lemma="letter" pos="n2" xml:id="B05432-001-a-3110">Letters</w>
     <w lemma="see" pos="vvn" xml:id="B05432-001-a-3120">seen</w>
     <pc xml:id="B05432-001-a-3130">,</pc>
     <w lemma="you" pos="pn" xml:id="B05432-001-a-3140">ye</w>
     <w lemma="pass" pos="vvb" xml:id="B05432-001-a-3150">pass</w>
     <w lemma="to" pos="acp" xml:id="B05432-001-a-3160">to</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-3170">the</w>
     <w lemma="mercat-cross" pos="n1" xml:id="B05432-001-a-3180">Mercat-Cross</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-3190">of</w>
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05432-001-a-3200">Edinburgh</w>
     <pc xml:id="B05432-001-a-3210">,</pc>
     <w lemma="and" pos="cc" xml:id="B05432-001-a-3220">and</w>
     <w lemma="to" pos="acp" xml:id="B05432-001-a-3230">to</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-3240">the</w>
     <w lemma="mercat-cross" pos="n2" xml:id="B05432-001-a-3250">Mercat-Crosses</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-3260">of</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-3270">the</w>
     <w lemma="n/a" pos="fla" xml:id="B05432-001-a-3280">remanent</w>
     <w lemma="headburgh" pos="n2" reg="Headburghs" xml:id="B05432-001-a-3290">Head-Burghs</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-3300">of</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-3310">the</w>
     <w lemma="several" pos="j" xml:id="B05432-001-a-3320">several</w>
     <w lemma="shire" pos="n2" xml:id="B05432-001-a-3330">Shires</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-3340">of</w>
     <w lemma="this" pos="d" xml:id="B05432-001-a-3350">this</w>
     <w lemma="our" pos="po" xml:id="B05432-001-a-3360">Our</w>
     <w lemma="ancient" pos="j" reg="Ancient" xml:id="B05432-001-a-3370">Antient</w>
     <w lemma="kingdom" pos="n1" xml:id="B05432-001-a-3380">Kingdom</w>
     <pc xml:id="B05432-001-a-3390">,</pc>
     <w lemma="and" pos="cc" xml:id="B05432-001-a-3400">and</w>
     <w lemma="there" pos="av" xml:id="B05432-001-a-3410">there</w>
     <w lemma="by" pos="acp" xml:id="B05432-001-a-3420">by</w>
     <w lemma="open" pos="j" xml:id="B05432-001-a-3430">open</w>
     <w lemma="proclamation" pos="n1" xml:id="B05432-001-a-3440">Proclamation</w>
     <pc xml:id="B05432-001-a-3450">,</pc>
     <w lemma="make" pos="vvb" xml:id="B05432-001-a-3460">make</w>
     <w lemma="intimation" pos="n1" xml:id="B05432-001-a-3470">Intimation</w>
     <pc xml:id="B05432-001-a-3480">,</pc>
     <w lemma="that" pos="cs" xml:id="B05432-001-a-3490">that</w>
     <w lemma="our" pos="po" xml:id="B05432-001-a-3500">Our</w>
     <w lemma="say" pos="j-vn" xml:id="B05432-001-a-3510">said</w>
     <w lemma="parliament" pos="n1" xml:id="B05432-001-a-3520">Parliament</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-3530">of</w>
     <w lemma="this" pos="d" xml:id="B05432-001-a-3540">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05432-001-a-3550">Kingdom</w>
     <w lemma="be" pos="vvz" xml:id="B05432-001-a-3560">is</w>
     <w lemma="adjourn" pos="vvn" xml:id="B05432-001-a-3570">Adjourned</w>
     <w lemma="till" pos="acp" xml:id="B05432-001-a-3580">till</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-3590">the</w>
     <w lemma="say" pos="j-vn" xml:id="B05432-001-a-3600">said</w>
     <w lemma="nine" pos="ord" xml:id="B05432-001-a-3610">ninth</w>
     <w lemma="day" pos="n1" xml:id="B05432-001-a-3620">day</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-3630">of</w>
     <w lemma="May" pos="nn1" rend="hi" xml:id="B05432-001-a-3640">May</w>
     <w lemma="next" pos="ord" xml:id="B05432-001-a-3650">next</w>
     <w lemma="to" pos="prt" xml:id="B05432-001-a-3660">to</w>
     <w lemma="come" pos="vvi" xml:id="B05432-001-a-3670">come</w>
     <pc unit="sentence" xml:id="B05432-001-a-3680">.</pc>
     <w lemma="and" pos="cc" xml:id="B05432-001-a-3690">And</w>
     <w lemma="ordain" pos="vvz" xml:id="B05432-001-a-3700">Ordains</w>
     <w lemma="these" pos="d" xml:id="B05432-001-a-3710">these</w>
     <w lemma="present" pos="n2" xml:id="B05432-001-a-3720">presents</w>
     <w lemma="to" pos="prt" xml:id="B05432-001-a-3730">to</w>
     <w lemma="be" pos="vvi" xml:id="B05432-001-a-3740">be</w>
     <w lemma="print" pos="vvn" xml:id="B05432-001-a-3750">Printed</w>
     <pc unit="sentence" xml:id="B05432-001-a-3760">.</pc>
    </p>
    <closer xml:id="B05432-e200">
     <dateline xml:id="B05432-e210">
      <hi xml:id="B05432-e220">
       <w lemma="give" pos="vvn" xml:id="B05432-001-a-3770">Given</w>
       <w lemma="under" pos="acp" xml:id="B05432-001-a-3780">under</w>
       <w lemma="our" pos="po" xml:id="B05432-001-a-3790">Our</w>
       <w lemma="signet" pos="n1" xml:id="B05432-001-a-3800">Signet</w>
       <w lemma="at" pos="acp" xml:id="B05432-001-a-3810">at</w>
      </hi>
      <w lemma="Edinburgh" pos="nn1" xml:id="B05432-001-a-3820">Edinburgh</w>
      <pc xml:id="B05432-001-a-3830">,</pc>
      <date xml:id="B05432-e230">
       <hi xml:id="B05432-e240">
        <w lemma="the" pos="d" xml:id="B05432-001-a-3840">the</w>
        <w lemma="seventeen" pos="ord" xml:id="B05432-001-a-3850">seventeenth</w>
        <w lemma="day" pos="n1" xml:id="B05432-001-a-3860">day</w>
        <w lemma="of" pos="acp" xml:id="B05432-001-a-3870">of</w>
       </hi>
       <w lemma="April" pos="nn1" xml:id="B05432-001-a-3880">April</w>
       <pc xml:id="B05432-001-a-3890">,</pc>
       <hi xml:id="B05432-e250">
        <w lemma="and" pos="cc" xml:id="B05432-001-a-3900">and</w>
        <w lemma="of" pos="acp" xml:id="B05432-001-a-3910">of</w>
        <w lemma="our" pos="po" xml:id="B05432-001-a-3920">Our</w>
        <w lemma="reign" pos="n1" xml:id="B05432-001-a-3930">Reign</w>
        <w lemma="the" pos="d" xml:id="B05432-001-a-3940">the</w>
        <w lemma="seven" pos="ord" xml:id="B05432-001-a-3950">seventh</w>
        <w lemma="year" pos="n1" xml:id="B05432-001-a-3960">Year</w>
        <pc xml:id="B05432-001-a-3970">,</pc>
       </hi>
       <w lemma="1695." pos="crd" xml:id="B05432-001-a-3980">1695.</w>
       <pc unit="sentence" xml:id="B05432-001-a-3990"/>
      </date>
     </dateline>
     <signed xml:id="B05432-e260">
      <hi xml:id="B05432-e270">
       <w lemma="n/a" pos="fla" xml:id="B05432-001-a-4000">Per</w>
       <w lemma="n/a" pos="fla" xml:id="B05432-001-a-4010">Actum</w>
       <w lemma="n/a" pos="fla" xml:id="B05432-001-a-4020">Dominorum</w>
       <w lemma="n/a" pos="fla" xml:id="B05432-001-a-4030">Secreti</w>
       <w lemma="n/a" pos="fla" xml:id="B05432-001-a-4040">Concilii</w>
      </hi>
      <pc rend="follows-hi" unit="sentence" xml:id="B05432-001-a-4050">.</pc>
      <w lemma="GILB" pos="nn1" xml:id="B05432-001-a-4060">GILB</w>
      <pc unit="sentence" xml:id="B05432-001-a-4070">.</pc>
      <w lemma="ELIOT" pos="nn1" xml:id="B05432-001-a-4080">ELIOT</w>
      <pc xml:id="B05432-001-a-4090">,</pc>
      <hi xml:id="B05432-e280">
       <w lemma="cls." pos="ab" xml:id="B05432-001-a-4100">Cls.</w>
       <w lemma="sti." pos="ab" xml:id="B05432-001-a-4110">Sti.</w>
       <w lemma="n/a" pos="fla" xml:id="B05432-001-a-4120">Concilii</w>
      </hi>
      <pc rend="follows-hi" unit="sentence" xml:id="B05432-001-a-4130">.</pc>
     </signed>
    </closer>
    <closer xml:id="B05432-e290">
     <w lemma="GOD" pos="nn1" xml:id="B05432-001-a-4140">GOD</w>
     <w lemma="save" pos="acp" xml:id="B05432-001-a-4150">Save</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-4160">the</w>
     <w lemma="king" pos="n1" xml:id="B05432-001-a-4170">King</w>
     <pc unit="sentence" xml:id="B05432-001-a-4171">.</pc>
     <pc unit="sentence" xml:id="B05432-001-a-4180"/>
    </closer>
   </div>
  </body>
  <back xml:id="B05432-e300">
   <div type="colophon" xml:id="B05432-e310">
    <p xml:id="B05432-e320">
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05432-001-a-4190">Edinburgh</w>
     <pc xml:id="B05432-001-a-4200">,</pc>
     <w lemma="print" pos="vvn" xml:id="B05432-001-a-4210">Printed</w>
     <w lemma="by" pos="acp" xml:id="B05432-001-a-4220">by</w>
     <w lemma="the" pos="d" xml:id="B05432-001-a-4230">the</w>
     <w lemma="heir" pos="n2" xml:id="B05432-001-a-4240">Heirs</w>
     <w lemma="and" pos="cc" xml:id="B05432-001-a-4250">and</w>
     <w lemma="successor" pos="n2" xml:id="B05432-001-a-4260">Successors</w>
     <w lemma="of" pos="acp" xml:id="B05432-001-a-4270">of</w>
     <hi xml:id="B05432-e340">
      <w lemma="Andrew" pos="nn1" xml:id="B05432-001-a-4280">Andrew</w>
      <w lemma="Anderson" pos="nn1" xml:id="B05432-001-a-4290">Anderson</w>
     </hi>
     <pc rend="follows-hi" xml:id="B05432-001-a-4300">,</pc>
     <w lemma="printer" pos="n1" xml:id="B05432-001-a-4310">Printer</w>
     <w lemma="to" pos="acp" xml:id="B05432-001-a-4320">to</w>
     <w lemma="his" pos="po" xml:id="B05432-001-a-4330">His</w>
     <w lemma="most" pos="avs-d" xml:id="B05432-001-a-4340">Most</w>
     <w lemma="excellent" pos="j" xml:id="B05432-001-a-4350">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="B05432-001-a-4360">Majesty</w>
     <pc xml:id="B05432-001-a-4370">,</pc>
     <hi xml:id="B05432-e350">
      <w lemma="n/a" pos="fla" xml:id="B05432-001-a-4380">Anno</w>
      <w lemma="dom." pos="ab" xml:id="B05432-001-a-4390">Dom.</w>
     </hi>
     <w lemma="1695." pos="crd" xml:id="B05432-001-a-4400">1695.</w>
     <pc unit="sentence" xml:id="B05432-001-a-4410"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
