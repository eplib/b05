<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B05603">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A proclamation, for a solemn and publick thanksgiving.</title>
    <author>Scotland. Privy Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B05603 of text R183469 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing S1787). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B05603</idno>
    <idno type="STC">Wing S1787</idno>
    <idno type="STC">ESTC R183469</idno>
    <idno type="EEBO-CITATION">52528963</idno>
    <idno type="OCLC">ocm 52528963</idno>
    <idno type="VID">179041</idno>
    <idno type="PROQUESTGOID">2240873507</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B05603)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179041)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2776:30)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A proclamation, for a solemn and publick thanksgiving.</title>
      <author>Scotland. Privy Council.</author>
      <author>Scotland. Sovereign (1689-1694 : William and Mary)</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by the heir of Andrew Anderson, Printer to their most excellent Majestys,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>1690.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Intentional blank spaces in text.</note>
      <note>Dated: Given under Our Signet, at Holy-rood-house, the seventeenth day of September, one thousand six hundred and ninety, and of Our Reign the second year.</note>
      <note>Signed: Da. Moncreif, Cls. Secreti Concilii.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Prayers -- Law and legislation -- Scotland -- Early works to 1800.</term>
     <term>Public worship -- Scotland -- Early works to 1800.</term>
     <term>Church and state -- Scotland -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A proclamation, : for a solemn and publick thanksgiving.</ep:title>
    <ep:author>Scotland. Privy Council</ep:author>
    <ep:publicationYear>1690</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>442</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>2</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>452.49</ep:defectRate>
    <ep:finalGrade>F</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 452.49 defects per 10,000 words puts this text in the F category of texts with  100 or more defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-02</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-03</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-04</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-04</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B05603-t">
  <body xml:id="B05603-e0">
   <div type="royal_proclamation" xml:id="B05603-e10">
    <pb facs="tcp:179041:1" rend="simple:additions" xml:id="B05603-001-a"/>
    <head xml:id="B05603-e20">
     <w lemma="a" pos="d" xml:id="B05603-001-a-0010">A</w>
     <w lemma="proclamation" pos="n1" xml:id="B05603-001-a-0020">PROCLAMATION</w>
     <hi xml:id="B05603-e30">
      <w lemma="for" pos="acp" xml:id="B05603-001-a-0030">For</w>
      <w lemma="a" pos="d" xml:id="B05603-001-a-0040">a</w>
      <w lemma="solemn" pos="j" xml:id="B05603-001-a-0050">solemn</w>
      <w lemma="and" pos="cc" xml:id="B05603-001-a-0060">and</w>
      <w lemma="public" pos="j" reg="public" xml:id="B05603-001-a-0070">publick</w>
      <w lemma="thanksgiving" pos="n1" xml:id="B05603-001-a-0080">Thanksgiving</w>
      <pc unit="sentence" xml:id="B05603-001-a-0090">.</pc>
     </hi>
    </head>
    <opener xml:id="B05603-e40">
     <signed xml:id="B05603-e50">
      <w lemma="william" pos="nn1" rend="hi" xml:id="B05603-001-a-0100">WILLIAM</w>
      <w lemma="and" pos="cc" xml:id="B05603-001-a-0110">and</w>
      <w lemma="MARY" pos="nn1" rend="hi" xml:id="B05603-001-a-0120">MARY</w>
      <w lemma="by" pos="acp" xml:id="B05603-001-a-0130">by</w>
      <w lemma="the" pos="d" xml:id="B05603-001-a-0140">the</w>
      <w lemma="grace" pos="n1" xml:id="B05603-001-a-0150">Grace</w>
      <w lemma="of" pos="acp" xml:id="B05603-001-a-0160">of</w>
      <w lemma="God" pos="nn1" xml:id="B05603-001-a-0170">God</w>
      <pc xml:id="B05603-001-a-0180">,</pc>
      <w lemma="king" pos="n1" xml:id="B05603-001-a-0190">King</w>
      <w lemma="and" pos="cc" xml:id="B05603-001-a-0200">and</w>
      <w lemma="queen" pos="n1" xml:id="B05603-001-a-0210">Queen</w>
      <w lemma="of" pos="acp" xml:id="B05603-001-a-0220">of</w>
      <hi xml:id="B05603-e80">
       <w lemma="great-britain" pos="nn1" xml:id="B05603-001-a-0230">Great-Britain</w>
       <pc xml:id="B05603-001-a-0240">,</pc>
       <w lemma="France" pos="nn1" xml:id="B05603-001-a-0250">France</w>
      </hi>
      <w lemma="and" pos="cc" xml:id="B05603-001-a-0260">and</w>
      <hi xml:id="B05603-e90">
       <w lemma="Ireland" pos="nn1" xml:id="B05603-001-a-0270">Ireland</w>
       <pc xml:id="B05603-001-a-0280">,</pc>
      </hi>
      <w lemma="defender" pos="n2" xml:id="B05603-001-a-0290">Defenders</w>
      <w lemma="of" pos="acp" xml:id="B05603-001-a-0300">of</w>
      <w lemma="the" pos="d" xml:id="B05603-001-a-0310">the</w>
      <w lemma="faith" pos="n1" xml:id="B05603-001-a-0320">Faith</w>
      <pc xml:id="B05603-001-a-0330">;</pc>
     </signed>
     <salute xml:id="B05603-e100">
      <w lemma="to" pos="acp" xml:id="B05603-001-a-0340">To</w>
      <w lemma="our" pos="po" xml:id="B05603-001-a-0350">Our</w>
      <w lemma="lovit" pos="nn2" xml:id="B05603-001-a-0360">Lovits</w>
      <gap extent="1" reason="blank" unit="spans" xml:id="B05603-e110"/>
      <w lemma="macer" pos="n2" xml:id="B05603-001-a-0370">Macers</w>
      <w lemma="of" pos="acp" xml:id="B05603-001-a-0380">of</w>
      <w lemma="our" pos="po" xml:id="B05603-001-a-0390">our</w>
      <w lemma="privy" pos="j" xml:id="B05603-001-a-0400">Privy</w>
      <w lemma="council" pos="n1" xml:id="B05603-001-a-0410">Council</w>
      <pc xml:id="B05603-001-a-0420">,</pc>
      <w lemma="messenger" pos="n2" xml:id="B05603-001-a-0430">Messengers</w>
      <w lemma="at" pos="acp" xml:id="B05603-001-a-0440">at</w>
      <w lemma="arm" pos="n2" xml:id="B05603-001-a-0450">Arms</w>
      <pc xml:id="B05603-001-a-0460">,</pc>
      <w lemma="our" pos="po" xml:id="B05603-001-a-0470">Our</w>
      <w lemma="sheriff" pos="n2" xml:id="B05603-001-a-0480">Sheriffs</w>
      <w lemma="in" pos="acp" xml:id="B05603-001-a-0490">in</w>
      <w lemma="that" pos="d" xml:id="B05603-001-a-0500">that</w>
      <w lemma="part" pos="n1" xml:id="B05603-001-a-0510">part</w>
      <w lemma="conjunct" pos="av-j" xml:id="B05603-001-a-0520">conjunctly</w>
      <w lemma="and" pos="cc" xml:id="B05603-001-a-0530">and</w>
      <w lemma="several" pos="av-j" xml:id="B05603-001-a-0540">severally</w>
      <pc xml:id="B05603-001-a-0550">,</pc>
      <w lemma="special" pos="av-j" xml:id="B05603-001-a-0560">specially</w>
      <w lemma="constitute" pos="vvi" xml:id="B05603-001-a-0570">constitute</w>
      <pc xml:id="B05603-001-a-0580">,</pc>
      <w lemma="greeting" pos="n1" xml:id="B05603-001-a-0590">greeting</w>
      <pc xml:id="B05603-001-a-0600">;</pc>
     </salute>
    </opener>
    <p xml:id="B05603-e130">
     <w lemma="whereas" pos="cs" xml:id="B05603-001-a-0610">Whereas</w>
     <pc xml:id="B05603-001-a-0620">,</pc>
     <w lemma="we" pos="pns" xml:id="B05603-001-a-0630">We</w>
     <w lemma="have" pos="vvb" xml:id="B05603-001-a-0640">have</w>
     <w lemma="think" pos="vvn" xml:id="B05603-001-a-0650">thought</w>
     <w lemma="fit" pos="j" xml:id="B05603-001-a-0660">fit</w>
     <pc xml:id="B05603-001-a-0670">,</pc>
     <w lemma="to" pos="prt" xml:id="B05603-001-a-0680">to</w>
     <w lemma="appoint" pos="vvi" xml:id="B05603-001-a-0690">appoint</w>
     <w lemma="a" pos="d" xml:id="B05603-001-a-0700">a</w>
     <w lemma="solemn" pos="j" xml:id="B05603-001-a-0710">solemn</w>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-0720">and</w>
     <w lemma="public" pos="j" reg="public" xml:id="B05603-001-a-0730">publick</w>
     <w lemma="day" pos="n1" xml:id="B05603-001-a-0740">Day</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-0750">of</w>
     <w lemma="thanksgiving" pos="n1" xml:id="B05603-001-a-0760">Thanksgiving</w>
     <pc xml:id="B05603-001-a-0770">,</pc>
     <w lemma="to" pos="prt" xml:id="B05603-001-a-0780">to</w>
     <w lemma="be" pos="vvi" xml:id="B05603-001-a-0790">be</w>
     <w lemma="keep" pos="vvn" xml:id="B05603-001-a-0800">kept</w>
     <pc xml:id="B05603-001-a-0810">,</pc>
     <w lemma="for" pos="acp" xml:id="B05603-001-a-0820">for</w>
     <w lemma="give" pos="vvg" xml:id="B05603-001-a-0830">giving</w>
     <w lemma="thanks" pos="n2" xml:id="B05603-001-a-0840">Thanks</w>
     <w lemma="to" pos="acp" xml:id="B05603-001-a-0850">to</w>
     <w lemma="almighty" pos="j" xml:id="B05603-001-a-0860">Almighty</w>
     <w lemma="GOD" pos="nn1" xml:id="B05603-001-a-0870">GOD</w>
     <pc xml:id="B05603-001-a-0880">,</pc>
     <w lemma="for" pos="acp" xml:id="B05603-001-a-0890">for</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-0900">the</w>
     <w lemma="great" pos="j" xml:id="B05603-001-a-0910">great</w>
     <w lemma="success" pos="n1" xml:id="B05603-001-a-0920">Success</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-0930">of</w>
     <w lemma="our" pos="po" xml:id="B05603-001-a-0940">our</w>
     <w lemma="arm" pos="n2" xml:id="B05603-001-a-0950">Arms</w>
     <w lemma="in" pos="acp" xml:id="B05603-001-a-0960">in</w>
     <w lemma="our" pos="po" xml:id="B05603-001-a-0970">our</w>
     <w lemma="expedition" pos="n1" xml:id="B05603-001-a-0980">Expedition</w>
     <w lemma="into" pos="acp" xml:id="B05603-001-a-0990">into</w>
     <w lemma="our" pos="po" xml:id="B05603-001-a-1000">our</w>
     <w lemma="kingdom" pos="n1" xml:id="B05603-001-a-1010">Kingdom</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-1020">of</w>
     <hi xml:id="B05603-e140">
      <w lemma="Ireland" pos="nn1" xml:id="B05603-001-a-1030">Ireland</w>
      <pc xml:id="B05603-001-a-1040">,</pc>
     </hi>
     <w lemma="against" pos="acp" xml:id="B05603-001-a-1050">against</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-1060">the</w>
     <w lemma="enemy" pos="n2" xml:id="B05603-001-a-1070">Enemies</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-1080">of</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-1090">the</w>
     <hi xml:id="B05603-e150">
      <w lemma="protestant" pos="jnn" xml:id="B05603-001-a-1100">Protestant</w>
      <w lemma="religion" pos="n1" xml:id="B05603-001-a-1110">Religion</w>
      <pc xml:id="B05603-001-a-1120">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-1130">and</w>
     <w lemma="our" pos="po" xml:id="B05603-001-a-1140">Our</w>
     <w lemma="government" pos="n1" xml:id="B05603-001-a-1150">Government</w>
     <pc xml:id="B05603-001-a-1160">,</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-1170">and</w>
     <w lemma="for" pos="acp" xml:id="B05603-001-a-1180">for</w>
     <w lemma="our" pos="po" xml:id="B05603-001-a-1190">Our</w>
     <w lemma="safe" pos="j" xml:id="B05603-001-a-1200">Safe</w>
     <w lemma="return" pos="n1" xml:id="B05603-001-a-1210">Return</w>
     <pc unit="sentence" xml:id="B05603-001-a-1220">.</pc>
     <w lemma="therefore" pos="av" xml:id="B05603-001-a-1230">Therefore</w>
     <pc xml:id="B05603-001-a-1240">,</pc>
     <w lemma="we" pos="pns" xml:id="B05603-001-a-1250">We</w>
     <w lemma="with" pos="acp" xml:id="B05603-001-a-1260">with</w>
     <w lemma="advice" pos="n1" xml:id="B05603-001-a-1270">Advice</w>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-1280">and</w>
     <w lemma="consent" pos="n1" xml:id="B05603-001-a-1290">Consent</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-1300">of</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-1310">the</w>
     <w lemma="lord" pos="n2" xml:id="B05603-001-a-1320">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-1330">of</w>
     <w lemma="our" pos="po" xml:id="B05603-001-a-1340">Our</w>
     <w lemma="privy" pos="j" xml:id="B05603-001-a-1350">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05603-001-a-1360">Council</w>
     <pc xml:id="B05603-001-a-1370">,</pc>
     <w lemma="do" pos="vvb" xml:id="B05603-001-a-1380">do</w>
     <w lemma="hereby" pos="av" xml:id="B05603-001-a-1390">hereby</w>
     <w lemma="indict" pos="vvi" xml:id="B05603-001-a-1400">Indict</w>
     <pc xml:id="B05603-001-a-1410">,</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-1420">and</w>
     <w lemma="appoint" pos="vvb" xml:id="B05603-001-a-1430">Appoint</w>
     <w lemma="a" pos="d" xml:id="B05603-001-a-1440">a</w>
     <w lemma="day" pos="n1" xml:id="B05603-001-a-1450">Day</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-1460">of</w>
     <w lemma="solemn" pos="j" xml:id="B05603-001-a-1470">solemn</w>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-1480">and</w>
     <w lemma="public" pos="j" reg="public" xml:id="B05603-001-a-1490">publick</w>
     <w lemma="thanksgiving" pos="n1" xml:id="B05603-001-a-1500">Thanksgiving</w>
     <pc xml:id="B05603-001-a-1510">,</pc>
     <w lemma="to" pos="prt" xml:id="B05603-001-a-1520">to</w>
     <w lemma="be" pos="vvi" xml:id="B05603-001-a-1530">be</w>
     <w lemma="religious" pos="av-j" xml:id="B05603-001-a-1540">Religiously</w>
     <w lemma="observe" pos="vvn" xml:id="B05603-001-a-1550">Observed</w>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-1560">and</w>
     <w lemma="keep" pos="vvn" xml:id="B05603-001-a-1570">Kept</w>
     <w lemma="in" pos="acp" xml:id="B05603-001-a-1580">in</w>
     <w lemma="all" pos="d" xml:id="B05603-001-a-1590">all</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-1600">the</w>
     <w lemma="church" pos="n2" xml:id="B05603-001-a-1610">Churches</w>
     <w lemma="&amp;" pos="cc" xml:id="B05603-001-a-1620">&amp;</w>
     <w lemma="meeting-house" pos="n2" xml:id="B05603-001-a-1630">Meeting-houses</w>
     <w lemma="within" pos="acp" xml:id="B05603-001-a-1640">within</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-1650">the</w>
     <w lemma="city" pos="n1" xml:id="B05603-001-a-1660">City</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-1670">of</w>
     <hi xml:id="B05603-e160">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05603-001-a-1680">Edinburgh</w>
      <pc xml:id="B05603-001-a-1690">,</pc>
     </hi>
     <w lemma="&amp;" pos="cc" xml:id="B05603-001-a-1700">&amp;</w>
     <w lemma="in" pos="acp" xml:id="B05603-001-a-1710">in</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-1720">the</w>
     <w lemma="shire" pos="n2" xml:id="B05603-001-a-1730">Shires</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-1740">of</w>
     <hi xml:id="B05603-e170">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05603-001-a-1750">Edinburgh</w>
      <pc xml:id="B05603-001-a-1760">,</pc>
      <w lemma="Haddingtoun" pos="nn1" xml:id="B05603-001-a-1770">Haddingtoun</w>
      <w lemma="&amp;" pos="cc" xml:id="B05603-001-a-1780">&amp;</w>
      <w lemma="linlithgow" pos="nn1" xml:id="B05603-001-a-1790">Linlithgow</w>
     </hi>
     <w lemma="upon" pos="acp" xml:id="B05603-001-a-1800">upon</w>
     <w lemma="Sunday" pos="nn1" xml:id="B05603-001-a-1810">Sunday</w>
     <w lemma="next" pos="ord" xml:id="B05603-001-a-1820">next</w>
     <pc xml:id="B05603-001-a-1830">,</pc>
     <w lemma="the" pos="d" xml:id="B05603-001-a-1840">the</w>
     <w lemma="twenty" pos="crd" reg="twenty" xml:id="B05603-001-a-1850">twentie</w>
     <w lemma="one" pos="crd" xml:id="B05603-001-a-1860">one</w>
     <w lemma="day" pos="n1" xml:id="B05603-001-a-1870">day</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-1880">of</w>
     <w lemma="September" pos="nn1" rend="hi" xml:id="B05603-001-a-1890">September</w>
     <w lemma="current" pos="n1" xml:id="B05603-001-a-1900">current</w>
     <pc xml:id="B05603-001-a-1910">;</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-1920">and</w>
     <w lemma="in" pos="acp" xml:id="B05603-001-a-1930">in</w>
     <w lemma="all" pos="d" xml:id="B05603-001-a-1940">all</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-1950">the</w>
     <w lemma="church" pos="n2" xml:id="B05603-001-a-1960">Churches</w>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-1970">and</w>
     <w lemma="meeting-house" pos="n2" xml:id="B05603-001-a-1980">Meeting-houses</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-1990">of</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-2000">the</w>
     <w lemma="other" pos="d" xml:id="B05603-001-a-2010">other</w>
     <w lemma="shire" pos="n2" xml:id="B05603-001-a-2020">Shires</w>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-2030">and</w>
     <w lemma="burgh" pos="nng1" reg="burgh's" xml:id="B05603-001-a-2040">Burghs</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-2050">of</w>
     <w lemma="this" pos="d" xml:id="B05603-001-a-2060">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05603-001-a-2070">Kingdom</w>
     <pc xml:id="B05603-001-a-2080">,</pc>
     <w lemma="upon" pos="acp" xml:id="B05603-001-a-2090">upon</w>
     <w lemma="Sunday" pos="nn1" xml:id="B05603-001-a-2100">Sunday</w>
     <pc xml:id="B05603-001-a-2110">,</pc>
     <w lemma="the" pos="d" xml:id="B05603-001-a-2120">the</w>
     <w lemma="five" pos="ord" xml:id="B05603-001-a-2130">fifth</w>
     <w lemma="day" pos="n1" xml:id="B05603-001-a-2140">Day</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-2150">of</w>
     <w lemma="October" pos="nn1" rend="hi" xml:id="B05603-001-a-2160">October</w>
     <w lemma="next" pos="ord" xml:id="B05603-001-a-2170">next</w>
     <pc xml:id="B05603-001-a-2180">:</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-2190">And</w>
     <w lemma="ordain" pos="vvz" xml:id="B05603-001-a-2200">Ordains</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-2210">the</w>
     <w lemma="minister" pos="n2" xml:id="B05603-001-a-2220">Ministers</w>
     <w lemma="in" pos="acp" xml:id="B05603-001-a-2230">in</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-2240">the</w>
     <w lemma="say" pos="ng1-vn" reg="said's" xml:id="B05603-001-a-2250">saids</w>
     <w lemma="other" pos="d" xml:id="B05603-001-a-2260">other</w>
     <w lemma="shire" pos="n2" xml:id="B05603-001-a-2270">Shires</w>
     <pc xml:id="B05603-001-a-2280">,</pc>
     <w lemma="to" pos="prt" xml:id="B05603-001-a-2290">to</w>
     <w lemma="cause" pos="vvi" xml:id="B05603-001-a-2300">cause</w>
     <w lemma="read" pos="vvn" xml:id="B05603-001-a-2310">Read</w>
     <pc xml:id="B05603-001-a-2320">,</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-2330">and</w>
     <w lemma="make" pos="vvi" xml:id="B05603-001-a-2340">make</w>
     <w lemma="intimation" pos="n1" xml:id="B05603-001-a-2350">Intimation</w>
     <w lemma="hereof" pos="av" xml:id="B05603-001-a-2360">hereof</w>
     <w lemma="upon" pos="acp" xml:id="B05603-001-a-2370">upon</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-2380">the</w>
     <w lemma="Sunday" pos="nn1" xml:id="B05603-001-a-2390">Sunday</w>
     <w lemma="precede" pos="vvg" reg="preceding" xml:id="B05603-001-a-2400">preceeding</w>
     <pc xml:id="B05603-001-a-2410">,</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-2420">and</w>
     <w lemma="that" pos="cs" xml:id="B05603-001-a-2430">that</w>
     <w lemma="all" pos="d" xml:id="B05603-001-a-2440">all</w>
     <w lemma="person" pos="n2" xml:id="B05603-001-a-2450">Persons</w>
     <w lemma="give" pos="vvb" xml:id="B05603-001-a-2460">give</w>
     <w lemma="punctual" pos="j" xml:id="B05603-001-a-2470">punctual</w>
     <w lemma="obedience" pos="n1" xml:id="B05603-001-a-2480">Obedience</w>
     <w lemma="hereunto" pos="av" xml:id="B05603-001-a-2490">hereunto</w>
     <pc xml:id="B05603-001-a-2500">,</pc>
     <w lemma="as" pos="acp" xml:id="B05603-001-a-2510">as</w>
     <w lemma="they" pos="pns" xml:id="B05603-001-a-2520">they</w>
     <w lemma="will" pos="vmb" xml:id="B05603-001-a-2530">will</w>
     <w lemma="be" pos="vvi" xml:id="B05603-001-a-2540">be</w>
     <w lemma="answerable" pos="j" xml:id="B05603-001-a-2550">answerable</w>
     <w lemma="at" pos="acp" xml:id="B05603-001-a-2560">at</w>
     <w lemma="their" pos="po" xml:id="B05603-001-a-2570">their</w>
     <w lemma="high" pos="js" xml:id="B05603-001-a-2580">highest</w>
     <w lemma="peril" pos="n1" reg="Peril" xml:id="B05603-001-a-2590">Perril</w>
     <pc unit="sentence" xml:id="B05603-001-a-2600">.</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-2610">And</w>
     <w lemma="we" orig="VVe" pos="pns" xml:id="B05603-001-a-2620">We</w>
     <w lemma="require" pos="vvb" xml:id="B05603-001-a-2630">require</w>
     <w lemma="our" pos="po" xml:id="B05603-001-a-2640">Our</w>
     <w lemma="solicitor" pos="n1" xml:id="B05603-001-a-2650">Solicitor</w>
     <pc xml:id="B05603-001-a-2660">,</pc>
     <w lemma="in" pos="acp" xml:id="B05603-001-a-2670">in</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-2680">the</w>
     <w lemma="most" pos="avs-d" xml:id="B05603-001-a-2690">most</w>
     <w lemma="convenient" pos="j" xml:id="B05603-001-a-2700">convenient</w>
     <w lemma="way" orig="VVay" pos="n1" xml:id="B05603-001-a-2710">Way</w>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-2720">and</w>
     <w lemma="method" pos="n1" xml:id="B05603-001-a-2730">Method</w>
     <w lemma="to" pos="prt" xml:id="B05603-001-a-2740">to</w>
     <w lemma="dispatch" pos="vvi" xml:id="B05603-001-a-2750">dispatch</w>
     <pc xml:id="B05603-001-a-2760">,</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-2770">and</w>
     <w lemma="send" pos="vvb" xml:id="B05603-001-a-2780">send</w>
     <w lemma="print" pos="vvn" xml:id="B05603-001-a-2790">printed</w>
     <w lemma="copy" pos="n2" reg="Copies" xml:id="B05603-001-a-2800">Coppies</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-2810">of</w>
     <w lemma="this" pos="d" xml:id="B05603-001-a-2820">this</w>
     <w lemma="our" pos="po" xml:id="B05603-001-a-2830">Our</w>
     <w lemma="proclamation" pos="n1" xml:id="B05603-001-a-2840">Proclamation</w>
     <pc xml:id="B05603-001-a-2850">,</pc>
     <w lemma="to" pos="prt" xml:id="B05603-001-a-2860">to</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-2870">the</w>
     <w lemma="sheriff" pos="n2" xml:id="B05603-001-a-2880">Sheriffs</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-2890">of</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-2900">the</w>
     <w lemma="several" pos="j" xml:id="B05603-001-a-2910">several</w>
     <w lemma="shire" pos="n2" xml:id="B05603-001-a-2920">Shires</w>
     <pc xml:id="B05603-001-a-2930">,</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-2940">and</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-2950">the</w>
     <w lemma="Stewart" pos="nng1" xml:id="B05603-001-a-2960">Stewarts</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-2970">of</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-2980">the</w>
     <w lemma="stwartry" pos="n2" xml:id="B05603-001-a-2990">Stwartries</w>
     <pc xml:id="B05603-001-a-3000">,</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-3010">and</w>
     <w lemma="their" pos="po" xml:id="B05603-001-a-3020">their</w>
     <w lemma="deputy" pos="n2" xml:id="B05603-001-a-3030">Deputs</w>
     <pc xml:id="B05603-001-a-3040">,</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-3050">and</w>
     <w lemma="clerk" pos="n2" xml:id="B05603-001-a-3060">Clerks</w>
     <pc xml:id="B05603-001-a-3070">,</pc>
     <w lemma="who" pos="crq" xml:id="B05603-001-a-3080">whom</w>
     <w lemma="we" orig="VVe" pos="pns" xml:id="B05603-001-a-3090">We</w>
     <w lemma="ordain" pos="vvb" xml:id="B05603-001-a-3100">ordain</w>
     <w lemma="to" pos="acp" xml:id="B05603-001-a-3110">to</w>
     <w lemma="cause" pos="n1" xml:id="B05603-001-a-3120">cause</w>
     <w lemma="publish" pos="vvb" xml:id="B05603-001-a-3130">Publish</w>
     <pc xml:id="B05603-001-a-3140">,</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-3150">and</w>
     <w lemma="immediate" pos="av-j" reg="immediately" xml:id="B05603-001-a-3160">immediatly</w>
     <w lemma="transmit" pos="vvi" xml:id="B05603-001-a-3170">transmit</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-3180">the</w>
     <w lemma="famine" pos="n1" xml:id="B05603-001-a-3190">famine</w>
     <w lemma="to" pos="acp" xml:id="B05603-001-a-3200">to</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-3210">the</w>
     <w lemma="minister" pos="n2" xml:id="B05603-001-a-3220">Ministers</w>
     <w lemma="in" pos="acp" xml:id="B05603-001-a-3230">in</w>
     <w lemma="all" pos="d" xml:id="B05603-001-a-3240">all</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-3250">the</w>
     <w lemma="church" pos="n2" xml:id="B05603-001-a-3260">Churches</w>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-3270">and</w>
     <w lemma="meeting-house" pos="n2" xml:id="B05603-001-a-3280">Meeting-houses</w>
     <w lemma="within" pos="acp" xml:id="B05603-001-a-3290">within</w>
     <w lemma="their" pos="po" xml:id="B05603-001-a-3300">their</w>
     <w lemma="respective" pos="j" xml:id="B05603-001-a-3310">respective</w>
     <w lemma="jurisdiction" pos="n2" xml:id="B05603-001-a-3320">Jurisdictions</w>
     <pc unit="sentence" xml:id="B05603-001-a-3330">.</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-3340">And</w>
     <w lemma="ordain" pos="vvz" xml:id="B05603-001-a-3350">ordains</w>
     <w lemma="their" pos="po" reg="their" xml:id="B05603-001-a-3360">thir</w>
     <w lemma="present" pos="n2" xml:id="B05603-001-a-3370">presents</w>
     <w lemma="to" pos="prt" xml:id="B05603-001-a-3380">to</w>
     <w lemma="be" pos="vvi" xml:id="B05603-001-a-3390">be</w>
     <w lemma="print" pos="vvn" xml:id="B05603-001-a-3400">Printed</w>
     <pc xml:id="B05603-001-a-3410">,</pc>
     <w lemma="&amp;" pos="cc" xml:id="B05603-001-a-3420">&amp;</w>
     <w lemma="publish" pos="vvn" xml:id="B05603-001-a-3430">Published</w>
     <w lemma="at" pos="acp" xml:id="B05603-001-a-3440">at</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-3450">the</w>
     <w lemma="mercat-cross" pos="j" xml:id="B05603-001-a-3460">Mercat-cross</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-3470">of</w>
     <hi xml:id="B05603-e200">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05603-001-a-3480">Edinburgh</w>
      <pc xml:id="B05603-001-a-3490">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-3500">and</w>
     <w lemma="at" pos="acp" xml:id="B05603-001-a-3510">at</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-3520">the</w>
     <w lemma="mercat-cross" pos="n2" xml:id="B05603-001-a-3530">Mercat-crosses</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-3540">of</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-3550">the</w>
     <w lemma="headburgh" pos="n2" xml:id="B05603-001-a-3560">Head-burghs</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-3570">of</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-3580">the</w>
     <w lemma="several" pos="j" xml:id="B05603-001-a-3590">several</w>
     <w lemma="shire" pos="n2" xml:id="B05603-001-a-3600">Shires</w>
     <pc xml:id="B05603-001-a-3610">,</pc>
     <w lemma="and" pos="cc" xml:id="B05603-001-a-3620">and</w>
     <w lemma="stewartry" pos="n2" xml:id="B05603-001-a-3630">Stewartries</w>
     <w lemma="within" pos="acp" xml:id="B05603-001-a-3640">within</w>
     <w lemma="this" pos="d" xml:id="B05603-001-a-3650">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05603-001-a-3660">Kingdom</w>
     <pc xml:id="B05603-001-a-3670">,</pc>
     <w lemma="that" pos="cs" xml:id="B05603-001-a-3680">that</w>
     <w lemma="none" pos="pix" xml:id="B05603-001-a-3690">none</w>
     <w lemma="may" pos="vmb" xml:id="B05603-001-a-3700">may</w>
     <w lemma="pretend" pos="vvi" xml:id="B05603-001-a-3710">pretend</w>
     <w lemma="ignorance" pos="n1" xml:id="B05603-001-a-3720">Ignorance</w>
     <pc unit="sentence" xml:id="B05603-001-a-3730">.</pc>
    </p>
    <closer xml:id="B05603-e210">
     <dateline xml:id="B05603-e220">
      <w lemma="give" pos="vvn" xml:id="B05603-001-a-3740">Given</w>
      <w lemma="under" pos="acp" xml:id="B05603-001-a-3750">under</w>
      <w lemma="our" pos="po" xml:id="B05603-001-a-3760">Our</w>
      <w lemma="signet" pos="n1" xml:id="B05603-001-a-3770">Signet</w>
      <pc xml:id="B05603-001-a-3780">,</pc>
      <w lemma="at" pos="acp" xml:id="B05603-001-a-3790">at</w>
      <hi xml:id="B05603-e230">
       <w lemma="holy-rood-house" pos="n1" xml:id="B05603-001-a-3800">Holy-rood-house</w>
       <pc xml:id="B05603-001-a-3810">,</pc>
      </hi>
      <date xml:id="B05603-e240">
       <w lemma="the" pos="d" xml:id="B05603-001-a-3820">the</w>
       <w lemma="seventeen" pos="ord" xml:id="B05603-001-a-3830">seventeenth</w>
       <w lemma="day" pos="n1" xml:id="B05603-001-a-3840">day</w>
       <w lemma="of" pos="acp" xml:id="B05603-001-a-3850">of</w>
       <hi xml:id="B05603-e250">
        <w lemma="September" pos="nn1" xml:id="B05603-001-a-3860">September</w>
        <pc xml:id="B05603-001-a-3870">,</pc>
       </hi>
       <w lemma="one" pos="crd" xml:id="B05603-001-a-3880">one</w>
       <w lemma="thousand" pos="crd" xml:id="B05603-001-a-3890">thousand</w>
       <w lemma="six" pos="crd" xml:id="B05603-001-a-3900">six</w>
       <w lemma="hundred" pos="crd" xml:id="B05603-001-a-3910">hundred</w>
       <w lemma="and" pos="cc" xml:id="B05603-001-a-3920">and</w>
       <w lemma="ninety" pos="crd" xml:id="B05603-001-a-3930">ninety</w>
       <pc xml:id="B05603-001-a-3940">,</pc>
       <w lemma="and" pos="cc" xml:id="B05603-001-a-3950">and</w>
       <w lemma="of" pos="acp" xml:id="B05603-001-a-3960">of</w>
       <w lemma="our" pos="po" xml:id="B05603-001-a-3970">Our</w>
       <w lemma="reign" pos="n1" xml:id="B05603-001-a-3980">Reign</w>
       <w lemma="the" pos="d" xml:id="B05603-001-a-3990">the</w>
       <w lemma="second" pos="ord" xml:id="B05603-001-a-4000">second</w>
       <w lemma="year" pos="n1" xml:id="B05603-001-a-4010">Year</w>
       <pc unit="sentence" xml:id="B05603-001-a-4020">.</pc>
      </date>
     </dateline>
     <signed xml:id="B05603-e260">
      <hi xml:id="B05603-e270">
       <w lemma="n/a" pos="fla" xml:id="B05603-001-a-4030">Per</w>
       <w lemma="n/a" pos="fla" xml:id="B05603-001-a-4040">Actum</w>
       <w lemma="n/a" pos="fla" xml:id="B05603-001-a-4050">Dominorum</w>
       <w lemma="sti." pos="ab" xml:id="B05603-001-a-4060">Sti.</w>
       <w lemma="n/a" pos="fla" xml:id="B05603-001-a-4070">Concilii</w>
       <pc unit="sentence" xml:id="B05603-001-a-4080">.</pc>
      </hi>
      <w lemma="gilb." pos="ab" xml:id="B05603-001-a-4090">Gilb.</w>
      <w lemma="eliot" pos="nn1" xml:id="B05603-001-a-4100">Eliot</w>
      <pc unit="sentence" xml:id="B05603-001-a-4110">.</pc>
      <hi xml:id="B05603-e280">
       <w lemma="Cls." pos="nn1" xml:id="B05603-001-a-4120">Cls.</w>
       <w lemma="sti." pos="ab" xml:id="B05603-001-a-4130">Sti.</w>
       <w lemma="n/a" pos="fla" xml:id="B05603-001-a-4140">Concilii</w>
       <pc unit="sentence" xml:id="B05603-001-a-4150">.</pc>
      </hi>
     </signed>
     <lb xml:id="B05603-e290"/>
     <hi xml:id="B05603-e300">
      <w lemma="God" pos="nn1" xml:id="B05603-001-a-4160">God</w>
      <w lemma="save" pos="acp" xml:id="B05603-001-a-4170">save</w>
      <w lemma="king" pos="n1" xml:id="B05603-001-a-4180">King</w>
     </hi>
     <w lemma="William" orig="VVilliam" pos="nn1" xml:id="B05603-001-a-4190">William</w>
     <hi xml:id="B05603-e310">
      <w lemma="and" pos="cc" xml:id="B05603-001-a-4200">and</w>
      <w lemma="queen" pos="n1" xml:id="B05603-001-a-4210">Queen</w>
     </hi>
     <w lemma="Mary" pos="nn1" xml:id="B05603-001-a-4220">Mary</w>
     <pc unit="sentence" xml:id="B05603-001-a-4230">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="B05603-e320">
   <div type="colophon" xml:id="B05603-e330">
    <p xml:id="B05603-e340">
     <hi xml:id="B05603-e350">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05603-001-a-4240">Edinburgh</w>
      <pc xml:id="B05603-001-a-4250">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="B05603-001-a-4260">Printed</w>
     <w lemma="by" pos="acp" xml:id="B05603-001-a-4270">by</w>
     <w lemma="the" pos="d" xml:id="B05603-001-a-4280">the</w>
     <w lemma="heir" pos="n1" xml:id="B05603-001-a-4290">Heir</w>
     <w lemma="of" pos="acp" xml:id="B05603-001-a-4300">of</w>
     <hi xml:id="B05603-e360">
      <w lemma="Andrew" pos="nn1" xml:id="B05603-001-a-4310">Andrew</w>
      <w lemma="Anderson" pos="nn1" xml:id="B05603-001-a-4320">Anderson</w>
      <pc xml:id="B05603-001-a-4330">,</pc>
     </hi>
     <w lemma="printer" pos="n1" xml:id="B05603-001-a-4340">Printer</w>
     <w lemma="to" pos="acp" xml:id="B05603-001-a-4350">to</w>
     <w lemma="their" pos="po" xml:id="B05603-001-a-4360">Their</w>
     <w lemma="most" pos="avs-d" xml:id="B05603-001-a-4370">most</w>
     <w lemma="excellent" pos="j" xml:id="B05603-001-a-4380">Excellent</w>
     <w lemma="majesty" pos="n2" reg="Majesties" xml:id="B05603-001-a-4390">Majestys</w>
     <pc xml:id="B05603-001-a-4400">,</pc>
     <w lemma="1690." pos="crd" xml:id="B05603-001-a-4410">1690.</w>
     <pc unit="sentence" xml:id="B05603-001-a-4420"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
