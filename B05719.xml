<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B05719">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A proclamation, prohibiting the exportation of victual furth of this kingdom.</title>
    <author>Scotland. Privy Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B05719 of text R233216 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing S1968). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B05719</idno>
    <idno type="STC">Wing S1968</idno>
    <idno type="STC">ESTC R233216</idno>
    <idno type="EEBO-CITATION">52529320</idno>
    <idno type="OCLC">ocm 52529320</idno>
    <idno type="VID">179108</idno>
    <idno type="PROQUESTGOID">2240879343</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B05719)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179108)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2777:1)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A proclamation, prohibiting the exportation of victual furth of this kingdom.</title>
      <author>Scotland. Privy Council.</author>
      <author>Scotland. Sovereign (1694-1702 : William II)</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by the heirs and successors of Andrew Anderson, Printer to the Kings most excellent Majesty,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>Anno Dom. 1698.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Initial letter.</note>
      <note>Intentional blank spaces in text.</note>
      <note>Dated: Given under Our Signet at Edinburgh the tenth day of May, and of Our Reign the tenth year 1698.</note>
      <note>Signed: Gilb. Eliot. Cls. Sti. Concilii.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Food law and legislation -- Scotland -- Early works to 1800.</term>
     <term>Export controls -- Scotland -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A proclamation, prohibiting the exportation of victual furth of this kingdom.</ep:title>
    <ep:author>Scotland. Privy Council</ep:author>
    <ep:publicationYear>1698</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>449</ep:wordCount>
    <ep:defectiveTokenCount>3</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>1</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>289.53</ep:defectRate>
    <ep:finalGrade>F</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 289.53 defects per 10,000 words puts this text in the F category of texts with  100 or more defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-01</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-05</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-06</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-06</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B05719-t">
  <body xml:id="B05719-e0">
   <div type="royal_proclamation" xml:id="B05719-e10">
    <pb facs="tcp:179108:1" rend="simple:additions" xml:id="B05719-001-a"/>
    <head xml:id="B05719-e20">
     <hi xml:id="B05719-e30">
      <w lemma="a" pos="d" xml:id="B05719-001-a-0010">A</w>
      <w lemma="proclamation" pos="n1" xml:id="B05719-001-a-0020">PROCLAMATION</w>
      <pc xml:id="B05719-001-a-0030">,</pc>
     </hi>
     <w lemma="prohibit" pos="vvg" xml:id="B05719-001-a-0040">Prohibiting</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-0050">the</w>
     <w lemma="exportation" pos="n1" xml:id="B05719-001-a-0060">Exportation</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-0070">of</w>
     <w lemma="victual" pos="n1" xml:id="B05719-001-a-0080">Victual</w>
     <w lemma="forth" pos="av" reg="forth" xml:id="B05719-001-a-0090">furth</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-0100">of</w>
     <w lemma="this" pos="d" xml:id="B05719-001-a-0110">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05719-001-a-0120">Kingdom</w>
     <pc unit="sentence" xml:id="B05719-001-a-0130">.</pc>
    </head>
    <opener xml:id="B05719-e40">
     <signed xml:id="B05719-e50">
      <w lemma="william" pos="nn1" rend="hi" xml:id="B05719-001-a-0140">WILLIAM</w>
      <w lemma="by" pos="acp" xml:id="B05719-001-a-0150">by</w>
      <w lemma="the" pos="d" xml:id="B05719-001-a-0160">the</w>
      <w lemma="grace" pos="n1" xml:id="B05719-001-a-0170">Grace</w>
      <w lemma="of" pos="acp" xml:id="B05719-001-a-0180">of</w>
      <w lemma="GOD" pos="nn1" xml:id="B05719-001-a-0190">GOD</w>
      <pc xml:id="B05719-001-a-0200">,</pc>
      <w lemma="king" pos="n1" xml:id="B05719-001-a-0210">King</w>
      <w lemma="of" pos="acp" xml:id="B05719-001-a-0220">of</w>
      <hi xml:id="B05719-e70">
       <w lemma="great-britain" pos="nn1" xml:id="B05719-001-a-0230">Great-Britain</w>
       <pc xml:id="B05719-001-a-0240">,</pc>
       <w lemma="France" pos="nn1" xml:id="B05719-001-a-0250">France</w>
       <pc xml:id="B05719-001-a-0260">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="B05719-001-a-0270">and</w>
      <hi xml:id="B05719-e80">
       <w lemma="Ireland" pos="nn1" xml:id="B05719-001-a-0280">Ireland</w>
       <pc xml:id="B05719-001-a-0290">,</pc>
      </hi>
      <w lemma="defender" pos="n1" xml:id="B05719-001-a-0300">Defender</w>
      <w lemma="of" pos="acp" xml:id="B05719-001-a-0310">of</w>
      <w lemma="the" pos="d" xml:id="B05719-001-a-0320">the</w>
      <w lemma="faith" pos="n1" xml:id="B05719-001-a-0330">Faith</w>
      <pc xml:id="B05719-001-a-0340">;</pc>
     </signed>
     <salute xml:id="B05719-e90">
      <w lemma="to" pos="prt" xml:id="B05719-001-a-0350">To</w>
      <gap extent="1" reason="blank" unit="spans" xml:id="B05719-e100"/>
      <w lemma="macer" pos="n2" xml:id="B05719-001-a-0360">Macers</w>
      <w lemma="of" pos="acp" xml:id="B05719-001-a-0370">of</w>
      <w lemma="our" pos="po" xml:id="B05719-001-a-0380">Our</w>
      <w lemma="privy" pos="j" xml:id="B05719-001-a-0390">Privy</w>
      <w lemma="council" pos="n1" xml:id="B05719-001-a-0400">Council</w>
      <pc xml:id="B05719-001-a-0410">,</pc>
      <w lemma="messenger" pos="n2" xml:id="B05719-001-a-0420">Messengers</w>
      <w lemma="at" pos="acp" xml:id="B05719-001-a-0430">at</w>
      <w lemma="arm" pos="n2" xml:id="B05719-001-a-0440">Arms</w>
      <pc xml:id="B05719-001-a-0450">,</pc>
      <w lemma="our" pos="po" xml:id="B05719-001-a-0460">Our</w>
      <w lemma="sheriff" pos="n2" xml:id="B05719-001-a-0470">Sheriffs</w>
      <w lemma="in" pos="acp" xml:id="B05719-001-a-0480">in</w>
      <w lemma="that" pos="d" xml:id="B05719-001-a-0490">that</w>
      <w lemma="part" pos="n1" xml:id="B05719-001-a-0500">Part</w>
      <pc xml:id="B05719-001-a-0510">,</pc>
      <w lemma="conjunct" pos="av-j" xml:id="B05719-001-a-0520">conjunctly</w>
      <w lemma="and" pos="cc" xml:id="B05719-001-a-0530">and</w>
      <w lemma="several" pos="av-j" xml:id="B05719-001-a-0540">severally</w>
      <pc xml:id="B05719-001-a-0550">,</pc>
      <w lemma="special" pos="av-j" xml:id="B05719-001-a-0560">specially</w>
      <w lemma="constitute" pos="vvi" xml:id="B05719-001-a-0570">Constitute</w>
      <pc xml:id="B05719-001-a-0580">,</pc>
      <w lemma="greeting" pos="n1" xml:id="B05719-001-a-0590">Greeting</w>
      <pc xml:id="B05719-001-a-0600">:</pc>
     </salute>
    </opener>
    <p xml:id="B05719-e120">
     <w lemma="n/a" pos="fes" xml:id="B05719-001-a-0610">FORASMUCHAS</w>
     <pc xml:id="B05719-001-a-0620">,</pc>
     <w lemma="the" pos="d" xml:id="B05719-001-a-0630">the</w>
     <w lemma="present" pos="j" xml:id="B05719-001-a-0640">present</w>
     <w lemma="cold" pos="j" xml:id="B05719-001-a-0650">cold</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-0660">and</w>
     <w lemma="backward" pos="av-j" xml:id="B05719-001-a-0670">backward</w>
     <w lemma="season" pos="n1" xml:id="B05719-001-a-0680">Season</w>
     <pc xml:id="B05719-001-a-0690">,</pc>
     <w lemma="do" pos="vvz" xml:id="B05719-001-a-0700">doth</w>
     <w lemma="threaten" pos="vvi" xml:id="B05719-001-a-0710">threaten</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-0720">the</w>
     <w lemma="scarcity" pos="n1" xml:id="B05719-001-a-0730">Scarcity</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-0740">of</w>
     <w lemma="victual" pos="n1" xml:id="B05719-001-a-0750">Victual</w>
     <pc xml:id="B05719-001-a-0760">,</pc>
     <w lemma="so" pos="av" xml:id="B05719-001-a-0770">so</w>
     <w lemma="that" pos="cs" xml:id="B05719-001-a-0780">that</w>
     <w lemma="it" pos="pn" xml:id="B05719-001-a-0790">it</w>
     <w lemma="appear" pos="vvz" xml:id="B05719-001-a-0800">appears</w>
     <w lemma="necessary" pos="j" xml:id="B05719-001-a-0810">necessary</w>
     <w lemma="that" pos="cs" xml:id="B05719-001-a-0820">that</w>
     <w lemma="there" pos="av" xml:id="B05719-001-a-0830">there</w>
     <w lemma="be" pos="vvi" xml:id="B05719-001-a-0840">be</w>
     <w lemma="a" pos="d" xml:id="B05719-001-a-0850">a</w>
     <w lemma="restraint" pos="n1" xml:id="B05719-001-a-0860">Restraint</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-0870">and</w>
     <w lemma="prohibition" pos="n1" xml:id="B05719-001-a-0880">Prohibition</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-0890">of</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-0900">the</w>
     <w lemma="export" pos="n1" xml:id="B05719-001-a-0910">Export</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-0920">of</w>
     <w lemma="victual" pos="n1" xml:id="B05719-001-a-0930">Victual</w>
     <pc xml:id="B05719-001-a-0940">,</pc>
     <w lemma="at" pos="acp" xml:id="B05719-001-a-0950">at</w>
     <w lemma="least" pos="ds" xml:id="B05719-001-a-0960">least</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-0970">of</w>
     <w lemma="meal" pos="n1" xml:id="B05719-001-a-0980">Meal</w>
     <pc xml:id="B05719-001-a-0990">,</pc>
     <w lemma="oat" pos="n2" xml:id="B05719-001-a-1000">Oats</w>
     <pc xml:id="B05719-001-a-1010">,</pc>
     <w lemma="pease" pos="n1" xml:id="B05719-001-a-1020">Pease</w>
     <pc xml:id="B05719-001-a-1030">,</pc>
     <w lemma="barley" pos="n1" xml:id="B05719-001-a-1040">Barley</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-1050">and</w>
     <w lemma="bear" pos="n1" xml:id="B05719-001-a-1060">Bear</w>
     <pc xml:id="B05719-001-a-1070">,</pc>
     <w lemma="whereby" pos="crq" xml:id="B05719-001-a-1080">whereby</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-1090">the</w>
     <w lemma="foresay" pos="j-vn" xml:id="B05719-001-a-1100">foresaid</w>
     <w lemma="threaten" pos="j-vn" reg="threatened" xml:id="B05719-001-a-1110">threatned</w>
     <w lemma="scarcity" pos="n1" xml:id="B05719-001-a-1120">Scarcity</w>
     <w lemma="may" pos="vmb" xml:id="B05719-001-a-1130">may</w>
     <w lemma="be" pos="vvi" xml:id="B05719-001-a-1140">be</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-1150">the</w>
     <w lemma="better" pos="avc-j" xml:id="B05719-001-a-1160">better</w>
     <w lemma="prevent" pos="vvn" xml:id="B05719-001-a-1170">prevented</w>
     <pc unit="sentence" xml:id="B05719-001-a-1180">.</pc>
     <w lemma="therefore" pos="av" rend="hi" xml:id="B05719-001-a-1190">Therefore</w>
     <w lemma="we" pos="pns" xml:id="B05719-001-a-1200">We</w>
     <w lemma="with" pos="acp" xml:id="B05719-001-a-1210">with</w>
     <w lemma="advice" pos="n1" xml:id="B05719-001-a-1220">Advice</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-1230">of</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-1240">the</w>
     <w lemma="lord" pos="n2" xml:id="B05719-001-a-1250">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-1260">of</w>
     <w lemma="our" pos="po" xml:id="B05719-001-a-1270">Our</w>
     <w lemma="privy" pos="j" xml:id="B05719-001-a-1280">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05719-001-a-1290">Council</w>
     <pc xml:id="B05719-001-a-1300">,</pc>
     <w lemma="have" pos="vvb" xml:id="B05719-001-a-1310">have</w>
     <w lemma="think" pos="vvn" xml:id="B05719-001-a-1320">thought</w>
     <w lemma="fit" pos="j" xml:id="B05719-001-a-1330">fit</w>
     <w lemma="to" pos="prt" xml:id="B05719-001-a-1340">to</w>
     <w lemma="prohibit" pos="vvi" reg="prohibit" xml:id="B05719-001-a-1350">prohibite</w>
     <pc xml:id="B05719-001-a-1360">,</pc>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-1370">and</w>
     <w lemma="do" pos="vvb" xml:id="B05719-001-a-1380">do</w>
     <w lemma="hereby" pos="av" xml:id="B05719-001-a-1390">hereby</w>
     <w lemma="strict" pos="av-j" xml:id="B05719-001-a-1400">strictly</w>
     <w lemma="prohibit" pos="vvi" reg="prohibit" xml:id="B05719-001-a-1410">prohibite</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-1420">and</w>
     <w lemma="discharge" pos="vvi" xml:id="B05719-001-a-1430">discharge</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-1440">the</w>
     <w lemma="export" pos="vvg" xml:id="B05719-001-a-1450">exporting</w>
     <w lemma="forth" pos="av" xml:id="B05719-001-a-1460">forth</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-1470">of</w>
     <w lemma="this" pos="d" xml:id="B05719-001-a-1480">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05719-001-a-1490">Kingdom</w>
     <pc xml:id="B05719-001-a-1500">,</pc>
     <w lemma="either" pos="av-d" xml:id="B05719-001-a-1510">either</w>
     <w lemma="by" pos="acp" xml:id="B05719-001-a-1520">by</w>
     <w lemma="land" pos="n1" xml:id="B05719-001-a-1530">Land</w>
     <w lemma="or" pos="cc" xml:id="B05719-001-a-1540">or</w>
     <w lemma="by" pos="acp" xml:id="B05719-001-a-1550">by</w>
     <w lemma="sea" pos="n1" xml:id="B05719-001-a-1560">Sea</w>
     <pc xml:id="B05719-001-a-1570">,</pc>
     <w lemma="any" pos="d" xml:id="B05719-001-a-1580">any</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-1590">of</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-1600">the</w>
     <w lemma="foresaids" pos="ng1" reg="foresaids'" xml:id="B05719-001-a-1610">foresaids</w>
     <w lemma="kind" pos="ng1" reg="Kind's" xml:id="B05719-001-a-1620">Kinds</w>
     <w lemma="or" pos="cc" xml:id="B05719-001-a-1630">or</w>
     <w lemma="grain" pos="n2" xml:id="B05719-001-a-1640">Grains</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-1650">of</w>
     <w lemma="victual" pos="n1" xml:id="B05719-001-a-1660">Victual</w>
     <pc xml:id="B05719-001-a-1670">,</pc>
     <w lemma="viz." pos="ab" rend="hi" xml:id="B05719-001-a-1680">viz.</w>
     <w lemma="meal" pos="n1" xml:id="B05719-001-a-1690">Meal</w>
     <pc xml:id="B05719-001-a-1700">,</pc>
     <w lemma="oat" pos="n2" xml:id="B05719-001-a-1710">Oats</w>
     <pc xml:id="B05719-001-a-1720">,</pc>
     <w lemma="pease" pos="n1" xml:id="B05719-001-a-1730">Pease</w>
     <pc xml:id="B05719-001-a-1740">,</pc>
     <w lemma="barley" pos="n1" xml:id="B05719-001-a-1750">Barley</w>
     <w lemma="or" pos="cc" xml:id="B05719-001-a-1760">or</w>
     <w lemma="bear" pos="n1" xml:id="B05719-001-a-1770">Bear</w>
     <w lemma="after" pos="acp" xml:id="B05719-001-a-1780">after</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-1790">the</w>
     <w lemma="day" pos="n1" xml:id="B05719-001-a-1800">Day</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-1810">and</w>
     <w lemma="date" pos="n1" xml:id="B05719-001-a-1820">Date</w>
     <w lemma="hereof" pos="av" xml:id="B05719-001-a-1830">hereof</w>
     <pc xml:id="B05719-001-a-1840">,</pc>
     <w lemma="under" pos="acp" xml:id="B05719-001-a-1850">under</w>
     <w lemma="all" pos="d" xml:id="B05719-001-a-1860">all</w>
     <w lemma="high" pos="js" xml:id="B05719-001-a-1870">highest</w>
     <w lemma="pain" pos="n2" xml:id="B05719-001-a-1880">Pains</w>
     <pc xml:id="B05719-001-a-1890">,</pc>
     <w lemma="to" pos="prt" xml:id="B05719-001-a-1900">to</w>
     <w lemma="be" pos="vvi" xml:id="B05719-001-a-1910">be</w>
     <w lemma="inflict" pos="vvn" xml:id="B05719-001-a-1920">inflicted</w>
     <w lemma="upon" pos="acp" xml:id="B05719-001-a-1930">upon</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-1940">the</w>
     <w lemma="contraveener" pos="n2" xml:id="B05719-001-a-1950">Contraveeners</w>
     <pc xml:id="B05719-001-a-1960">,</pc>
     <w lemma="either" pos="d" xml:id="B05719-001-a-1970">either</w>
     <w lemma="merchant" pos="n2" xml:id="B05719-001-a-1980">Merchants</w>
     <pc xml:id="B05719-001-a-1990">,</pc>
     <w lemma="skipper" pos="n2" xml:id="B05719-001-a-2000">Skippers</w>
     <pc xml:id="B05719-001-a-2010">,</pc>
     <w lemma="or" pos="cc" xml:id="B05719-001-a-2020">or</w>
     <w lemma="other" pos="pi2-d" xml:id="B05719-001-a-2030">others</w>
     <w lemma="whatsoever" pos="crq" xml:id="B05719-001-a-2040">whatsomever</w>
     <pc xml:id="B05719-001-a-2050">:</pc>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-2060">And</w>
     <w lemma="far" pos="avc-j" reg="farther" xml:id="B05719-001-a-2070">farder</w>
     <pc xml:id="B05719-001-a-2080">,</pc>
     <w lemma="we" pos="pns" xml:id="B05719-001-a-2090">We</w>
     <w lemma="hereby" pos="av" xml:id="B05719-001-a-2100">hereby</w>
     <w lemma="require" pos="vvi" xml:id="B05719-001-a-2110">require</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-2120">and</w>
     <w lemma="charge" pos="vvb" xml:id="B05719-001-a-2130">Charge</w>
     <w lemma="all" pos="d" xml:id="B05719-001-a-2140">all</w>
     <w lemma="sheriff" pos="n2" xml:id="B05719-001-a-2150">Sheriffs</w>
     <pc xml:id="B05719-001-a-2160">,</pc>
     <w lemma="Stewart" pos="nng1" xml:id="B05719-001-a-2170">Stewarts</w>
     <pc xml:id="B05719-001-a-2180">,</pc>
     <w lemma="baily" pos="n2" reg="Bailies" xml:id="B05719-001-a-2190">Baillies</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-2200">and</w>
     <w lemma="their" pos="po" xml:id="B05719-001-a-2210">their</w>
     <w lemma="deputy" pos="n2" xml:id="B05719-001-a-2220">Deputs</w>
     <pc xml:id="B05719-001-a-2230">,</pc>
     <w lemma="magistrate" pos="n2" reg="Magistrates" xml:id="B05719-001-a-2240">Magistrats</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-2250">of</w>
     <w lemma="burgh" pos="nng1" reg="burgh's" xml:id="B05719-001-a-2260">Burghs</w>
     <pc xml:id="B05719-001-a-2270">,</pc>
     <w lemma="justice" pos="n2" xml:id="B05719-001-a-2280">Justices</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-2290">of</w>
     <w lemma="peace" pos="n1" xml:id="B05719-001-a-2300">Peace</w>
     <pc xml:id="B05719-001-a-2310">,</pc>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-2320">and</w>
     <w lemma="other" pos="d" xml:id="B05719-001-a-2330">other</w>
     <w lemma="officer" pos="n2" xml:id="B05719-001-a-2340">Officers</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-2350">of</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-2360">the</w>
     <w lemma="law" pos="n1" xml:id="B05719-001-a-2370">Law</w>
     <pc xml:id="B05719-001-a-2380">,</pc>
     <w lemma="as" pos="acp" xml:id="B05719-001-a-2390">as</w>
     <w lemma="also" pos="av" xml:id="B05719-001-a-2400">also</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-2410">of</w>
     <w lemma="our" pos="po" xml:id="B05719-001-a-2420">Our</w>
     <w lemma="customer" pos="n2" xml:id="B05719-001-a-2430">Customers</w>
     <pc xml:id="B05719-001-a-2440">;</pc>
     <w lemma="that" pos="cs" xml:id="B05719-001-a-2450">That</w>
     <w lemma="they" pos="pns" xml:id="B05719-001-a-2460">they</w>
     <w lemma="take" pos="vvb" xml:id="B05719-001-a-2470">take</w>
     <w lemma="all" pos="d" xml:id="B05719-001-a-2480">all</w>
     <w lemma="lawful" pos="j" xml:id="B05719-001-a-2490">lawful</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-2500">and</w>
     <w lemma="effectual" pos="j" xml:id="B05719-001-a-2510">effectual</w>
     <w lemma="mean" pos="n2" xml:id="B05719-001-a-2520">Means</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-2530">and</w>
     <w lemma="method" pos="n2" xml:id="B05719-001-a-2540">Methods</w>
     <w lemma="to" pos="prt" xml:id="B05719-001-a-2550">to</w>
     <w lemma="restrain" pos="vvi" xml:id="B05719-001-a-2560">restrain</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-2570">and</w>
     <w lemma="hinder" pos="vvi" xml:id="B05719-001-a-2580">hinder</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-2590">the</w>
     <w lemma="say" pos="j-vn" xml:id="B05719-001-a-2600">said</w>
     <w lemma="export" pos="n1" xml:id="B05719-001-a-2610">Export</w>
     <pc xml:id="B05719-001-a-2620">,</pc>
     <w lemma="as" pos="acp" xml:id="B05719-001-a-2630">as</w>
     <w lemma="they" pos="pns" xml:id="B05719-001-a-2640">they</w>
     <w lemma="will" pos="vmb" xml:id="B05719-001-a-2650">will</w>
     <w lemma="be" pos="vvi" xml:id="B05719-001-a-2660">be</w>
     <w lemma="answerable" pos="j" xml:id="B05719-001-a-2670">answerable</w>
     <pc xml:id="B05719-001-a-2680">,</pc>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-2690">and</w>
     <w lemma="these" pos="d" xml:id="B05719-001-a-2700">these</w>
     <w lemma="present" pos="n2" xml:id="B05719-001-a-2710">presents</w>
     <w lemma="to" pos="prt" xml:id="B05719-001-a-2720">to</w>
     <w lemma="be" pos="vvi" xml:id="B05719-001-a-2730">be</w>
     <w lemma="but" pos="acp" xml:id="B05719-001-a-2740">but</w>
     <w lemma="prejudice" pos="n1" xml:id="B05719-001-a-2750">prejudice</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-2760">of</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-2770">the</w>
     <w lemma="former" pos="j" xml:id="B05719-001-a-2780">former</w>
     <w lemma="order" pos="n1" xml:id="B05719-001-a-2790">Order</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-2800">and</w>
     <w lemma="restraint" pos="n1" xml:id="B05719-001-a-2810">Restraint</w>
     <w lemma="issue" pos="vvd" xml:id="B05719-001-a-2820">issued</w>
     <w lemma="out" pos="av" xml:id="B05719-001-a-2830">out</w>
     <w lemma="by" pos="acp" xml:id="B05719-001-a-2840">by</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-2850">the</w>
     <w lemma="lord" pos="n2" xml:id="B05719-001-a-2860">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-2870">of</w>
     <w lemma="our" pos="po" xml:id="B05719-001-a-2880">our</w>
     <w lemma="privy" pos="j" xml:id="B05719-001-a-2890">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05719-001-a-2900">Council</w>
     <pc xml:id="B05719-001-a-2910">,</pc>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-2920">of</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-2930">the</w>
     <w lemma="date" pos="n1" xml:id="B05719-001-a-2940">date</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-2950">the</w>
     <w lemma="twenty" pos="crd" xml:id="B05719-001-a-2960">twenty</w>
     <w lemma="six" pos="ord" xml:id="B05719-001-a-2970">sixth</w>
     <w lemma="day" pos="n1" xml:id="B05719-001-a-2980">day</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-2990">of</w>
     <w lemma="April" pos="nn1" rend="hi" xml:id="B05719-001-a-3000">April</w>
     <w lemma="last" pos="ord" xml:id="B05719-001-a-3010">last</w>
     <pc unit="sentence" xml:id="B05719-001-a-3020">.</pc>
     <w lemma="our" pos="po" xml:id="B05719-001-a-3030">OUR</w>
     <w lemma="will" pos="n1" xml:id="B05719-001-a-3040">WILL</w>
     <w lemma="be" pos="vvz" xml:id="B05719-001-a-3050">IS</w>
     <w lemma="herefore" pos="av" xml:id="B05719-001-a-3060">HEREFORE</w>
     <pc xml:id="B05719-001-a-3070">,</pc>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-3080">and</w>
     <w lemma="we" pos="pns" xml:id="B05719-001-a-3090">we</w>
     <w lemma="charge" pos="vvb" xml:id="B05719-001-a-3100">charge</w>
     <w lemma="you" pos="pn" xml:id="B05719-001-a-3110">you</w>
     <w lemma="strict" pos="av-j" xml:id="B05719-001-a-3120">strictly</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-3130">and</w>
     <w lemma="command" pos="vvb" xml:id="B05719-001-a-3140">Command</w>
     <pc xml:id="B05719-001-a-3150">,</pc>
     <w lemma="that" pos="cs" xml:id="B05719-001-a-3160">that</w>
     <w lemma="incontinent" pos="j" xml:id="B05719-001-a-3170">incontinent</w>
     <w lemma="these" pos="d" xml:id="B05719-001-a-3180">these</w>
     <w lemma="our" pos="po" xml:id="B05719-001-a-3190">Our</w>
     <w lemma="letter" pos="n2" xml:id="B05719-001-a-3200">Letters</w>
     <w lemma="see" pos="vvn" xml:id="B05719-001-a-3210">seen</w>
     <pc xml:id="B05719-001-a-3220">,</pc>
     <w lemma="you" pos="pn" xml:id="B05719-001-a-3230">ye</w>
     <w lemma="pass" pos="vvb" xml:id="B05719-001-a-3240">pass</w>
     <w lemma="to" pos="acp" xml:id="B05719-001-a-3250">to</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-3260">the</w>
     <w lemma="mercat-cross" pos="n1" xml:id="B05719-001-a-3270">Mercat-Cross</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-3280">of</w>
     <hi xml:id="B05719-e160">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05719-001-a-3290">Edinburgh</w>
      <pc xml:id="B05719-001-a-3300">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-3310">and</w>
     <w lemma="to" pos="prt" xml:id="B05719-001-a-3320">to</w>
     <w lemma="t●e" pos="vvb" xml:id="B05719-001-a-3330">the</w>
     <w lemma="mercat-cross" pos="n2" xml:id="B05719-001-a-3340">Mercat-Crosses</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-3350">of</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-3360">the</w>
     <w lemma="n/a" pos="fla" xml:id="B05719-001-a-3370">remanent</w>
     <w lemma="Hea" pos="zz" xml:id="B05719-001-a-3380">Hea</w>
     <w lemma="〈…〉" pos="zz" xml:id="B05719-001-a-3390">〈…〉</w>
     <w lemma="burgh" pos="vvz" xml:id="B05719-001-a-3400">burghs</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-3410">of</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-3420">the</w>
     <w lemma="several" pos="j" xml:id="B05719-001-a-3430">several</w>
     <w lemma="shire" pos="n2" xml:id="B05719-001-a-3440">Shires</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-3450">and</w>
     <w lemma="stewartry" pos="n2" xml:id="B05719-001-a-3460">Stewartries</w>
     <w lemma="within" pos="acp" xml:id="B05719-001-a-3470">within</w>
     <w lemma="this" pos="d" xml:id="B05719-001-a-3480">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05719-001-a-3490">Kingdom</w>
     <pc xml:id="B05719-001-a-3500">,</pc>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-3510">and</w>
     <w lemma="to" pos="acp" xml:id="B05719-001-a-3520">to</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-3530">the</w>
     <w lemma="〈…〉" pos="zz" xml:id="B05719-001-a-3540">〈…〉</w>
     <w lemma="aill" pos="uh" xml:id="B05719-001-a-3550">aill</w>
     <w lemma="sea" pos="n1" xml:id="B05719-001-a-3560">Sea</w>
     <w lemma="port" pos="n2" xml:id="B05719-001-a-3570">ports</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-3580">of</w>
     <w lemma="this" pos="d" xml:id="B05719-001-a-3590">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05719-001-a-3600">Kingdom</w>
     <pc xml:id="B05719-001-a-3610">,</pc>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-3620">and</w>
     <w lemma="there" pos="av" xml:id="B05719-001-a-3630">there</w>
     <w lemma="in" pos="acp" xml:id="B05719-001-a-3640">in</w>
     <w lemma="our" pos="po" xml:id="B05719-001-a-3650">Our</w>
     <w lemma="name" pos="n1" xml:id="B05719-001-a-3660">Name</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-3670">and</w>
     <w lemma="authority" pos="n1" xml:id="B05719-001-a-3680">Authority</w>
     <pc xml:id="B05719-001-a-3690">,</pc>
     <w lemma="make" pos="vvb" xml:id="B05719-001-a-3700">make</w>
     <w lemma="publication" pos="n1" xml:id="B05719-001-a-3710">Publication</w>
     <w lemma="hereof" pos="av" xml:id="B05719-001-a-3720">hereof</w>
     <pc xml:id="B05719-001-a-3730">,</pc>
     <w lemma="that" pos="cs" xml:id="B05719-001-a-3740">that</w>
     <w lemma="none" pos="pix" xml:id="B05719-001-a-3750">none</w>
     <w lemma="pretend" pos="vvb" xml:id="B05719-001-a-3760">pretend</w>
     <w lemma="ignorance" pos="n1" xml:id="B05719-001-a-3770">Ignorance</w>
     <pc xml:id="B05719-001-a-3780">,</pc>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-3790">and</w>
     <w lemma="ordain" pos="vvz" xml:id="B05719-001-a-3800">Ordains</w>
     <w lemma="these" pos="d" xml:id="B05719-001-a-3810">these</w>
     <w lemma="present" pos="n2" xml:id="B05719-001-a-3820">Presents</w>
     <w lemma="to" pos="prt" xml:id="B05719-001-a-3830">to</w>
     <w lemma="be" pos="vvi" xml:id="B05719-001-a-3840">be</w>
     <w lemma="print" pos="vvn" xml:id="B05719-001-a-3850">Printed</w>
     <pc unit="sentence" xml:id="B05719-001-a-3860">.</pc>
    </p>
    <closer xml:id="B05719-e170">
     <dateline xml:id="B05719-e180">
      <w lemma="give" pos="vvn" xml:id="B05719-001-a-3870">Given</w>
      <w lemma="under" pos="acp" xml:id="B05719-001-a-3880">under</w>
      <w lemma="our" pos="po" xml:id="B05719-001-a-3890">Our</w>
      <w lemma="signet" pos="n1" xml:id="B05719-001-a-3900">Signet</w>
      <w lemma="at" pos="acp" xml:id="B05719-001-a-3910">at</w>
      <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05719-001-a-3920">Edinburgh</w>
      <date xml:id="B05719-e200">
       <w lemma="the" pos="d" xml:id="B05719-001-a-3930">the</w>
       <w lemma="ten" pos="ord" xml:id="B05719-001-a-3940">tenth</w>
       <w lemma="day" pos="n1" xml:id="B05719-001-a-3950">day</w>
       <w lemma="of" pos="acp" xml:id="B05719-001-a-3960">of</w>
       <hi xml:id="B05719-e210">
        <w lemma="May" pos="nn1" xml:id="B05719-001-a-3970">May</w>
        <pc xml:id="B05719-001-a-3980">,</pc>
       </hi>
       <w lemma="and" pos="cc" xml:id="B05719-001-a-3990">and</w>
       <w lemma="of" pos="acp" xml:id="B05719-001-a-4000">of</w>
       <w lemma="our" pos="po" xml:id="B05719-001-a-4010">Our</w>
       <w lemma="reign" pos="n1" xml:id="B05719-001-a-4020">Reign</w>
       <w lemma="the" pos="d" xml:id="B05719-001-a-4030">the</w>
       <w lemma="ten" pos="ord" xml:id="B05719-001-a-4040">tenth</w>
       <w lemma="year" pos="n1" xml:id="B05719-001-a-4050">Year</w>
       <w lemma="1698" pos="crd" rend="hi" xml:id="B05719-001-a-4060">1698</w>
      </date>
      <pc unit="sentence" xml:id="B05719-001-a-4070">.</pc>
     </dateline>
     <signed xml:id="B05719-e230">
      <w lemma="n/a" pos="fla" xml:id="B05719-001-a-4080">Per</w>
      <w lemma="n/a" pos="fla" xml:id="B05719-001-a-4090">Actum</w>
      <w lemma="n/a" pos="fla" xml:id="B05719-001-a-4100">Dominorum</w>
      <w lemma="n/a" pos="fla" xml:id="B05719-001-a-4110">Secreti</w>
      <w lemma="n/a" pos="fla" xml:id="B05719-001-a-4120">Concilii</w>
      <hi xml:id="B05719-e240">
       <w lemma="GILB" pos="nn1" xml:id="B05719-001-a-4130">GILB</w>
       <pc unit="sentence" xml:id="B05719-001-a-4140">.</pc>
       <w lemma="eliot" pos="nn1" xml:id="B05719-001-a-4150">ELIOT</w>
       <pc unit="sentence" xml:id="B05719-001-a-4160">.</pc>
      </hi>
      <w lemma="Cls." pos="nn1" xml:id="B05719-001-a-4170">Cls.</w>
      <w lemma="sti." pos="ab" xml:id="B05719-001-a-4180">Sti.</w>
      <w lemma="n/a" pos="fla" xml:id="B05719-001-a-4190">Concilii</w>
      <pc unit="sentence" xml:id="B05719-001-a-4200">.</pc>
     </signed>
     <lb xml:id="B05719-e250"/>
     <w lemma="GOD" pos="nn1" xml:id="B05719-001-a-4210">GOD</w>
     <hi xml:id="B05719-e260">
      <w lemma="save" pos="acp" xml:id="B05719-001-a-4220">Save</w>
      <w lemma="the" pos="d" xml:id="B05719-001-a-4230">the</w>
     </hi>
     <w lemma="King" pos="n1" xml:id="B05719-001-a-4240">King</w>
     <pc unit="sentence" xml:id="B05719-001-a-4241">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="B05719-e270">
   <div type="colophon" xml:id="B05719-e280">
    <p xml:id="B05719-e290">
     <hi xml:id="B05719-e300">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05719-001-a-4260">Edinburgh</w>
      <pc xml:id="B05719-001-a-4270">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="B05719-001-a-4280">Printed</w>
     <w lemma="by" pos="acp" xml:id="B05719-001-a-4290">by</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-4300">the</w>
     <w lemma="heir" pos="n2" xml:id="B05719-001-a-4310">Heirs</w>
     <w lemma="and" pos="cc" xml:id="B05719-001-a-4320">and</w>
     <w lemma="successor" pos="n2" xml:id="B05719-001-a-4330">Successors</w>
     <w lemma="of" pos="acp" xml:id="B05719-001-a-4340">of</w>
     <hi xml:id="B05719-e310">
      <w lemma="Andrew" pos="nn1" xml:id="B05719-001-a-4350">Andrew</w>
      <w lemma="Anderson" pos="nn1" xml:id="B05719-001-a-4360">Anderson</w>
     </hi>
     <w lemma="printer" pos="n1" xml:id="B05719-001-a-4370">Printer</w>
     <w lemma="to" pos="acp" xml:id="B05719-001-a-4380">to</w>
     <w lemma="the" pos="d" xml:id="B05719-001-a-4390">the</w>
     <w lemma="king" pos="n2" xml:id="B05719-001-a-4400">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="B05719-001-a-4410">Most</w>
     <w lemma="excellent" pos="j" xml:id="B05719-001-a-4420">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="B05719-001-a-4430">Majesty</w>
     <pc xml:id="B05719-001-a-4440">,</pc>
     <hi xml:id="B05719-e320">
      <w lemma="n/a" pos="fla" xml:id="B05719-001-a-4450">Anno</w>
      <w lemma="dom." pos="ab" xml:id="B05719-001-a-4460">Dom.</w>
     </hi>
     <w lemma="1698." pos="crd" xml:id="B05719-001-a-4470">1698.</w>
     <pc unit="sentence" xml:id="B05719-001-a-4480"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
