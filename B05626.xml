<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B05626">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A proclamation for adjourning the Parliament, to the twentieth of March next, 1696.</title>
    <author>Scotland. Privy Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B05626 of text R226062 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing S1820). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B05626</idno>
    <idno type="STC">Wing S1820</idno>
    <idno type="STC">ESTC R226062</idno>
    <idno type="EEBO-CITATION">52529290</idno>
    <idno type="OCLC">ocm 52529290</idno>
    <idno type="VID">179058</idno>
    <idno type="PROQUESTGOID">2248529230</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B05626)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179058)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2776:47)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A proclamation for adjourning the Parliament, to the twentieth of March next, 1696.</title>
      <author>Scotland. Privy Council.</author>
      <author>Scotland. Sovereign (1694-1702 : William II)</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by the heirs and successors of Andrew Anderson, Printer to the Kings most excellent Majesty,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>1695.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Royal arms at head of text; initial letter.</note>
      <note>Dated: Given under Our Signet at Edinburgh the fifth day of February, and of Our Reign the seventh year.</note>
      <note>Signed: Gilb. Eliot, Cls. Sti. Concilii.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Scotland. -- Parliament -- Early works to 1800.</term>
     <term>Scotland -- Politics and government -- 1689-1745 -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A proclamation for adjourning the Parliament, to the twentieth of March next, 1696.</ep:title>
    <ep:author>Scotland. Privy Council</ep:author>
    <ep:publicationYear>1695</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>369</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-01</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-01</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-03</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-03</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B05626-t">
  <body xml:id="B05626-e0">
   <div type="royal_proclamation" xml:id="B05626-e10">
    <pb facs="tcp:179058:1" rend="simple:additions" xml:id="B05626-001-a"/>
    <head xml:id="B05626-e20">
     <figure xml:id="B05626-e30">
      <p xml:id="B05626-e40">
       <w lemma="WR" pos="sy" xml:id="B05626-001-a-0010">WR</w>
      </p>
      <p xml:id="B05626-e50">
       <w lemma="n/a" pos="ffr" xml:id="B05626-001-a-0020">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="B05626-001-a-0030">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="B05626-001-a-0040">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="B05626-001-a-0050">DROIT</w>
      </p>
      <p xml:id="B05626-e60">
       <w lemma="n/a" pos="ffr" xml:id="B05626-001-a-0060">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="B05626-001-a-0070">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="B05626-001-a-0080">QUI</w>
       <w lemma="n/a" pos="ffr" xml:id="B05626-001-a-0090">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="B05626-001-a-0100">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="B05626-001-a-0110">PENSE</w>
      </p>
      <figDesc xml:id="B05626-e70">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="B05626-e80">
     <hi xml:id="B05626-e90">
      <w lemma="a" pos="d" xml:id="B05626-001-a-0120">A</w>
      <w lemma="proclamation" pos="n1" xml:id="B05626-001-a-0130">PROCLAMATION</w>
     </hi>
     <w lemma="for" pos="acp" xml:id="B05626-001-a-0140">For</w>
     <w lemma="adjourn" pos="vvg" xml:id="B05626-001-a-0150">Adjourning</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-0160">the</w>
     <w lemma="parliament" pos="n1" xml:id="B05626-001-a-0170">Parliament</w>
     <pc xml:id="B05626-001-a-0180">,</pc>
     <w lemma="to" pos="prt" xml:id="B05626-001-a-0190">to</w>
     <date xml:id="B05626-e100">
      <w lemma="the" pos="d" xml:id="B05626-001-a-0200">the</w>
      <w lemma="twenty" pos="ord" xml:id="B05626-001-a-0210">Twentieth</w>
      <w lemma="of" pos="acp" xml:id="B05626-001-a-0220">of</w>
      <w lemma="March" pos="nn1" rend="hi" xml:id="B05626-001-a-0230">March</w>
      <w lemma="next" pos="ord" xml:id="B05626-001-a-0240">next</w>
      <pc xml:id="B05626-001-a-0250">,</pc>
      <hi xml:id="B05626-e120">
       <w lemma="1696." pos="crd" xml:id="B05626-001-a-0260">1696.</w>
       <pc unit="sentence" xml:id="B05626-001-a-0270"/>
      </hi>
     </date>
    </head>
    <opener xml:id="B05626-e130">
     <signed xml:id="B05626-e140">
      <w lemma="WILLIAM" pos="nn1" rend="decorinit" xml:id="B05626-001-a-0280">WILLIAM</w>
      <w lemma="by" pos="acp" xml:id="B05626-001-a-0290">by</w>
      <w lemma="the" pos="d" xml:id="B05626-001-a-0300">the</w>
      <w lemma="grace" pos="n1" xml:id="B05626-001-a-0310">Grace</w>
      <w lemma="of" pos="acp" xml:id="B05626-001-a-0320">of</w>
      <w lemma="God" pos="nn1" xml:id="B05626-001-a-0330">God</w>
      <w lemma="king" pos="n1" xml:id="B05626-001-a-0340">King</w>
      <w lemma="of" pos="acp" xml:id="B05626-001-a-0350">of</w>
      <w lemma="great" pos="j" xml:id="B05626-001-a-0360">great</w>
      <hi xml:id="B05626-e150">
       <w lemma="Britain" pos="nn1" xml:id="B05626-001-a-0370">Britain</w>
       <pc xml:id="B05626-001-a-0380">,</pc>
       <w lemma="France" pos="nn1" xml:id="B05626-001-a-0390">France</w>
      </hi>
      <w lemma="and" pos="cc" xml:id="B05626-001-a-0400">and</w>
      <hi xml:id="B05626-e160">
       <w lemma="Ireland" pos="nn1" xml:id="B05626-001-a-0410">Ireland</w>
       <pc xml:id="B05626-001-a-0420">,</pc>
      </hi>
      <w lemma="defender" pos="n1" xml:id="B05626-001-a-0430">Defender</w>
      <w lemma="of" pos="acp" xml:id="B05626-001-a-0440">of</w>
      <w lemma="the" pos="d" xml:id="B05626-001-a-0450">the</w>
      <w lemma="faith" pos="n1" xml:id="B05626-001-a-0460">Faith</w>
      <pc xml:id="B05626-001-a-0470">,</pc>
     </signed>
     <salute xml:id="B05626-e170">
      <w lemma="to" pos="acp" xml:id="B05626-001-a-0480">to</w>
      <w lemma="our" pos="po" xml:id="B05626-001-a-0490">our</w>
      <w lemma="lion" pos="n1" reg="Lion" xml:id="B05626-001-a-0500">Lyon</w>
      <w lemma="king" pos="n1" xml:id="B05626-001-a-0510">King</w>
      <w lemma="at" pos="acp" xml:id="B05626-001-a-0520">at</w>
      <w lemma="arm" pos="n2" xml:id="B05626-001-a-0530">Arms</w>
      <w lemma="and" pos="cc" xml:id="B05626-001-a-0540">and</w>
      <w lemma="his" pos="po" xml:id="B05626-001-a-0550">his</w>
      <w lemma="brethren" pos="n2" xml:id="B05626-001-a-0560">Brethren</w>
      <w lemma="herald" pos="n2" reg="Heralds" xml:id="B05626-001-a-0570">Heraulds</w>
      <pc xml:id="B05626-001-a-0580">,</pc>
      <w lemma="macer" pos="n2" xml:id="B05626-001-a-0590">Macers</w>
      <w lemma="of" pos="acp" xml:id="B05626-001-a-0600">of</w>
      <w lemma="our" pos="po" xml:id="B05626-001-a-0610">Our</w>
      <w lemma="privy" pos="j" xml:id="B05626-001-a-0620">Privy</w>
      <w lemma="council" pos="n1" xml:id="B05626-001-a-0630">Council</w>
      <pc xml:id="B05626-001-a-0640">,</pc>
      <w lemma="pursuivant" pos="n2" reg="Pursuivants" xml:id="B05626-001-a-0650">Pursevants</w>
      <pc xml:id="B05626-001-a-0660">,</pc>
      <w lemma="messenger" pos="n2" xml:id="B05626-001-a-0670">Messengers</w>
      <w lemma="at" pos="acp" xml:id="B05626-001-a-0680">at</w>
      <w lemma="arm" pos="n2" xml:id="B05626-001-a-0690">Arms</w>
      <pc xml:id="B05626-001-a-0700">,</pc>
      <w lemma="our" pos="po" xml:id="B05626-001-a-0710">Our</w>
      <w lemma="sheriff" pos="n2" xml:id="B05626-001-a-0720">Sheriffs</w>
      <w lemma="in" pos="acp" xml:id="B05626-001-a-0730">in</w>
      <w lemma="that" pos="d" xml:id="B05626-001-a-0740">that</w>
      <w lemma="part" pos="n1" xml:id="B05626-001-a-0750">part</w>
      <pc xml:id="B05626-001-a-0760">,</pc>
      <w lemma="conjunct" pos="av-j" xml:id="B05626-001-a-0770">Conjunctly</w>
      <w lemma="and" pos="cc" xml:id="B05626-001-a-0780">and</w>
      <w lemma="several" pos="av-j" xml:id="B05626-001-a-0790">Severally</w>
      <pc xml:id="B05626-001-a-0800">,</pc>
      <w lemma="special" pos="av-j" xml:id="B05626-001-a-0810">Specially</w>
      <w lemma="constitute" pos="vvi" xml:id="B05626-001-a-0820">Constitute</w>
      <pc xml:id="B05626-001-a-0830">,</pc>
      <w lemma="greeting" pos="n1" xml:id="B05626-001-a-0840">Greeting</w>
      <pc xml:id="B05626-001-a-0850">;</pc>
     </salute>
    </opener>
    <p xml:id="B05626-e180">
     <w lemma="forasmuch" pos="av" xml:id="B05626-001-a-0860">Forasmuch</w>
     <w lemma="as" pos="acp" xml:id="B05626-001-a-0870">as</w>
     <w lemma="by" pos="acp" xml:id="B05626-001-a-0880">by</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-0890">the</w>
     <w lemma="last" pos="ord" xml:id="B05626-001-a-0900">last</w>
     <w lemma="act" pos="n1" xml:id="B05626-001-a-0910">Act</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-0920">of</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-0930">the</w>
     <w lemma="five" pos="ord" xml:id="B05626-001-a-0940">fifth</w>
     <w lemma="session" pos="n1" xml:id="B05626-001-a-0950">Session</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-0960">of</w>
     <w lemma="this" pos="d" xml:id="B05626-001-a-0970">this</w>
     <w lemma="our" pos="po" xml:id="B05626-001-a-0980">Our</w>
     <w lemma="current" pos="j" xml:id="B05626-001-a-0990">Current</w>
     <w lemma="parliament" pos="n1" xml:id="B05626-001-a-1000">Parliament</w>
     <pc xml:id="B05626-001-a-1010">,</pc>
     <w lemma="the" pos="d" xml:id="B05626-001-a-1020">the</w>
     <w lemma="same" pos="d" xml:id="B05626-001-a-1030">same</w>
     <w lemma="be" pos="vvz" xml:id="B05626-001-a-1040">is</w>
     <w lemma="adjourn" pos="vvn" xml:id="B05626-001-a-1050">adjourned</w>
     <w lemma="to" pos="acp" xml:id="B05626-001-a-1060">to</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-1070">the</w>
     <w lemma="seven" pos="ord" xml:id="B05626-001-a-1080">seventh</w>
     <w lemma="day" pos="n1" xml:id="B05626-001-a-1090">day</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-1100">of</w>
     <hi xml:id="B05626-e190">
      <w lemma="November" pos="nn1" xml:id="B05626-001-a-1110">November</w>
      <pc xml:id="B05626-001-a-1120">,</pc>
     </hi>
     <w lemma="than" pos="cs" reg="than" xml:id="B05626-001-a-1130">then</w>
     <w lemma="next" pos="ord" xml:id="B05626-001-a-1140">next</w>
     <w lemma="to" pos="prt" xml:id="B05626-001-a-1150">to</w>
     <w lemma="come" pos="vvi" xml:id="B05626-001-a-1160">come</w>
     <pc xml:id="B05626-001-a-1170">,</pc>
     <w lemma="now" pos="av" xml:id="B05626-001-a-1180">now</w>
     <w lemma="instant" pos="j" xml:id="B05626-001-a-1190">Instant</w>
     <pc xml:id="B05626-001-a-1200">:</pc>
     <w lemma="and" pos="cc" xml:id="B05626-001-a-1210">And</w>
     <w lemma="our" pos="po" xml:id="B05626-001-a-1220">our</w>
     <w lemma="affair" pos="n2" xml:id="B05626-001-a-1230">Affairs</w>
     <w lemma="not" pos="xx" xml:id="B05626-001-a-1240">not</w>
     <w lemma="require" pos="vvg" xml:id="B05626-001-a-1250">requiring</w>
     <w lemma="a" pos="d" xml:id="B05626-001-a-1260">a</w>
     <w lemma="meeting" pos="n1" xml:id="B05626-001-a-1270">Meeting</w>
     <w lemma="thereof" pos="av" xml:id="B05626-001-a-1280">thereof</w>
     <w lemma="at" pos="acp" xml:id="B05626-001-a-1290">at</w>
     <w lemma="that" pos="d" xml:id="B05626-001-a-1300">that</w>
     <w lemma="time" pos="n1" xml:id="B05626-001-a-1310">time</w>
     <pc xml:id="B05626-001-a-1320">,</pc>
     <w lemma="we" pos="pns" xml:id="B05626-001-a-1330">We</w>
     <w lemma="have" pos="vvb" xml:id="B05626-001-a-1340">have</w>
     <w lemma="think" pos="vvn" xml:id="B05626-001-a-1350">thought</w>
     <w lemma="fit" pos="j" xml:id="B05626-001-a-1360">fit</w>
     <w lemma="that" pos="cs" xml:id="B05626-001-a-1370">that</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-1380">the</w>
     <w lemma="adjournment" pos="n1" xml:id="B05626-001-a-1390">Adjournment</w>
     <w lemma="thereof" pos="av" xml:id="B05626-001-a-1400">thereof</w>
     <w lemma="shall" pos="vmb" xml:id="B05626-001-a-1410">shall</w>
     <w lemma="be" pos="vvi" xml:id="B05626-001-a-1420">be</w>
     <w lemma="continue" pos="vvn" xml:id="B05626-001-a-1430">continued</w>
     <w lemma="from" pos="acp" xml:id="B05626-001-a-1440">from</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-1450">the</w>
     <w lemma="say" pos="j-vn" xml:id="B05626-001-a-1460">said</w>
     <w lemma="day" pos="n1" xml:id="B05626-001-a-1470">day</w>
     <w lemma="to" pos="acp" xml:id="B05626-001-a-1480">to</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-1490">the</w>
     <w lemma="twenty" pos="ord" xml:id="B05626-001-a-1500">Twentieth</w>
     <w lemma="day" pos="n1" xml:id="B05626-001-a-1510">Day</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-1520">of</w>
     <w lemma="March" pos="nn1" rend="hi" xml:id="B05626-001-a-1530">March</w>
     <w lemma="next" pos="ord" xml:id="B05626-001-a-1540">next</w>
     <w lemma="to" pos="prt" xml:id="B05626-001-a-1550">to</w>
     <w lemma="come" pos="vvi" xml:id="B05626-001-a-1560">come</w>
     <pc xml:id="B05626-001-a-1570">,</pc>
     <w lemma="therefore" pos="av" xml:id="B05626-001-a-1580">Therefore</w>
     <pc xml:id="B05626-001-a-1590">,</pc>
     <w lemma="we" pos="pns" xml:id="B05626-001-a-1600">We</w>
     <w lemma="with" pos="acp" xml:id="B05626-001-a-1610">with</w>
     <w lemma="advice" pos="n1" xml:id="B05626-001-a-1620">Advice</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-1630">of</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-1640">the</w>
     <w lemma="lord" pos="n2" xml:id="B05626-001-a-1650">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-1660">of</w>
     <w lemma="our" pos="po" xml:id="B05626-001-a-1670">Our</w>
     <w lemma="privy" pos="j" xml:id="B05626-001-a-1680">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05626-001-a-1690">Council</w>
     <pc xml:id="B05626-001-a-1700">,</pc>
     <w lemma="do" pos="vvb" xml:id="B05626-001-a-1710">do</w>
     <w lemma="hereby" pos="av" xml:id="B05626-001-a-1720">hereby</w>
     <w lemma="adjourn" pos="vvb" xml:id="B05626-001-a-1730">Adjourn</w>
     <w lemma="our" pos="po" xml:id="B05626-001-a-1740">Our</w>
     <w lemma="say" pos="j-vn" xml:id="B05626-001-a-1750">said</w>
     <w lemma="current" pos="n1" xml:id="B05626-001-a-1760">Current</w>
     <w lemma="parliament" pos="n1" xml:id="B05626-001-a-1770">Parliament</w>
     <pc xml:id="B05626-001-a-1780">,</pc>
     <w lemma="unto" pos="acp" xml:id="B05626-001-a-1790">unto</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-1800">the</w>
     <w lemma="say" pos="vvd" xml:id="B05626-001-a-1810">said</w>
     <w lemma="twenty" pos="ord" xml:id="B05626-001-a-1820">Twentieth</w>
     <w lemma="day" pos="n1" xml:id="B05626-001-a-1830">Day</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-1840">of</w>
     <w lemma="March" pos="nn1" rend="hi" xml:id="B05626-001-a-1850">March</w>
     <w lemma="next" pos="ord" xml:id="B05626-001-a-1860">next</w>
     <w lemma="ensue" pos="j-vg" xml:id="B05626-001-a-1870">Ensuing</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-1880">the</w>
     <w lemma="date" pos="n1" xml:id="B05626-001-a-1890">Date</w>
     <w lemma="hereof" pos="av" xml:id="B05626-001-a-1900">hereof</w>
     <pc xml:id="B05626-001-a-1910">;</pc>
     <w lemma="require" pos="vvg" xml:id="B05626-001-a-1920">Requiring</w>
     <w lemma="all" pos="d" xml:id="B05626-001-a-1930">all</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-1940">the</w>
     <w lemma="member" pos="n2" xml:id="B05626-001-a-1950">Members</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-1960">of</w>
     <w lemma="our" pos="po" xml:id="B05626-001-a-1970">Our</w>
     <w lemma="say" pos="j-vn" xml:id="B05626-001-a-1980">said</w>
     <w lemma="parliament" pos="n1" xml:id="B05626-001-a-1990">Parliament</w>
     <w lemma="to" pos="prt" xml:id="B05626-001-a-2000">to</w>
     <w lemma="attend" pos="vvi" xml:id="B05626-001-a-2010">attend</w>
     <w lemma="that" pos="d" xml:id="B05626-001-a-2020">that</w>
     <w lemma="day" pos="n1" xml:id="B05626-001-a-2030">Day</w>
     <w lemma="in" pos="acp" xml:id="B05626-001-a-2040">in</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-2050">the</w>
     <w lemma="usual" pos="j" xml:id="B05626-001-a-2060">usual</w>
     <w lemma="way" pos="n1" xml:id="B05626-001-a-2070">way</w>
     <pc xml:id="B05626-001-a-2080">,</pc>
     <w lemma="and" pos="cc" xml:id="B05626-001-a-2090">and</w>
     <w lemma="under" pos="acp" xml:id="B05626-001-a-2100">under</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-2110">the</w>
     <w lemma="certification" pos="n2" xml:id="B05626-001-a-2120">Certifications</w>
     <w lemma="contain" pos="vvn" xml:id="B05626-001-a-2130">contained</w>
     <w lemma="in" pos="acp" xml:id="B05626-001-a-2140">in</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-2150">the</w>
     <w lemma="several" pos="j" xml:id="B05626-001-a-2160">several</w>
     <w lemma="act" pos="n2" xml:id="B05626-001-a-2170">Acts</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-2180">of</w>
     <w lemma="parliament" pos="n1" xml:id="B05626-001-a-2190">Parliament</w>
     <w lemma="make" pos="vvd" xml:id="B05626-001-a-2200">made</w>
     <w lemma="thereanent" pos="av" xml:id="B05626-001-a-2210">thereanent</w>
     <pc unit="sentence" xml:id="B05626-001-a-2220">.</pc>
     <w lemma="our" pos="po" xml:id="B05626-001-a-2230">OUR</w>
     <w lemma="will" pos="n1" xml:id="B05626-001-a-2240">WILL</w>
     <w lemma="be" pos="vvz" xml:id="B05626-001-a-2250">IS</w>
     <w lemma="herefore" pos="av" xml:id="B05626-001-a-2260">HEREFORE</w>
     <pc xml:id="B05626-001-a-2270">,</pc>
     <w lemma="and" pos="cc" xml:id="B05626-001-a-2280">and</w>
     <w lemma="we" pos="pns" xml:id="B05626-001-a-2290">We</w>
     <w lemma="charge" pos="vvb" xml:id="B05626-001-a-2300">Charge</w>
     <w lemma="you" pos="pn" xml:id="B05626-001-a-2310">you</w>
     <w lemma="strict" pos="av-j" xml:id="B05626-001-a-2320">strictly</w>
     <pc xml:id="B05626-001-a-2330">,</pc>
     <w lemma="and" pos="cc" xml:id="B05626-001-a-2340">and</w>
     <w lemma="command" pos="vvb" xml:id="B05626-001-a-2350">Command</w>
     <pc xml:id="B05626-001-a-2360">,</pc>
     <w lemma="that" pos="cs" xml:id="B05626-001-a-2370">that</w>
     <w lemma="incontinent" pos="j" xml:id="B05626-001-a-2380">Incontinent</w>
     <pc xml:id="B05626-001-a-2390">,</pc>
     <w lemma="these" pos="d" xml:id="B05626-001-a-2400">these</w>
     <w lemma="our" pos="po" xml:id="B05626-001-a-2410">Our</w>
     <w lemma="letter" pos="n2" xml:id="B05626-001-a-2420">Letters</w>
     <w lemma="see" pos="vvn" xml:id="B05626-001-a-2430">seen</w>
     <pc xml:id="B05626-001-a-2440">,</pc>
     <w lemma="you" pos="pn" xml:id="B05626-001-a-2450">ye</w>
     <w lemma="pass" pos="vvb" xml:id="B05626-001-a-2460">pass</w>
     <w lemma="to" pos="acp" xml:id="B05626-001-a-2470">to</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-2480">the</w>
     <w lemma="mercat" pos="n1" xml:id="B05626-001-a-2490">Mercat</w>
     <w lemma="cross" pos="n1" xml:id="B05626-001-a-2500">Cross</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-2510">of</w>
     <hi xml:id="B05626-e220">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05626-001-a-2520">Edinburgh</w>
      <pc xml:id="B05626-001-a-2530">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B05626-001-a-2540">and</w>
     <w lemma="to" pos="acp" xml:id="B05626-001-a-2550">to</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-2560">the</w>
     <w lemma="mercat" pos="n1" xml:id="B05626-001-a-2570">Mercat</w>
     <w lemma="cross" pos="vvz" xml:id="B05626-001-a-2580">Crosses</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-2590">of</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-2600">the</w>
     <w lemma="n/a" pos="fla" xml:id="B05626-001-a-2610">remanent</w>
     <w lemma="head" pos="n1" xml:id="B05626-001-a-2620">Head</w>
     <w lemma="burgh" pos="nng1" reg="burgh's" xml:id="B05626-001-a-2630">Burghs</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-2640">of</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-2650">the</w>
     <w lemma="several" pos="j" xml:id="B05626-001-a-2660">several</w>
     <w lemma="shire" pos="n2" xml:id="B05626-001-a-2670">Shires</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-2680">of</w>
     <w lemma="this" pos="d" xml:id="B05626-001-a-2690">this</w>
     <w lemma="our" pos="po" xml:id="B05626-001-a-2700">Our</w>
     <w lemma="ancient" pos="j" xml:id="B05626-001-a-2710">Ancient</w>
     <w lemma="kingdom" pos="n1" xml:id="B05626-001-a-2720">Kingdom</w>
     <pc xml:id="B05626-001-a-2730">;</pc>
     <w lemma="and" pos="cc" xml:id="B05626-001-a-2740">And</w>
     <w lemma="there" pos="av" xml:id="B05626-001-a-2750">there</w>
     <w lemma="by" pos="acp" xml:id="B05626-001-a-2760">by</w>
     <w lemma="open" pos="j" xml:id="B05626-001-a-2770">open</w>
     <w lemma="proclamation" pos="n1" xml:id="B05626-001-a-2780">Proclamation</w>
     <w lemma="make" pos="vvi" xml:id="B05626-001-a-2790">make</w>
     <w lemma="intimation" pos="n1" xml:id="B05626-001-a-2800">Intimation</w>
     <pc xml:id="B05626-001-a-2810">,</pc>
     <w lemma="that" pos="cs" xml:id="B05626-001-a-2820">that</w>
     <w lemma="our" pos="po" xml:id="B05626-001-a-2830">Our</w>
     <w lemma="say" pos="j-vn" xml:id="B05626-001-a-2840">said</w>
     <w lemma="parliament" pos="n1" xml:id="B05626-001-a-2850">Parliament</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-2860">of</w>
     <w lemma="this" pos="d" xml:id="B05626-001-a-2870">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05626-001-a-2880">Kingdom</w>
     <w lemma="be" pos="vvz" xml:id="B05626-001-a-2890">is</w>
     <w lemma="adjourn" pos="vvn" xml:id="B05626-001-a-2900">Adjourned</w>
     <w lemma="to" pos="acp" xml:id="B05626-001-a-2910">to</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-2920">the</w>
     <w lemma="say" pos="vvd" xml:id="B05626-001-a-2930">said</w>
     <w lemma="twenty" pos="ord" xml:id="B05626-001-a-2940">Twentieth</w>
     <w lemma="day" pos="n1" xml:id="B05626-001-a-2950">Day</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-2960">of</w>
     <w lemma="March" pos="nn1" rend="hi" xml:id="B05626-001-a-2970">March</w>
     <w lemma="next" pos="ord" xml:id="B05626-001-a-2980">next</w>
     <w lemma="to" pos="prt" xml:id="B05626-001-a-2990">to</w>
     <w lemma="come" pos="vvi" xml:id="B05626-001-a-3000">come</w>
     <w lemma="and" pos="cc" xml:id="B05626-001-a-3010">And</w>
     <w lemma="ordain" pos="vvz" xml:id="B05626-001-a-3020">Ordains</w>
     <w lemma="these" pos="d" xml:id="B05626-001-a-3030">these</w>
     <w lemma="present" pos="n2" xml:id="B05626-001-a-3040">Presents</w>
     <w lemma="to" pos="prt" xml:id="B05626-001-a-3050">to</w>
     <w lemma="be" pos="vvi" xml:id="B05626-001-a-3060">be</w>
     <w lemma="print" pos="vvn" xml:id="B05626-001-a-3070">Printed</w>
     <pc unit="sentence" xml:id="B05626-001-a-3080">.</pc>
    </p>
    <closer xml:id="B05626-e240">
     <dateline xml:id="B05626-e250">
      <w lemma="give" pos="vvn" xml:id="B05626-001-a-3090">Given</w>
      <w lemma="under" pos="acp" xml:id="B05626-001-a-3100">under</w>
      <w lemma="our" pos="po" xml:id="B05626-001-a-3110">Our</w>
      <w lemma="signet" pos="n1" xml:id="B05626-001-a-3120">Signet</w>
      <w lemma="at" pos="acp" xml:id="B05626-001-a-3130">at</w>
      <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05626-001-a-3140">Edinburgh</w>
      <date xml:id="B05626-e270">
       <w lemma="the" pos="d" xml:id="B05626-001-a-3150">the</w>
       <w lemma="five" pos="ord" xml:id="B05626-001-a-3160">Fifth</w>
       <w lemma="day" pos="n1" xml:id="B05626-001-a-3170">Day</w>
       <w lemma="of" pos="acp" xml:id="B05626-001-a-3180">of</w>
       <hi xml:id="B05626-e280">
        <w lemma="November" pos="nn1" xml:id="B05626-001-a-3190">November</w>
        <pc xml:id="B05626-001-a-3200">,</pc>
       </hi>
       <w lemma="and" pos="cc" xml:id="B05626-001-a-3210">and</w>
       <w lemma="of" pos="acp" xml:id="B05626-001-a-3220">of</w>
       <w lemma="our" pos="po" xml:id="B05626-001-a-3230">Our</w>
       <w lemma="reign" pos="n1" xml:id="B05626-001-a-3240">Reign</w>
       <w lemma="the" pos="d" xml:id="B05626-001-a-3250">the</w>
       <w lemma="seven" pos="ord" xml:id="B05626-001-a-3260">Seventh</w>
       <w lemma="year" pos="n1" xml:id="B05626-001-a-3270">Year</w>
       <pc unit="sentence" xml:id="B05626-001-a-3280">.</pc>
      </date>
     </dateline>
     <signed xml:id="B05626-e290">
      <w lemma="n/a" pos="fla" xml:id="B05626-001-a-3290">Per</w>
      <w lemma="n/a" pos="fla" xml:id="B05626-001-a-3300">Actum</w>
      <w lemma="n/a" pos="fla" xml:id="B05626-001-a-3310">Dominorum</w>
      <w lemma="n/a" pos="fla" xml:id="B05626-001-a-3320">Secreti</w>
      <w lemma="n/a" pos="fla" xml:id="B05626-001-a-3330">Concilii</w>
      <pc unit="sentence" xml:id="B05626-001-a-3340">.</pc>
      <hi xml:id="B05626-e300">
       <w lemma="GILB" pos="nn1" xml:id="B05626-001-a-3350">GILB</w>
       <pc unit="sentence" xml:id="B05626-001-a-3360">.</pc>
       <w lemma="eliot" pos="nn1" xml:id="B05626-001-a-3370">ELIOT</w>
       <pc xml:id="B05626-001-a-3380">,</pc>
      </hi>
      <w lemma="Cls." pos="nn1" xml:id="B05626-001-a-3390">Cls.</w>
      <w lemma="sti." pos="ab" xml:id="B05626-001-a-3400">Sti.</w>
      <w lemma="n/a" pos="fla" xml:id="B05626-001-a-3410">Concilii</w>
      <pc unit="sentence" xml:id="B05626-001-a-3420">.</pc>
     </signed>
     <lb xml:id="B05626-e310"/>
     <w lemma="GOD" pos="nn1" xml:id="B05626-001-a-3430">GOD</w>
     <w lemma="save" pos="acp" xml:id="B05626-001-a-3440">Save</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-3450">the</w>
     <w lemma="king" pos="n1" xml:id="B05626-001-a-3460">KING</w>
     <pc unit="sentence" xml:id="B05626-001-a-3470">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="B05626-e320">
   <div type="colophon" xml:id="B05626-e330">
    <p xml:id="B05626-e340">
     <hi xml:id="B05626-e350">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05626-001-a-3480">Edinburgh</w>
      <pc xml:id="B05626-001-a-3490">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="B05626-001-a-3500">Printed</w>
     <w lemma="by" pos="acp" xml:id="B05626-001-a-3510">by</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-3520">the</w>
     <w lemma="heir" pos="n2" xml:id="B05626-001-a-3530">Heirs</w>
     <w lemma="and" pos="cc" xml:id="B05626-001-a-3540">and</w>
     <w lemma="successor" pos="n2" xml:id="B05626-001-a-3550">Successors</w>
     <w lemma="of" pos="acp" xml:id="B05626-001-a-3560">of</w>
     <hi xml:id="B05626-e360">
      <w lemma="Andrew" pos="nn1" xml:id="B05626-001-a-3570">Andrew</w>
      <w lemma="Anderson" pos="nn1" xml:id="B05626-001-a-3580">Anderson</w>
      <pc xml:id="B05626-001-a-3590">,</pc>
     </hi>
     <w lemma="printer" pos="n1" xml:id="B05626-001-a-3600">Printer</w>
     <w lemma="to" pos="acp" xml:id="B05626-001-a-3610">to</w>
     <w lemma="the" pos="d" xml:id="B05626-001-a-3620">the</w>
     <w lemma="king" pos="n2" xml:id="B05626-001-a-3630">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="B05626-001-a-3640">Most</w>
     <w lemma="excellent" pos="j" xml:id="B05626-001-a-3650">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="B05626-001-a-3660">Majesty</w>
     <pc xml:id="B05626-001-a-3670">,</pc>
     <w lemma="1695." pos="crd" xml:id="B05626-001-a-3680">1695.</w>
     <pc unit="sentence" xml:id="B05626-001-a-3690"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
