<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B05522">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title type="_245">A proclamation, anent the opening of the signet. At Edinburgh July 18, 1689.</title>
    <author>Scotland. Privy Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B05522 of text R183392 in the <ref target="B05522-http;//estc.bl.uk">English Short Title Catalog</ref>. Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in	 a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2KB of XML-encoded text transcribed from 2 1-bit group-IV TIFF page images.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B05522</idno>
    <idno type="stc">Wing S1675</idno>
    <idno type="stc">ESTC R183392</idno>
    <idno type="oclc">ocm 52529258</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this text, in whole or in part. Please contact project staff at eebotcp-info(at)umich.edu for further information or permissions.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online text creation partnership.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. B05522)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179001)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2775:72)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title type="_245">A proclamation, anent the opening of the signet. At Edinburgh July 18, 1689.</title>
      <author>Scotland. Privy Council.</author>
      <author>Scotland. Sovereign (1689-1694 : William and Mary)</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)   </extent>
     <publicationStmt>
      <publisher>Printed by the heir of Andrew Anderson, by order of his Majesties Privy Council,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>Anno Dom. 1689.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Initial letter.</note>
      <note>Signed: Gilb. Eliot, Cls. Sti. Concilii.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <encodingDesc>
   <projectDesc>
    <p>Header created with script mrcb2eeboutf.xsl on 2016-01-28.</p>
   </projectDesc>
   <editorialDecl n="4">
    <p>Manually keyed and coded text linked to page images in accordance with level 4 of the TEI in Libraries guidelines.</p>
    <p>Issued variously as SGML (TCP schema; ASCII text with mnemonic sdata character entities); displayable XML (TCP schema; characters represented either as UTF-8 Unicode or text strings within braces); or lossless XML (TEI P5, characters represented either as UTF-8 Unicode or TEI g elements).</p>
    <p>Keying and markup guidelines available at TCP web site (http://www.textcreationpartnership.org/docs/)</p>
   </editorialDecl>
  </encodingDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="lcsh">
     <term type="topical_term">Courts --  Scotland --  Early works to 1800.</term>
     <term type="genre_form">Broadsides --  Scotland --  17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A proclamation, anent the opening of the signet. : At Edinburgh July 18, 1689.</ep:title>
    <ep:author>Scotland. Privy Council.</ep:author>
    <ep:publicationYear>1689</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>2</ep:pageCount>
    <ep:wordCount>262</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2014-07 </date>
    <label>SPi Global</label>Keyed and coded from ProQuest page images 
      </change>
   <change>
    <date>2014-09 </date>
    <label>Mona Logarbo </label>Sampled and proofread 
      </change>
   <change>
    <date>2014-09 </date>
    <label>Mona Logarbo</label>Text and markup reviewed and edited 
      </change>
   <change>
    <date>2015-03 </date>
    <label>pfs</label>Batch review (QC) and XML conversion 
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B05522-e10010" xml:lang="eng">
  <body xml:id="B05522-e10020">
   <div type="royal_proclamation" xml:id="B05522-e10030">
    <pb facs="1" xml:id="B05522-001-a"/>
    <head xml:id="B05522-e10040">
     <figure rend="block" xml:id="B05522-e10050">
      <figDesc xml:id="B05522-e10060">Crown surmounting French fleur-de-lis with Scottish thistle and Tudor rose on either side, each of the latter two flanked by the Scottish unicorn and English lion respectively, against a background of stylized foliage</figDesc>
     </figure>
    </head>
    <head xml:id="B05522-e10070">
     <w lemma="a" pos="d" xml:id="B05522-001-a-0010">A</w>
     <w lemma="proclamation" pos="n1" xml:id="B05522-001-a-0020">PROCLAMATION</w>
     <pc xml:id="B05522-001-a-0030">,</pc>
     <w lemma="anent" pos="acp" xml:id="B05522-001-a-0040">Anent</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-0050">the</w>
     <w lemma="open" pos="n1-vg" xml:id="B05522-001-a-0060">opening</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-0070">of</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-0080">the</w>
     <w lemma="signet" pos="n1" xml:id="B05522-001-a-0090">Signet</w>
     <pc unit="sentence" xml:id="B05522-001-a-0100">.</pc>
    </head>
    <opener xml:id="B05522-e10080">
     <dateline xml:id="B05522-e10090">
      <w lemma="at" pos="acp" xml:id="B05522-001-a-0110">at</w>
      <w lemma="edinburgh" pos="nn1" rend="hi" xml:id="B05522-001-a-0120">Edinburgh</w>
      <date xml:id="B05522-e10100">
       <w lemma="july" pos="nn1" rend="hi" xml:id="B05522-001-a-0130">July</w>
       <w lemma="18" pos="crd" xml:id="B05522-001-a-0140">18</w>
       <pc xml:id="B05522-001-a-0150">,</pc>
       <w lemma="1689." pos="crd" xml:id="B05522-001-a-0160">1689.</w>
       <pc unit="sentence" xml:id="B05522-001-a-0170"/>
      </date>
     </dateline>
    </opener>
    <p xml:id="B05522-e10110">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="B05522-001-a-0180">WHereas</w>
     <w lemma="his" pos="po" xml:id="B05522-001-a-0190">His</w>
     <w lemma="majesty" pos="n1" xml:id="B05522-001-a-0200">Majesty</w>
     <pc xml:id="B05522-001-a-0210">,</pc>
     <w lemma="by" pos="acp" xml:id="B05522-001-a-0220">by</w>
     <w lemma="his" pos="po" xml:id="B05522-001-a-0230">His</w>
     <w lemma="letter" pos="n1" xml:id="B05522-001-a-0240">Letter</w>
     <w lemma="date" pos="vvn" xml:id="B05522-001-a-0250">dated</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-0260">the</w>
     <w lemma="ten" pos="ord" xml:id="B05522-001-a-0270">tenth</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-0280">of</w>
     <w lemma="this" pos="d" xml:id="B05522-001-a-0290">this</w>
     <w lemma="instant" pos="j" xml:id="B05522-001-a-0300">instant</w>
     <w lemma="july" pos="nn1" rendition="#hi" xml:id="B05522-001-a-0310">July</w>
     <pc xml:id="B05522-001-a-0320">,</pc>
     <w lemma="direct" pos="vvn" xml:id="B05522-001-a-0330">directed</w>
     <w lemma="to" pos="acp" xml:id="B05522-001-a-0340">to</w>
     <w lemma="his" pos="po" xml:id="B05522-001-a-0350">his</w>
     <w lemma="high" pos="j" xml:id="B05522-001-a-0360">High</w>
     <w lemma="commissioner" pos="n1" xml:id="B05522-001-a-0370">Commissioner</w>
     <pc xml:id="B05522-001-a-0380">,</pc>
     <w lemma="and" pos="cc" xml:id="B05522-001-a-0390">and</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-0400">the</w>
     <w lemma="lord" pos="n2" xml:id="B05522-001-a-0410">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-0420">of</w>
     <w lemma="his" pos="po" xml:id="B05522-001-a-0430">His</w>
     <w lemma="majesty" pos="ng1" xml:id="B05522-001-a-0440">Majesties</w>
     <w lemma="privy" pos="j" xml:id="B05522-001-a-0450">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05522-001-a-0460">Council</w>
     <pc xml:id="B05522-001-a-0470">;</pc>
     <w lemma="do" pos="vvd" xml:id="B05522-001-a-0480">Did</w>
     <w lemma="require" pos="vvi" xml:id="B05522-001-a-0490">Require</w>
     <w lemma="they" pos="pno" xml:id="B05522-001-a-0500">them</w>
     <w lemma="to" pos="prt" xml:id="B05522-001-a-0510">to</w>
     <w lemma="emit" pos="vvi" xml:id="B05522-001-a-0520">emit</w>
     <w lemma="a" pos="d" xml:id="B05522-001-a-0530">a</w>
     <w lemma="proclamation" pos="n1" xml:id="B05522-001-a-0540">Proclamation</w>
     <w lemma="in" pos="acp" xml:id="B05522-001-a-0550">in</w>
     <w lemma="his" pos="po" xml:id="B05522-001-a-0560">His</w>
     <w lemma="majesty" pos="ng1" xml:id="B05522-001-a-0570">Majesties</w>
     <w lemma="name" pos="n1" xml:id="B05522-001-a-0580">Name</w>
     <pc xml:id="B05522-001-a-0590">,</pc>
     <w lemma="signify" pos="vvg" xml:id="B05522-001-a-0600">Signifying</w>
     <w lemma="that" pos="cs" xml:id="B05522-001-a-0610">that</w>
     <w lemma="his" pos="po" xml:id="B05522-001-a-0620">His</w>
     <w lemma="majesty" pos="n1" xml:id="B05522-001-a-0630">Majesty</w>
     <w lemma="have" pos="vvd" xml:id="B05522-001-a-0640">had</w>
     <w lemma="consider" pos="vvn" xml:id="B05522-001-a-0650">considered</w>
     <w lemma="how" pos="crq" xml:id="B05522-001-a-0660">how</w>
     <w lemma="prejudicial" pos="j" xml:id="B05522-001-a-0670">prejudicial</w>
     <w lemma="it" pos="pn" xml:id="B05522-001-a-0680">it</w>
     <w lemma="be" pos="vvz" xml:id="B05522-001-a-0690">is</w>
     <w lemma="to" pos="acp" xml:id="B05522-001-a-0700">to</w>
     <w lemma="his" pos="po" xml:id="B05522-001-a-0710">His</w>
     <w lemma="lieges" pos="ng2" reg="lieges" xml:id="B05522-001-a-0720">Leidges</w>
     <pc xml:id="B05522-001-a-0730">,</pc>
     <w lemma="that" pos="d" xml:id="B05522-001-a-0740">that</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-0750">the</w>
     <w lemma="court" pos="n2" xml:id="B05522-001-a-0760">Courts</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-0770">of</w>
     <w lemma="justice" pos="n1" xml:id="B05522-001-a-0780">Justice</w>
     <w lemma="shall" pos="vmd" xml:id="B05522-001-a-0790">should</w>
     <w lemma="continue" pos="vvi" xml:id="B05522-001-a-0800">continue</w>
     <w lemma="so" pos="av" xml:id="B05522-001-a-0810">so</w>
     <w lemma="long" pos="av-j" xml:id="B05522-001-a-0820">long</w>
     <w lemma="silent" pos="j" xml:id="B05522-001-a-0830">silent</w>
     <pc xml:id="B05522-001-a-0840">,</pc>
     <w lemma="and" pos="cc" xml:id="B05522-001-a-0850">and</w>
     <w lemma="that" pos="cs" xml:id="B05522-001-a-0860">that</w>
     <w lemma="he" pos="pns" xml:id="B05522-001-a-0870">he</w>
     <w lemma="have" pos="vvd" xml:id="B05522-001-a-0880">had</w>
     <w lemma="think" pos="vvn" xml:id="B05522-001-a-0890">thought</w>
     <w lemma="fit" pos="j" xml:id="B05522-001-a-0900">fit</w>
     <w lemma="to" pos="prt" xml:id="B05522-001-a-0910">to</w>
     <w lemma="name" pos="vvi" xml:id="B05522-001-a-0920">name</w>
     <w lemma="some" pos="d" xml:id="B05522-001-a-0930">some</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-0940">of</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-0950">the</w>
     <w lemma="college" pos="n1" reg="college" xml:id="B05522-001-a-0960">Colledge</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-0970">of</w>
     <w lemma="justice" pos="n1" xml:id="B05522-001-a-0980">Justice</w>
     <pc xml:id="B05522-001-a-0990">,</pc>
     <w lemma="and" pos="cc" xml:id="B05522-001-a-1000">And</w>
     <w lemma="therefore" pos="av" xml:id="B05522-001-a-1010">therefore</w>
     <w lemma="to" pos="prt" xml:id="B05522-001-a-1020">to</w>
     <w lemma="declare" pos="vvi" xml:id="B05522-001-a-1030">Declare</w>
     <pc xml:id="B05522-001-a-1040">,</pc>
     <w lemma="that" pos="cs" xml:id="B05522-001-a-1050">that</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-1060">the</w>
     <w lemma="signet" pos="n1" xml:id="B05522-001-a-1070">Signet</w>
     <w lemma="be" pos="vvz" xml:id="B05522-001-a-1080">is</w>
     <w lemma="now" pos="av" xml:id="B05522-001-a-1090">now</w>
     <w lemma="open" pos="vvn" xml:id="B05522-001-a-1100">opened</w>
     <pc xml:id="B05522-001-a-1110">,</pc>
     <w lemma="that" pos="cs" xml:id="B05522-001-a-1120">That</w>
     <w lemma="all" pos="d" xml:id="B05522-001-a-1130">all</w>
     <w lemma="person" pos="n2" xml:id="B05522-001-a-1140">Persons</w>
     <w lemma="who" pos="crq" xml:id="B05522-001-a-1150">who</w>
     <w lemma="be" pos="vvb" xml:id="B05522-001-a-1160">are</w>
     <w lemma="any" pos="d" xml:id="B05522-001-a-1170">any</w>
     <w lemma="way" pos="n2" xml:id="B05522-001-a-1180">ways</w>
     <w lemma="concern" pos="vvd" xml:id="B05522-001-a-1190">concerned</w>
     <w lemma="therein" pos="av" xml:id="B05522-001-a-1200">therein</w>
     <pc xml:id="B05522-001-a-1210">,</pc>
     <w lemma="may" pos="vmb" xml:id="B05522-001-a-1220">may</w>
     <w lemma="have" pos="vvi" xml:id="B05522-001-a-1230">have</w>
     <w lemma="due" pos="j" xml:id="B05522-001-a-1240">due</w>
     <w lemma="notice" pos="n1" xml:id="B05522-001-a-1250">notice</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-1260">of</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-1270">the</w>
     <w lemma="same" pos="d" xml:id="B05522-001-a-1280">same</w>
     <pc xml:id="B05522-001-a-1290">,</pc>
     <w lemma="to" pos="acp" xml:id="B05522-001-a-1300">To</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-1310">the</w>
     <w lemma="end" pos="n1" xml:id="B05522-001-a-1320">end</w>
     <w lemma="that" pos="cs" xml:id="B05522-001-a-1330">that</w>
     <w lemma="law" pos="n1" xml:id="B05522-001-a-1340">Law</w>
     <w lemma="may" pos="vmb" xml:id="B05522-001-a-1350">may</w>
     <w lemma="have" pos="vvi" xml:id="B05522-001-a-1360">have</w>
     <w lemma="its" pos="po" xml:id="B05522-001-a-1370">its</w>
     <w lemma="due" pos="j" xml:id="B05522-001-a-1380">due</w>
     <w lemma="course" pos="n1" xml:id="B05522-001-a-1390">course</w>
     <pc unit="sentence" xml:id="B05522-001-a-1400">.</pc>
     <w lemma="therefore" pos="av" xml:id="B05522-001-a-1410">Therefore</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-1420">the</w>
     <w lemma="lord" pos="n1" xml:id="B05522-001-a-1430">Lord</w>
     <w lemma="high" pos="j" xml:id="B05522-001-a-1440">High</w>
     <w lemma="commissioner" pos="n1" xml:id="B05522-001-a-1450">Commissioner</w>
     <pc xml:id="B05522-001-a-1460">,</pc>
     <w lemma="and" pos="cc" xml:id="B05522-001-a-1470">and</w>
     <w lemma="lord" pos="n2" xml:id="B05522-001-a-1480">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-1490">of</w>
     <w lemma="privy" pos="j" xml:id="B05522-001-a-1500">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05522-001-a-1510">Council</w>
     <pc xml:id="B05522-001-a-1520">,</pc>
     <w lemma="do" pos="vvd" xml:id="B05522-001-a-1530">Do</w>
     <w lemma="ordain" pos="vvi" xml:id="B05522-001-a-1540">Ordain</w>
     <w lemma="a" pos="d" xml:id="B05522-001-a-1550">a</w>
     <w lemma="public" pos="j" reg="public" xml:id="B05522-001-a-1560">publick</w>
     <w lemma="proclamation" pos="n1" xml:id="B05522-001-a-1570">Proclamation</w>
     <w lemma="to" pos="prt" xml:id="B05522-001-a-1580">to</w>
     <w lemma="be" pos="vvi" xml:id="B05522-001-a-1590">be</w>
     <w lemma="make" pos="vvn" xml:id="B05522-001-a-1600">made</w>
     <pc xml:id="B05522-001-a-1610">,</pc>
     <w lemma="declare" pos="vvg" xml:id="B05522-001-a-1620">Declaring</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-1630">the</w>
     <w lemma="signet" pos="n1" xml:id="B05522-001-a-1640">Signet</w>
     <w lemma="to" pos="prt" xml:id="B05522-001-a-1650">to</w>
     <w lemma="be" pos="vvi" xml:id="B05522-001-a-1660">be</w>
     <w lemma="open" pos="j" xml:id="B05522-001-a-1670">open</w>
     <pc xml:id="B05522-001-a-1680">;</pc>
     <w lemma="but" pos="acp" xml:id="B05522-001-a-1690">But</w>
     <w lemma="in" pos="acp" xml:id="B05522-001-a-1700">in</w>
     <w lemma="regard" pos="n1" xml:id="B05522-001-a-1710">regard</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-1720">of</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-1730">the</w>
     <w lemma="present" pos="j" xml:id="B05522-001-a-1740">present</w>
     <w lemma="rebellion" pos="n1" xml:id="B05522-001-a-1750">Rebellion</w>
     <pc xml:id="B05522-001-a-1760">,</pc>
     <w lemma="they" pos="pns" xml:id="B05522-001-a-1770">They</w>
     <w lemma="stop" pos="vvb" xml:id="B05522-001-a-1780">stop</w>
     <w lemma="all" pos="d" xml:id="B05522-001-a-1790">all</w>
     <w lemma="personal" pos="j" xml:id="B05522-001-a-1800">personal</w>
     <w lemma="execution" pos="n1" xml:id="B05522-001-a-1810">Execution</w>
     <w lemma="until" pos="acp" xml:id="B05522-001-a-1820">until</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-1830">the</w>
     <w lemma="first" pos="ord" xml:id="B05522-001-a-1840">first</w>
     <w lemma="day" pos="n1" xml:id="B05522-001-a-1850">day</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-1860">of</w>
     <w lemma="november" pos="nn1" rend="hi" xml:id="B05522-001-a-1870">November</w>
     <w lemma="next" pos="ord" xml:id="B05522-001-a-1880">next</w>
     <pc xml:id="B05522-001-a-1890">;</pc>
     <w lemma="likeas" pos="nn1" xml:id="B05522-001-a-1900">Likeas</w>
     <w lemma="in" pos="acp" xml:id="B05522-001-a-1910">in</w>
     <w lemma="his" pos="po" xml:id="B05522-001-a-1920">His</w>
     <w lemma="majesty" pos="ng1" xml:id="B05522-001-a-1930">Majesties</w>
     <w lemma="name" pos="n1" xml:id="B05522-001-a-1940">Name</w>
     <w lemma="and" pos="cc" xml:id="B05522-001-a-1950">and</w>
     <w lemma="authority" pos="n1" xml:id="B05522-001-a-1960">Authority</w>
     <pc xml:id="B05522-001-a-1970">,</pc>
     <w lemma="they" pos="pns" xml:id="B05522-001-a-1980">They</w>
     <w lemma="do" pos="vvd" xml:id="B05522-001-a-1990">do</w>
     <w lemma="publish" pos="vvi" xml:id="B05522-001-a-2000">publish</w>
     <w lemma="and" pos="cc" xml:id="B05522-001-a-2010">and</w>
     <w lemma="declare" pos="vvi" xml:id="B05522-001-a-2020">declare</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-2030">the</w>
     <w lemma="same" pos="d" xml:id="B05522-001-a-2040">same</w>
     <pc xml:id="B05522-001-a-2050">,</pc>
     <w lemma="and" pos="cc" xml:id="B05522-001-a-2060">And</w>
     <w lemma="ordain" pos="vvz" xml:id="B05522-001-a-2070">Ordains</w>
     <w lemma="this" pos="d" xml:id="B05522-001-a-2080">this</w>
     <w lemma="proclamation" pos="n1" xml:id="B05522-001-a-2090">Proclamation</w>
     <w lemma="to" pos="prt" xml:id="B05522-001-a-2100">to</w>
     <w lemma="be" pos="vvi" xml:id="B05522-001-a-2110">be</w>
     <w lemma="publish" pos="vvn" xml:id="B05522-001-a-2120">published</w>
     <w lemma="at" pos="acp" xml:id="B05522-001-a-2130">at</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-2140">the</w>
     <w lemma="mercat" pos="n1" xml:id="B05522-001-a-2150">Mercat</w>
     <w lemma="cross" pos="n1" reg="across" xml:id="B05522-001-a-2160">Cross</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-2170">of</w>
     <w lemma="edinburgh" pos="nn1" rendition="#hi" xml:id="B05522-001-a-2180">Edinburgh</w>
     <pc unit="sentence" xml:id="B05522-001-a-2190">.</pc>
     <w lemma="extract" pos="vvn" xml:id="B05522-001-a-2200">Extracted</w>
     <w lemma="by" pos="acp" xml:id="B05522-001-a-2210">by</w>
     <w lemma="i" pos="png" xml:id="B05522-001-a-2220">me</w>
    </p>
    <closer xml:id="B05522-e10120">
     <signed xml:id="B05522-e10130">
      <hi xml:id="B05522-e10140">
       <w lemma="GILB" pos="nn1" xml:id="B05522-001-a-2230">GILB</w>
       <pc unit="sentence" xml:id="B05522-001-a-2240">.</pc>
       <w lemma="eliot" pos="nn1" xml:id="B05522-001-a-2250">ELIOT</w>
      </hi>
      <pc rendition="#follows-hi" xml:id="B05522-001-a-2260">,</pc>
      <w lemma="Cls." pos="nn1" xml:id="B05522-001-a-2270">Cls.</w>
      <w lemma="n/a" pos="fla" xml:id="B05522-001-a-2280">Sti</w>
      <pc unit="sentence" xml:id="B05522-001-a-2290">.</pc>
      <w lemma="n/a" pos="fla" xml:id="B05522-001-a-2300">Concilii</w>
      <pc unit="sentence" xml:id="B05522-001-a-2310">.</pc>
     </signed>
     <lb xml:id="B05522-e10150"/>
     <w lemma="god" pos="nn1" xml:id="B05522-001-a-2320">God</w>
     <w lemma="save" pos="acp" xml:id="B05522-001-a-2330">save</w>
     <w lemma="king" pos="n1" xml:id="B05522-001-a-2340">King</w>
     <w lemma="william" pos="nn1" xml:id="B05522-001-a-2350">William</w>
     <w lemma="and" pos="cc" xml:id="B05522-001-a-2360">and</w>
     <w lemma="queen" pos="n1" xml:id="B05522-001-a-2370">Queen</w>
     <w lemma="marry" pos="vvi" xml:id="B05522-001-a-2380">Mary</w>
     <pc unit="sentence" xml:id="B05522-001-a-2390">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="B05522-e10160">
   <div type="colophon" xml:id="B05522-e10170">
    <p xml:id="B05522-e10180">
     <w lemma="edinburgh" pos="nn1" rendition="#hi" xml:id="B05522-001-a-2400">Edinburgh</w>
     <pc xml:id="B05522-001-a-2410">,</pc>
     <w lemma="print" pos="vvn" xml:id="B05522-001-a-2420">Printed</w>
     <w lemma="by" pos="acp" xml:id="B05522-001-a-2430">by</w>
     <w lemma="the" pos="d" xml:id="B05522-001-a-2440">the</w>
     <w lemma="heir" pos="n1" xml:id="B05522-001-a-2450">Heir</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-2460">of</w>
     <hi xml:id="B05522-e10190">
      <w lemma="andrew" pos="nn1" xml:id="B05522-001-a-2470">Andrew</w>
      <w lemma="anderson" pos="nn1" xml:id="B05522-001-a-2480">Anderson</w>
     </hi>
     <pc rendition="#follows-hi" xml:id="B05522-001-a-2490">,</pc>
     <w lemma="by" pos="acp" xml:id="B05522-001-a-2500">By</w>
     <w lemma="order" pos="n1" xml:id="B05522-001-a-2510">Order</w>
     <w lemma="of" pos="acp" xml:id="B05522-001-a-2520">of</w>
     <w lemma="his" pos="po" xml:id="B05522-001-a-2530">His</w>
     <w lemma="majesty" pos="ng1" xml:id="B05522-001-a-2540">Majesties</w>
     <w lemma="privy" pos="j" xml:id="B05522-001-a-2550">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05522-001-a-2560">Council</w>
     <pc xml:id="B05522-001-a-2570">,</pc>
     <w lemma="n/a" pos="fla" xml:id="B05522-001-a-2580">Ano</w>
     <w lemma="n/a" pos="fla" rendition="#hi" xml:id="B05522-001-a-2590">Dom</w>
     <pc unit="sentence" xml:id="B05522-001-a-2600">.</pc>
     <w lemma="1689." pos="crd" xml:id="B05522-001-a-2610">1689.</w>
     <pc unit="sentence" xml:id="B05522-001-a-2620"/>
    </p>
    <pb facs="2" xml:id="B05522-002-a"/>
   </div>
  </back>
 </text>
</TEI>
