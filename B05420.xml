<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B05420">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A proclamation adjourning the Parliament from the 17 of August, to the 16 of November, 1692.</title>
    <author>Scotland. Privy Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B05420 of text R226030 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing S1553). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B05420</idno>
    <idno type="STC">Wing S1553</idno>
    <idno type="STC">ESTC R226030</idno>
    <idno type="EEBO-CITATION">52529223</idno>
    <idno type="OCLC">ocm 52529223</idno>
    <idno type="VID">178952</idno>
    <idno type="PROQUESTGOID">2248539216</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. B05420)</note>
    <note>Transcribed from: (Early English Books Online ; image set 178952)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2775:23)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A proclamation adjourning the Parliament from the 17 of August, to the 16 of November, 1692.</title>
      <author>Scotland. Privy Council.</author>
      <author>Scotland. Sovereign (1689-1694 : William and Mary)</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by the heir of Andrew Anderson, Printer to their most excellent Majesties,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>Anno Dom. 1692.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Royal arms at head of text; initial letter.</note>
      <note>Signed at end: Gilb. Eliot, Cls. Sti. Concilii.</note>
      <note>Dated at end: Given under Our Signet at Edinburgh the twenty sixth day of July, and of Our Reign the fourth year, 1692.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Scotland. -- Parliament -- Early works to 1800.</term>
     <term>Scotland -- Politics and government -- 1689-1745 -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A proclamation, adjourning the Parliament from the 17 of August, to the 16 of November, 1692.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1692</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>460</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-10</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-12</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2009-03</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-03</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B05420-e10010">
  <body xml:id="B05420-e10020">
   <div type="proclamation" xml:id="B05420-e10030">
    <pb facs="tcp:178952:1" rend="simple:additions" xml:id="B05420-001-a"/>
    <head xml:id="B05420-e10040">
     <figure xml:id="B05420-e10050">
      <figure xml:id="B05420-e10060">
       <figDesc xml:id="B05420-e10070">monogram of 'W' (William) superimposed on' M' (Mary)</figDesc>
      </figure>
      <p xml:id="B05420-e10080">
       <w lemma="n/a" pos="ffr" xml:id="B05420-001-a-0010">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="B05420-001-a-0020">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="B05420-001-a-0030">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="B05420-001-a-0040">DROIT</w>
      </p>
      <p xml:id="B05420-e10090">
       <w lemma="n/a" pos="ffr" xml:id="B05420-001-a-0050">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="B05420-001-a-0060">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="B05420-001-a-0070">QUI</w>
       <w lemma="n/a" pos="ffr" xml:id="B05420-001-a-0080">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="B05420-001-a-0090">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="B05420-001-a-0100">PENSE</w>
      </p>
      <figDesc xml:id="B05420-e10100">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="B05420-e10110">
     <hi xml:id="B05420-e10120">
      <w lemma="a" pos="d" xml:id="B05420-001-a-0110">A</w>
      <w lemma="proclamation" pos="n1" xml:id="B05420-001-a-0120">PROCLAMATION</w>
     </hi>
     <pc rend="follows-hi" xml:id="B05420-001-a-0130">,</pc>
     <w lemma="adjourn" pos="vvg" xml:id="B05420-001-a-0140">Adjourning</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-0150">the</w>
     <w lemma="parliament" pos="n1" xml:id="B05420-001-a-0160">Parliament</w>
     <w lemma="from" pos="acp" xml:id="B05420-001-a-0170">From</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-0180">the</w>
     <w lemma="17" pos="crd" xml:id="B05420-001-a-0190">17</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-0200">of</w>
     <w lemma="August" pos="nn1" rend="hi" xml:id="B05420-001-a-0210">August</w>
     <pc xml:id="B05420-001-a-0220">,</pc>
     <w lemma="to" pos="acp" xml:id="B05420-001-a-0230">to</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-0240">the</w>
     <w lemma="16" pos="crd" xml:id="B05420-001-a-0250">16</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-0260">of</w>
     <w lemma="November" pos="nn1" rend="hi" xml:id="B05420-001-a-0270">November</w>
     <pc xml:id="B05420-001-a-0280">,</pc>
     <w lemma="1692." pos="crd" xml:id="B05420-001-a-0290">1692.</w>
     <pc unit="sentence" xml:id="B05420-001-a-0300"/>
    </head>
    <opener xml:id="B05420-e10150">
     <signed xml:id="B05420-e10160">
      <w lemma="WILLIAM" pos="nn1" rend="hi" xml:id="B05420-001-a-0310">WILLIAM</w>
      <w lemma="and" pos="cc" xml:id="B05420-001-a-0320">and</w>
      <w lemma="MARY" pos="nn1" rend="hi" xml:id="B05420-001-a-0330">MARY</w>
      <w lemma="by" pos="acp" xml:id="B05420-001-a-0340">by</w>
      <w lemma="the" pos="d" xml:id="B05420-001-a-0350">the</w>
      <w lemma="grace" pos="n1" xml:id="B05420-001-a-0360">grace</w>
      <w lemma="of" pos="acp" xml:id="B05420-001-a-0370">of</w>
      <w lemma="GOD" pos="nn1" xml:id="B05420-001-a-0380">GOD</w>
      <pc xml:id="B05420-001-a-0390">,</pc>
      <w lemma="king" pos="n1" xml:id="B05420-001-a-0400">King</w>
      <w lemma="and" pos="cc" xml:id="B05420-001-a-0410">and</w>
      <w lemma="queen" pos="n1" xml:id="B05420-001-a-0420">Queen</w>
      <w lemma="of" pos="acp" xml:id="B05420-001-a-0430">of</w>
      <hi xml:id="B05420-e10190">
       <w lemma="great-britain" pos="nn1" xml:id="B05420-001-a-0440">Great-Britain</w>
       <pc xml:id="B05420-001-a-0450">,</pc>
       <w lemma="France" pos="nn1" xml:id="B05420-001-a-0460">France</w>
      </hi>
      <w lemma="and" pos="cc" xml:id="B05420-001-a-0470">and</w>
      <w lemma="Ireland" pos="nn1" rend="hi" xml:id="B05420-001-a-0480">Ireland</w>
      <pc xml:id="B05420-001-a-0490">,</pc>
      <w lemma="defender" pos="n2" xml:id="B05420-001-a-0500">Defenders</w>
      <w lemma="of" pos="acp" xml:id="B05420-001-a-0510">of</w>
      <w lemma="the" pos="d" xml:id="B05420-001-a-0520">the</w>
      <w lemma="faith" pos="n1" xml:id="B05420-001-a-0530">Faith</w>
      <pc xml:id="B05420-001-a-0540">,</pc>
     </signed>
     <salute xml:id="B05420-e10210">
      <w lemma="to" pos="prt" xml:id="B05420-001-a-0550">To</w>
      <w lemma="our" pos="po" xml:id="B05420-001-a-0560">our</w>
      <w lemma="lion" pos="n1" reg="Lion" xml:id="B05420-001-a-0570">Lyon</w>
      <w lemma="king" pos="n1" xml:id="B05420-001-a-0580">King</w>
      <w lemma="at" pos="acp" xml:id="B05420-001-a-0590">at</w>
      <w lemma="arm" pos="n2" xml:id="B05420-001-a-0600">Arms</w>
      <pc xml:id="B05420-001-a-0610">,</pc>
      <w lemma="and" pos="cc" xml:id="B05420-001-a-0620">and</w>
      <w lemma="his" pos="po" xml:id="B05420-001-a-0630">his</w>
      <w lemma="brethren" pos="n2" xml:id="B05420-001-a-0640">brethren</w>
      <w lemma="herald" pos="n2" reg="Heralds" xml:id="B05420-001-a-0650">Heraulds</w>
      <pc xml:id="B05420-001-a-0660">,</pc>
      <w lemma="macer" pos="n2" xml:id="B05420-001-a-0670">Macers</w>
      <w lemma="of" pos="acp" xml:id="B05420-001-a-0680">of</w>
      <w lemma="our" pos="po" xml:id="B05420-001-a-0690">Our</w>
      <w lemma="privy" pos="j" xml:id="B05420-001-a-0700">Privy</w>
      <w lemma="council" pos="n1" xml:id="B05420-001-a-0710">Council</w>
      <pc xml:id="B05420-001-a-0720">,</pc>
      <w lemma="pursuivant" pos="n2" reg="Pursuivants" xml:id="B05420-001-a-0730">Pursevants</w>
      <pc xml:id="B05420-001-a-0740">,</pc>
      <w lemma="messenger" pos="n2" xml:id="B05420-001-a-0750">Messengers</w>
      <w lemma="at" pos="acp" xml:id="B05420-001-a-0760">at</w>
      <w lemma="arm" pos="n2" xml:id="B05420-001-a-0770">Arms</w>
      <pc xml:id="B05420-001-a-0780">,</pc>
      <w lemma="our" pos="po" xml:id="B05420-001-a-0790">Our</w>
      <w lemma="sheriff" pos="n2" xml:id="B05420-001-a-0800">Sheriffs</w>
      <w lemma="in" pos="acp" xml:id="B05420-001-a-0810">in</w>
      <w lemma="that" pos="d" xml:id="B05420-001-a-0820">that</w>
      <w lemma="part" pos="n1" xml:id="B05420-001-a-0830">part</w>
      <w lemma="conjunct" pos="av-j" xml:id="B05420-001-a-0840">conjunctly</w>
      <w lemma="and" pos="cc" xml:id="B05420-001-a-0850">and</w>
      <w lemma="several" pos="av-j" xml:id="B05420-001-a-0860">severally</w>
      <w lemma="special" pos="av-j" xml:id="B05420-001-a-0870">specially</w>
      <w lemma="constitute" pos="vvi" xml:id="B05420-001-a-0880">constitute</w>
      <w lemma="greeting" pos="n1" xml:id="B05420-001-a-0890">greeting</w>
      <pc xml:id="B05420-001-a-0900">;</pc>
     </salute>
    </opener>
    <p xml:id="B05420-e10220">
     <w lemma="forasmuch" pos="av" xml:id="B05420-001-a-0910">Forasmuch</w>
     <w lemma="as" pos="acp" xml:id="B05420-001-a-0920">as</w>
     <w lemma="we" pos="pns" xml:id="B05420-001-a-0930">We</w>
     <pc xml:id="B05420-001-a-0940">,</pc>
     <w lemma="with" pos="acp" xml:id="B05420-001-a-0950">with</w>
     <w lemma="advice" pos="n1" xml:id="B05420-001-a-0960">Advice</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-0970">of</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-0980">the</w>
     <w lemma="lord" pos="n2" xml:id="B05420-001-a-0990">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-1000">of</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-1010">our</w>
     <w lemma="privy" pos="j" xml:id="B05420-001-a-1020">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05420-001-a-1030">Council</w>
     <pc xml:id="B05420-001-a-1040">,</pc>
     <w lemma="do" pos="vvd" xml:id="B05420-001-a-1050">did</w>
     <w lemma="by" pos="acp" xml:id="B05420-001-a-1060">by</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-1070">Our</w>
     <w lemma="proclamation" pos="n1" xml:id="B05420-001-a-1080">Proclamation</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-1090">of</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-1100">the</w>
     <w lemma="date" pos="n1" xml:id="B05420-001-a-1110">date</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-1120">the</w>
     <w lemma="twenty" pos="crd" reg="twenty" xml:id="B05420-001-a-1130">twentie</w>
     <w lemma="nine" pos="ord" xml:id="B05420-001-a-1140">ninth</w>
     <w lemma="day" pos="n1" xml:id="B05420-001-a-1150">day</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-1160">of</w>
     <w lemma="March" pos="nn1" rend="hi" xml:id="B05420-001-a-1170">March</w>
     <w lemma="last" pos="ord" xml:id="B05420-001-a-1180">last</w>
     <w lemma="bypass" pos="j" reg="bypast" xml:id="B05420-001-a-1190">by-past</w>
     <pc xml:id="B05420-001-a-1200">,</pc>
     <w lemma="adjourn" pos="vvb" xml:id="B05420-001-a-1210">Adjourn</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-1220">the</w>
     <w lemma="current" pos="j" xml:id="B05420-001-a-1230">current</w>
     <w lemma="parliament" pos="n1" xml:id="B05420-001-a-1240">Parliament</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-1250">of</w>
     <w lemma="this" pos="d" xml:id="B05420-001-a-1260">this</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-1270">Our</w>
     <w lemma="ancient" pos="j" reg="ancient" xml:id="B05420-001-a-1280">antient</w>
     <w lemma="kingdom" pos="n1" xml:id="B05420-001-a-1290">Kingdom</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-1300">of</w>
     <w lemma="Scotland" pos="nn1" rend="hi" xml:id="B05420-001-a-1310">Scotland</w>
     <pc xml:id="B05420-001-a-1320">,</pc>
     <w lemma="from" pos="acp" xml:id="B05420-001-a-1330">from</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-1340">the</w>
     <w lemma="fifteen" pos="ord" xml:id="B05420-001-a-1350">fifteenth</w>
     <w lemma="day" pos="n1" xml:id="B05420-001-a-1360">day</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-1370">of</w>
     <w lemma="April" pos="nn1" rend="hi" xml:id="B05420-001-a-1380">April</w>
     <w lemma="last" pos="ord" xml:id="B05420-001-a-1390">last</w>
     <w lemma="by" pos="acp" xml:id="B05420-001-a-1400">by</w>
     <w lemma="past" pos="j" xml:id="B05420-001-a-1410">past</w>
     <pc xml:id="B05420-001-a-1420">,</pc>
     <w lemma="to" pos="prt" xml:id="B05420-001-a-1430">to</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-1440">the</w>
     <w lemma="seventeen" pos="ord" xml:id="B05420-001-a-1450">seventeenth</w>
     <w lemma="day" pos="n1" xml:id="B05420-001-a-1460">day</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-1470">of</w>
     <w lemma="August" pos="nn1" rend="hi" xml:id="B05420-001-a-1480">August</w>
     <w lemma="next" pos="ord" xml:id="B05420-001-a-1490">next</w>
     <pc xml:id="B05420-001-a-1500">,</pc>
     <w lemma="in" pos="acp" xml:id="B05420-001-a-1510">in</w>
     <w lemma="this" pos="d" xml:id="B05420-001-a-1520">this</w>
     <w lemma="present" pos="j" xml:id="B05420-001-a-1530">present</w>
     <w lemma="year" pos="n1" xml:id="B05420-001-a-1540">year</w>
     <w lemma="one" pos="crd" xml:id="B05420-001-a-1550">One</w>
     <w lemma="thousand" pos="crd" xml:id="B05420-001-a-1560">thousand</w>
     <w lemma="six" pos="crd" xml:id="B05420-001-a-1570">six</w>
     <w lemma="hundred" pos="crd" xml:id="B05420-001-a-1580">hundred</w>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-1590">and</w>
     <w lemma="ninety" pos="crd" xml:id="B05420-001-a-1600">ninety</w>
     <w lemma="two" pos="crd" xml:id="B05420-001-a-1610">two</w>
     <pc xml:id="B05420-001-a-1620">;</pc>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-1630">And</w>
     <w lemma="now" pos="av" xml:id="B05420-001-a-1640">now</w>
     <w lemma="in" pos="acp" xml:id="B05420-001-a-1650">in</w>
     <w lemma="regard" pos="n1" xml:id="B05420-001-a-1660">regard</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-1670">of</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-1680">Our</w>
     <w lemma="absence" pos="n1" xml:id="B05420-001-a-1690">absence</w>
     <pc xml:id="B05420-001-a-1700">,</pc>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-1710">and</w>
     <w lemma="that" pos="cs" xml:id="B05420-001-a-1720">that</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-1730">our</w>
     <w lemma="affair" pos="n2" xml:id="B05420-001-a-1740">Affairs</w>
     <w lemma="in" pos="acp" xml:id="B05420-001-a-1750">in</w>
     <w lemma="this" pos="d" xml:id="B05420-001-a-1760">this</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-1770">our</w>
     <w lemma="say" pos="j-vn" xml:id="B05420-001-a-1780">said</w>
     <w lemma="ancient" pos="j" reg="ancient" xml:id="B05420-001-a-1790">antient</w>
     <w lemma="kingdom" pos="n1" xml:id="B05420-001-a-1800">Kingdom</w>
     <pc xml:id="B05420-001-a-1810">,</pc>
     <w lemma="do" pos="vvb" xml:id="B05420-001-a-1820">do</w>
     <w lemma="not" pos="xx" xml:id="B05420-001-a-1830">not</w>
     <w lemma="require" pos="vvi" xml:id="B05420-001-a-1840">require</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-1850">the</w>
     <w lemma="meeting" pos="n1" xml:id="B05420-001-a-1860">Meeting</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-1870">of</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-1880">our</w>
     <w lemma="parliament" pos="n1" xml:id="B05420-001-a-1890">Parliament</w>
     <w lemma="so" pos="av" xml:id="B05420-001-a-1900">so</w>
     <w lemma="soon" pos="av" xml:id="B05420-001-a-1910">soon</w>
     <pc xml:id="B05420-001-a-1920">,</pc>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-1930">and</w>
     <w lemma="that" pos="cs" xml:id="B05420-001-a-1940">that</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-1950">the</w>
     <w lemma="member" pos="n2" xml:id="B05420-001-a-1960">Members</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-1970">of</w>
     <w lemma="parliament" pos="n1" xml:id="B05420-001-a-1980">Parliament</w>
     <w lemma="may" pos="vmb" xml:id="B05420-001-a-1990">may</w>
     <w lemma="not" pos="xx" xml:id="B05420-001-a-2000">not</w>
     <w lemma="be" pos="vvi" xml:id="B05420-001-a-2010">be</w>
     <w lemma="put" pos="vvn" xml:id="B05420-001-a-2020">put</w>
     <w lemma="to" pos="acp" xml:id="B05420-001-a-2030">to</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-2040">the</w>
     <w lemma="trouble" pos="n1" xml:id="B05420-001-a-2050">trouble</w>
     <w lemma="or" pos="cc" xml:id="B05420-001-a-2060">or</w>
     <w lemma="charge" pos="n1" xml:id="B05420-001-a-2070">charge</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-2080">of</w>
     <w lemma="meet" pos="vvg" xml:id="B05420-001-a-2090">Meeting</w>
     <w lemma="upon" pos="acp" xml:id="B05420-001-a-2100">upon</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-2110">the</w>
     <w lemma="say" pos="j-vn" xml:id="B05420-001-a-2120">said</w>
     <w lemma="day" pos="n1" xml:id="B05420-001-a-2130">day</w>
     <pc xml:id="B05420-001-a-2140">;</pc>
     <w lemma="we" pos="pns" xml:id="B05420-001-a-2150">We</w>
     <w lemma="therefore" pos="av" xml:id="B05420-001-a-2160">therefore</w>
     <w lemma="with" pos="acp" xml:id="B05420-001-a-2170">with</w>
     <w lemma="advice" pos="n1" xml:id="B05420-001-a-2180">advice</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-2190">of</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-2200">the</w>
     <w lemma="lord" pos="n2" xml:id="B05420-001-a-2210">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-2220">of</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-2230">our</w>
     <w lemma="privy" pos="j" xml:id="B05420-001-a-2240">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05420-001-a-2250">Council</w>
     <pc xml:id="B05420-001-a-2260">,</pc>
     <w lemma="hereby" pos="av" xml:id="B05420-001-a-2270">hereby</w>
     <w lemma="adjourn" pos="vvb" xml:id="B05420-001-a-2280">Adjourn</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-2290">our</w>
     <w lemma="say" pos="j-vn" xml:id="B05420-001-a-2300">said</w>
     <w lemma="current" pos="n1" xml:id="B05420-001-a-2310">current</w>
     <w lemma="parliament" pos="n1" xml:id="B05420-001-a-2320">Parliament</w>
     <pc xml:id="B05420-001-a-2330">,</pc>
     <w lemma="until" pos="acp" xml:id="B05420-001-a-2340">until</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-2350">the</w>
     <w lemma="sixteenth" pos="ord" xml:id="B05420-001-a-2360">Sixteenth</w>
     <w lemma="day" pos="n1" xml:id="B05420-001-a-2370">day</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-2380">of</w>
     <w lemma="November" pos="nn1" rend="hi" xml:id="B05420-001-a-2390">November</w>
     <w lemma="next" pos="ord" xml:id="B05420-001-a-2400">next</w>
     <w lemma="to" pos="prt" xml:id="B05420-001-a-2410">to</w>
     <w lemma="come" pos="vvi" xml:id="B05420-001-a-2420">come</w>
     <pc xml:id="B05420-001-a-2430">,</pc>
     <w lemma="in" pos="acp" xml:id="B05420-001-a-2440">in</w>
     <w lemma="this" pos="d" xml:id="B05420-001-a-2450">this</w>
     <w lemma="say" pos="vvd" xml:id="B05420-001-a-2460">said</w>
     <w lemma="instant" pos="j" xml:id="B05420-001-a-2470">instant</w>
     <w lemma="year" pos="n1" xml:id="B05420-001-a-2480">year</w>
     <pc xml:id="B05420-001-a-2490">,</pc>
     <w lemma="one" pos="crd" xml:id="B05420-001-a-2500">One</w>
     <w lemma="thousand" pos="crd" xml:id="B05420-001-a-2510">thousand</w>
     <w lemma="six" pos="crd" xml:id="B05420-001-a-2520">six</w>
     <w lemma="hundred" pos="crd" xml:id="B05420-001-a-2530">hundred</w>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-2540">and</w>
     <w lemma="ninety" pos="crd" xml:id="B05420-001-a-2550">ninety</w>
     <w lemma="two" pos="crd" xml:id="B05420-001-a-2560">two</w>
     <pc xml:id="B05420-001-a-2570">,</pc>
     <w lemma="hereby" pos="av" xml:id="B05420-001-a-2580">hereby</w>
     <w lemma="require" pos="vvg" xml:id="B05420-001-a-2590">requiring</w>
     <w lemma="all" pos="d" xml:id="B05420-001-a-2600">all</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-2610">the</w>
     <w lemma="member" pos="n2" xml:id="B05420-001-a-2620">Members</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-2630">of</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-2640">Our</w>
     <w lemma="parliament" pos="n1" xml:id="B05420-001-a-2650">Parliament</w>
     <pc xml:id="B05420-001-a-2660">,</pc>
     <w lemma="to" pos="prt" xml:id="B05420-001-a-2670">to</w>
     <w lemma="give" pos="vvi" xml:id="B05420-001-a-2680">give</w>
     <w lemma="attendance" pos="n1" xml:id="B05420-001-a-2690">Attendance</w>
     <w lemma="at" pos="acp" xml:id="B05420-001-a-2700">at</w>
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05420-001-a-2710">Edinburgh</w>
     <w lemma="on" pos="acp" xml:id="B05420-001-a-2720">on</w>
     <w lemma="that" pos="d" xml:id="B05420-001-a-2730">that</w>
     <w lemma="day" pos="n1" xml:id="B05420-001-a-2740">day</w>
     <pc xml:id="B05420-001-a-2750">,</pc>
     <w lemma="in" pos="acp" xml:id="B05420-001-a-2760">in</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-2770">the</w>
     <w lemma="accustom" pos="j-vn" xml:id="B05420-001-a-2780">accustomed</w>
     <w lemma="manner" pos="n1" xml:id="B05420-001-a-2790">manner</w>
     <pc xml:id="B05420-001-a-2800">,</pc>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-2810">and</w>
     <w lemma="under" pos="acp" xml:id="B05420-001-a-2820">under</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-2830">the</w>
     <w lemma="certification" pos="n2" xml:id="B05420-001-a-2840">Certifications</w>
     <w lemma="contain" pos="vvn" xml:id="B05420-001-a-2850">contained</w>
     <w lemma="in" pos="acp" xml:id="B05420-001-a-2860">in</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-2870">the</w>
     <w lemma="several" pos="j" xml:id="B05420-001-a-2880">several</w>
     <w lemma="act" pos="n2" xml:id="B05420-001-a-2890">Acts</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-2900">of</w>
     <w lemma="parliament" pos="n1" xml:id="B05420-001-a-2910">Parliament</w>
     <pc unit="sentence" xml:id="B05420-001-a-2920">.</pc>
     <w lemma="our" pos="po" xml:id="B05420-001-a-2930">OUR</w>
     <w lemma="will" pos="n1" xml:id="B05420-001-a-2940">WILL</w>
     <w lemma="be" pos="vvz" xml:id="B05420-001-a-2950">IS</w>
     <w lemma="herefore" pos="av" xml:id="B05420-001-a-2960">HEREFORE</w>
     <pc xml:id="B05420-001-a-2970">,</pc>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-2980">and</w>
     <w lemma="we" pos="pns" xml:id="B05420-001-a-2990">We</w>
     <w lemma="charge" pos="vvb" xml:id="B05420-001-a-3000">charge</w>
     <w lemma="you" pos="pn" xml:id="B05420-001-a-3010">you</w>
     <w lemma="strict" pos="av-j" xml:id="B05420-001-a-3020">strictly</w>
     <pc xml:id="B05420-001-a-3030">,</pc>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-3040">and</w>
     <w lemma="command" pos="vvb" xml:id="B05420-001-a-3050">Command</w>
     <pc xml:id="B05420-001-a-3060">,</pc>
     <w lemma="that" pos="cs" xml:id="B05420-001-a-3070">that</w>
     <w lemma="in-continent" pos="n1" xml:id="B05420-001-a-3080">in-continent</w>
     <w lemma="these" pos="d" xml:id="B05420-001-a-3090">these</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-3100">Our</w>
     <w lemma="letter" pos="n2" xml:id="B05420-001-a-3110">Letters</w>
     <w lemma="see" pos="vvn" xml:id="B05420-001-a-3120">seen</w>
     <pc xml:id="B05420-001-a-3130">,</pc>
     <w lemma="you" pos="pn" xml:id="B05420-001-a-3140">ye</w>
     <w lemma="pass" pos="vvb" xml:id="B05420-001-a-3150">pass</w>
     <w lemma="to" pos="acp" xml:id="B05420-001-a-3160">to</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-3170">the</w>
     <w lemma="mercat-cross" pos="j" xml:id="B05420-001-a-3180">Mercat-cross</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-3190">of</w>
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05420-001-a-3200">Edinburgh</w>
     <pc xml:id="B05420-001-a-3210">,</pc>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-3220">and</w>
     <w lemma="to" pos="acp" xml:id="B05420-001-a-3230">to</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-3240">the</w>
     <w lemma="mercat-cross" pos="n2" xml:id="B05420-001-a-3250">Mercat-crosses</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-3260">of</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-3270">the</w>
     <w lemma="whole" pos="j" xml:id="B05420-001-a-3280">whole</w>
     <w lemma="headburgh" pos="n2" xml:id="B05420-001-a-3290">Head-burghs</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-3300">of</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-3310">the</w>
     <w lemma="several" pos="j" xml:id="B05420-001-a-3320">several</w>
     <w lemma="shire" pos="n2" xml:id="B05420-001-a-3330">Shires</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-3340">of</w>
     <w lemma="this" pos="d" xml:id="B05420-001-a-3350">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05420-001-a-3360">Kingdom</w>
     <pc xml:id="B05420-001-a-3370">,</pc>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-3380">and</w>
     <w lemma="there" pos="av" xml:id="B05420-001-a-3390">there</w>
     <w lemma="in" pos="acp" xml:id="B05420-001-a-3400">in</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-3410">Our</w>
     <w lemma="name" pos="n1" xml:id="B05420-001-a-3420">Name</w>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-3430">and</w>
     <w lemma="authority" pos="n1" xml:id="B05420-001-a-3440">Authority</w>
     <pc xml:id="B05420-001-a-3450">,</pc>
     <w lemma="by" pos="acp" xml:id="B05420-001-a-3460">by</w>
     <w lemma="open" pos="j" xml:id="B05420-001-a-3470">open</w>
     <w lemma="proclamation" pos="n1" xml:id="B05420-001-a-3480">Proclamation</w>
     <pc xml:id="B05420-001-a-3490">,</pc>
     <w lemma="make" pos="vvb" xml:id="B05420-001-a-3500">make</w>
     <w lemma="publication" pos="n1" xml:id="B05420-001-a-3510">publication</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-3520">of</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-3530">the</w>
     <w lemma="say" pos="j-vn" xml:id="B05420-001-a-3540">said</w>
     <w lemma="adjournment" pos="n1" xml:id="B05420-001-a-3550">Adjournment</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-3560">of</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-3570">Our</w>
     <w lemma="current" pos="j" xml:id="B05420-001-a-3580">current</w>
     <w lemma="parliament" pos="n1" xml:id="B05420-001-a-3590">Parliament</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-3600">of</w>
     <w lemma="this" pos="d" xml:id="B05420-001-a-3610">this</w>
     <w lemma="our" pos="po" xml:id="B05420-001-a-3620">Our</w>
     <w lemma="ancient" pos="j" reg="ancient" xml:id="B05420-001-a-3630">antient</w>
     <w lemma="kingdom" pos="n1" xml:id="B05420-001-a-3640">Kingdom</w>
     <pc xml:id="B05420-001-a-3650">,</pc>
     <w lemma="from" pos="acp" xml:id="B05420-001-a-3660">from</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-3670">the</w>
     <w lemma="say" pos="vvd" xml:id="B05420-001-a-3680">said</w>
     <w lemma="seventeen" pos="ord" xml:id="B05420-001-a-3690">seventeenth</w>
     <w lemma="day" pos="n1" xml:id="B05420-001-a-3700">day</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-3710">of</w>
     <w lemma="August" pos="nn1" rend="hi" xml:id="B05420-001-a-3720">August</w>
     <pc xml:id="B05420-001-a-3730">,</pc>
     <w lemma="to" pos="acp" xml:id="B05420-001-a-3740">to</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-3750">the</w>
     <w lemma="say" pos="vvd" xml:id="B05420-001-a-3760">said</w>
     <w lemma="sixteenth" pos="ord" xml:id="B05420-001-a-3770">sixteenth</w>
     <w lemma="day" pos="n1" xml:id="B05420-001-a-3780">day</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-3790">of</w>
     <w lemma="November" pos="nn1" rend="hi" xml:id="B05420-001-a-3800">November</w>
     <pc xml:id="B05420-001-a-3810">,</pc>
     <w lemma="both" pos="d" xml:id="B05420-001-a-3820">both</w>
     <w lemma="next" pos="ord" xml:id="B05420-001-a-3830">next</w>
     <w lemma="ensue" pos="vvg" xml:id="B05420-001-a-3840">ensuing</w>
     <pc xml:id="B05420-001-a-3850">;</pc>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-3860">and</w>
     <w lemma="ordain" pos="vvz" xml:id="B05420-001-a-3870">ordains</w>
     <w lemma="these" pos="d" xml:id="B05420-001-a-3880">these</w>
     <w lemma="present" pos="n2" xml:id="B05420-001-a-3890">presents</w>
     <w lemma="to" pos="prt" xml:id="B05420-001-a-3900">to</w>
     <w lemma="be" pos="vvi" xml:id="B05420-001-a-3910">be</w>
     <w lemma="print" pos="vvn" xml:id="B05420-001-a-3920">Printed</w>
     <pc unit="sentence" xml:id="B05420-001-a-3930">.</pc>
    </p>
    <closer xml:id="B05420-e10320">
     <dateline xml:id="B05420-e10330">
      <w lemma="give" pos="vvn" xml:id="B05420-001-a-3940">Given</w>
      <w lemma="under" pos="acp" xml:id="B05420-001-a-3950">under</w>
      <w lemma="our" pos="po" xml:id="B05420-001-a-3960">Our</w>
      <w lemma="signet" pos="n1" xml:id="B05420-001-a-3970">Signet</w>
      <w lemma="at" pos="acp" xml:id="B05420-001-a-3980">at</w>
      <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05420-001-a-3990">Edinburgh</w>
      <date xml:id="B05420-e10350">
       <w lemma="the" pos="d" xml:id="B05420-001-a-4000">the</w>
       <w lemma="twenty" pos="crd" xml:id="B05420-001-a-4010">twenty</w>
       <w lemma="six" pos="ord" xml:id="B05420-001-a-4020">sixth</w>
       <w lemma="day" pos="n1" xml:id="B05420-001-a-4030">day</w>
       <w lemma="of" pos="acp" xml:id="B05420-001-a-4040">of</w>
       <w lemma="July" pos="nn1" rend="hi" xml:id="B05420-001-a-4050">July</w>
       <pc xml:id="B05420-001-a-4060">,</pc>
       <w lemma="and" pos="cc" xml:id="B05420-001-a-4070">and</w>
       <w lemma="of" pos="acp" xml:id="B05420-001-a-4080">of</w>
       <w lemma="our" pos="po" xml:id="B05420-001-a-4090">Our</w>
       <w lemma="reign" pos="n1" xml:id="B05420-001-a-4100">Reign</w>
       <w lemma="the" pos="d" xml:id="B05420-001-a-4110">the</w>
       <w lemma="four" pos="ord" xml:id="B05420-001-a-4120">fourth</w>
       <w lemma="year" pos="n1" xml:id="B05420-001-a-4130">year</w>
       <pc xml:id="B05420-001-a-4140">,</pc>
       <w lemma="1692." pos="crd" xml:id="B05420-001-a-4150">1692.</w>
       <pc unit="sentence" xml:id="B05420-001-a-4160"/>
      </date>
     </dateline>
     <signed xml:id="B05420-e10370">
      <w lemma="n/a" pos="fla" xml:id="B05420-001-a-4170">Per</w>
      <w lemma="n/a" pos="fla" xml:id="B05420-001-a-4180">actum</w>
      <w lemma="n/a" pos="fla" xml:id="B05420-001-a-4190">Dominorum</w>
      <w lemma="n/a" pos="fla" xml:id="B05420-001-a-4200">Secreti</w>
      <w lemma="n/a" pos="fla" xml:id="B05420-001-a-4210">Concilii</w>
      <pc unit="sentence" xml:id="B05420-001-a-4220">.</pc>
      <w lemma="in" pos="acp" xml:id="B05420-001-a-4230">In</w>
      <w lemma="n/a" pos="fla" xml:id="B05420-001-a-4240">Supplimentum</w>
      <w lemma="Signeti" pos="nn1" xml:id="B05420-001-a-4250">Signeti</w>
      <pc unit="sentence" xml:id="B05420-001-a-4260">.</pc>
      <hi xml:id="B05420-e10380">
       <w lemma="GILB" pos="nn1" xml:id="B05420-001-a-4270">GILB</w>
       <pc xml:id="B05420-001-a-4280">,</pc>
       <w lemma="eliot" pos="nn1" xml:id="B05420-001-a-4290">ELIOT</w>
      </hi>
      <pc rend="follows-hi" xml:id="B05420-001-a-4300">,</pc>
      <w lemma="Cls." pos="nn1" xml:id="B05420-001-a-4310">Cls.</w>
      <w lemma="sti." pos="ab" xml:id="B05420-001-a-4320">Sti.</w>
      <w lemma="n/a" pos="fla" xml:id="B05420-001-a-4330">Concilii</w>
      <pc unit="sentence" xml:id="B05420-001-a-4340">.</pc>
     </signed>
     <lb xml:id="B05420-e10390"/>
     <w lemma="GOD" pos="nn1" xml:id="B05420-001-a-4350">GOD</w>
     <w lemma="save" pos="acp" xml:id="B05420-001-a-4360">Save</w>
     <w lemma="king" pos="n1" xml:id="B05420-001-a-4370">King</w>
     <w lemma="William" pos="nn1" rend="hi" xml:id="B05420-001-a-4380">William</w>
     <w lemma="and" pos="cc" xml:id="B05420-001-a-4390">and</w>
     <w lemma="queen" pos="n1" xml:id="B05420-001-a-4400">Queen</w>
     <w lemma="Mary" pos="nn1" rend="hi" xml:id="B05420-001-a-4410">Mary</w>
     <pc unit="sentence" xml:id="B05420-001-a-4420">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="B05420-e10420">
   <div type="colophon" xml:id="B05420-e10430">
    <p xml:id="B05420-e10440">
     <w lemma="print" pos="j-vn" xml:id="B05420-001-a-4430">Printed</w>
     <w lemma="by" pos="acp" xml:id="B05420-001-a-4440">by</w>
     <w lemma="the" pos="d" xml:id="B05420-001-a-4450">the</w>
     <w lemma="heir" pos="n1" xml:id="B05420-001-a-4460">Heir</w>
     <w lemma="of" pos="acp" xml:id="B05420-001-a-4470">of</w>
     <hi xml:id="B05420-e10450">
      <w lemma="Andrew" pos="nn1" xml:id="B05420-001-a-4480">Andrew</w>
      <w lemma="Anderson" pos="nn1" xml:id="B05420-001-a-4490">Anderson</w>
     </hi>
     <w lemma="printer" pos="n1" xml:id="B05420-001-a-4500">Printer</w>
     <w lemma="to" pos="acp" xml:id="B05420-001-a-4510">to</w>
     <w lemma="their" pos="po" xml:id="B05420-001-a-4520">Their</w>
     <w lemma="most" pos="avs-d" xml:id="B05420-001-a-4530">most</w>
     <w lemma="excellent" pos="j" xml:id="B05420-001-a-4540">excellent</w>
     <w lemma="majesty" pos="n2" xml:id="B05420-001-a-4550">Majesties</w>
     <pc xml:id="B05420-001-a-4560">,</pc>
     <hi xml:id="B05420-e10460">
      <w lemma="n/a" pos="fla" xml:id="B05420-001-a-4570">Anno</w>
      <w lemma="dom." pos="ab" xml:id="B05420-001-a-4580">Dom.</w>
     </hi>
     <w lemma="1692." pos="crd" xml:id="B05420-001-a-4590">1692.</w>
     <pc unit="sentence" xml:id="B05420-001-a-4600"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
