<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B05361">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>At Edinburgh, the twelfth day of December, one thousand six hundred and sixty one</title>
    <author>Scotland. Privy Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B05361 of text R183053 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing S1493B). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B05361</idno>
    <idno type="STC">Wing S1493B</idno>
    <idno type="STC">ESTC R183053</idno>
    <idno type="EEBO-CITATION">52612255</idno>
    <idno type="OCLC">ocm 52612255</idno>
    <idno type="VID">179558</idno>
    <idno type="PROQUESTGOID">2240858987</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. B05361)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179558)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2793:30)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>At Edinburgh, the twelfth day of December, one thousand six hundred and sixty one</title>
      <author>Scotland. Privy Council.</author>
      <author>Wedderburn, Peter, Sir, 1616?-1679.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by Evan Tyler, Printer to the Kings most excellent Majesty,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>1661.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Royal arms at head of text; initial letter.</note>
      <note>Text in black letter.</note>
      <note>Signed at end: Pet. Wedderburne, Cl. Sti. Concilii.</note>
      <note>A proclamation prohibiting patrons from presenting and presbyteries from admitting ministers to charges in the Church of Scotland since bishops had had their former privileges restored.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Church of Scotland -- Clergy -- Appointment, call and election -- Early works to 1800.</term>
     <term>Church of Scotland -- Government -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>At Edinburgh, the twelfth day of December, one thousand six hundred and sixty one. Forasmuch as by ane act of Privy Council ...</ep:title>
    <ep:author>Scotland. Privy Council</ep:author>
    <ep:publicationYear>1661</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>428</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-10</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-12</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2009-03</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-03</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B05361-e10010">
  <body xml:id="B05361-e10020">
   <div type="text" xml:id="B05361-e10030">
    <pb facs="tcp:179558:1" rend="simple:additions" xml:id="B05361-001-a"/>
    <head xml:id="B05361-e10040">
     <figure xml:id="B05361-e10050">
      <p xml:id="B05361-e10060">
       <w lemma="c" pos="sy" xml:id="B05361-001-a-0010">C</w>
       <w lemma="r" pos="sy" xml:id="B05361-001-a-0020">R</w>
      </p>
      <p xml:id="B05361-e10070">
       <w lemma="n/a" pos="ffr" xml:id="B05361-001-a-0030">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="B05361-001-a-0040">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="B05361-001-a-0050">QVI</w>
       <w lemma="n/a" pos="ffr" xml:id="B05361-001-a-0060">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="B05361-001-a-0070">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="B05361-001-a-0080">PENSE</w>
      </p>
      <figDesc xml:id="B05361-e10080">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <opener xml:id="B05361-e10090">
     <dateline xml:id="B05361-e10100">
      <w lemma="at" pos="acp" xml:id="B05361-001-a-0090">AT</w>
      <w lemma="Edinburgh" pos="nn1" xml:id="B05361-001-a-0100">EDINBURGH</w>
      <pc xml:id="B05361-001-a-0110">,</pc>
      <date xml:id="B05361-e10110">
       <w lemma="the" pos="d" xml:id="B05361-001-a-0120">The</w>
       <w lemma="twelve" pos="ord" xml:id="B05361-001-a-0130">twelfth</w>
       <w lemma="day" pos="n1" xml:id="B05361-001-a-0140">day</w>
       <w lemma="of" pos="acp" xml:id="B05361-001-a-0150">of</w>
       <w lemma="December" pos="nn1" rend="hi" xml:id="B05361-001-a-0160">December</w>
       <pc xml:id="B05361-001-a-0170">,</pc>
       <w lemma="one" pos="crd" xml:id="B05361-001-a-0180">one</w>
       <w lemma="thousand" pos="crd" xml:id="B05361-001-a-0190">thousand</w>
       <w lemma="six" pos="crd" xml:id="B05361-001-a-0200">six</w>
       <w lemma="hundred" pos="crd" xml:id="B05361-001-a-0210">hundred</w>
       <w lemma="and" pos="cc" xml:id="B05361-001-a-0220">and</w>
       <w lemma="sixty" pos="crd" xml:id="B05361-001-a-0230">sixty</w>
       <w lemma="one" pos="crd" xml:id="B05361-001-a-0240">one</w>
       <pc unit="sentence" xml:id="B05361-001-a-0250">.</pc>
      </date>
     </dateline>
    </opener>
    <p xml:id="B05361-e10130">
     <w lemma="forasmuch" pos="av" rend="decorinit" xml:id="B05361-001-a-0260">FOrasmuch</w>
     <w lemma="as" pos="acp" xml:id="B05361-001-a-0270">as</w>
     <w lemma="by" pos="acp" xml:id="B05361-001-a-0280">by</w>
     <w lemma="a" pos="d" xml:id="B05361-001-a-0290">an</w>
     <w lemma="act" pos="n1" xml:id="B05361-001-a-0300">Act</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-0310">of</w>
     <w lemma="privy" pos="j" xml:id="B05361-001-a-0320">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05361-001-a-0330">Council</w>
     <pc xml:id="B05361-001-a-0340">,</pc>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-0350">of</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-0360">the</w>
     <w lemma="date" pos="n1" xml:id="B05361-001-a-0370">date</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-0380">the</w>
     <w lemma="six" pos="ord" xml:id="B05361-001-a-0390">sixth</w>
     <w lemma="day" pos="n1" xml:id="B05361-001-a-0400">day</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-0410">of</w>
     <w lemma="September" pos="nn1" rend="hi" xml:id="B05361-001-a-0420">September</w>
     <w lemma="last" pos="ord" xml:id="B05361-001-a-0430">last</w>
     <pc xml:id="B05361-001-a-0440">,</pc>
     <w lemma="his" pos="po" xml:id="B05361-001-a-0450">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" rend="hi" xml:id="B05361-001-a-0460">Majesties</w>
     <w lemma="royal" pos="j" xml:id="B05361-001-a-0470">Royal</w>
     <w lemma="pleasure" pos="n1" xml:id="B05361-001-a-0480">pleasure</w>
     <w lemma="to" pos="prt" xml:id="B05361-001-a-0490">to</w>
     <w lemma="restore" pos="vvi" xml:id="B05361-001-a-0500">restore</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-0510">the</w>
     <w lemma="church" pos="n1" xml:id="B05361-001-a-0520">Church</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-0530">of</w>
     <w lemma="this" pos="d" xml:id="B05361-001-a-0540">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05361-001-a-0550">Kingdom</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-0560">to</w>
     <w lemma="its" pos="po" xml:id="B05361-001-a-0570">its</w>
     <w lemma="right" pos="j" xml:id="B05361-001-a-0580">right</w>
     <w lemma="government" pos="n1" xml:id="B05361-001-a-0590">Government</w>
     <w lemma="by" pos="acp" xml:id="B05361-001-a-0600">by</w>
     <w lemma="bishop" pos="n2" xml:id="B05361-001-a-0610">Bishops</w>
     <pc xml:id="B05361-001-a-0620">,</pc>
     <w lemma="as" pos="acp" xml:id="B05361-001-a-0630">as</w>
     <w lemma="it" pos="pn" xml:id="B05361-001-a-0640">it</w>
     <w lemma="be" pos="vvd" xml:id="B05361-001-a-0650">was</w>
     <w lemma="by" pos="acp" xml:id="B05361-001-a-0660">by</w>
     <w lemma="law" pos="n1" xml:id="B05361-001-a-0670">Law</w>
     <w lemma="before" pos="acp" xml:id="B05361-001-a-0680">before</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-0690">the</w>
     <w lemma="late" pos="j" xml:id="B05361-001-a-0700">late</w>
     <w lemma="trouble" pos="n2" xml:id="B05361-001-a-0710">troubles</w>
     <pc xml:id="B05361-001-a-0720">,</pc>
     <w lemma="during" pos="acp" xml:id="B05361-001-a-0730">during</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-0740">the</w>
     <w lemma="reign" pos="n2" xml:id="B05361-001-a-0750">reigns</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-0760">of</w>
     <w lemma="his" pos="po" xml:id="B05361-001-a-0770">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" rend="hi" xml:id="B05361-001-a-0780">Majesties</w>
     <w lemma="royal" pos="j" xml:id="B05361-001-a-0790">Royal</w>
     <w lemma="father" pos="n1" xml:id="B05361-001-a-0800">Father</w>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-0810">and</w>
     <w lemma="grandfather" pos="n1" reg="Grandfather" xml:id="B05361-001-a-0820">Grand-father</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-0830">of</w>
     <w lemma="bless" pos="j-vn" xml:id="B05361-001-a-0840">blessed</w>
     <w lemma="memory" pos="n1" xml:id="B05361-001-a-0850">memory</w>
     <pc xml:id="B05361-001-a-0860">,</pc>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-0870">and</w>
     <w lemma="as" pos="acp" xml:id="B05361-001-a-0880">as</w>
     <w lemma="it" pos="pn" xml:id="B05361-001-a-0890">it</w>
     <w lemma="now" pos="av" xml:id="B05361-001-a-0900">now</w>
     <w lemma="stand" pos="vvz" xml:id="B05361-001-a-0910">stands</w>
     <w lemma="settle" pos="vvn" reg="settled" xml:id="B05361-001-a-0920">setled</w>
     <w lemma="by" pos="acp" xml:id="B05361-001-a-0930">by</w>
     <w lemma="law" pos="n1" xml:id="B05361-001-a-0940">Law</w>
     <pc xml:id="B05361-001-a-0950">,</pc>
     <w lemma="be" pos="vvd" xml:id="B05361-001-a-0960">was</w>
     <w lemma="make" pos="vvn" xml:id="B05361-001-a-0970">made</w>
     <w lemma="know" pos="vvn" xml:id="B05361-001-a-0980">known</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-0990">to</w>
     <w lemma="all" pos="d" xml:id="B05361-001-a-1000">all</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-1010">the</w>
     <w lemma="subject" pos="n2" xml:id="B05361-001-a-1020">Subjects</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-1030">of</w>
     <w lemma="this" pos="d" xml:id="B05361-001-a-1040">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05361-001-a-1050">Kingdom</w>
     <w lemma="by" pos="acp" xml:id="B05361-001-a-1060">by</w>
     <w lemma="open" pos="j" xml:id="B05361-001-a-1070">open</w>
     <w lemma="proclamation" pos="n1" xml:id="B05361-001-a-1080">Proclamation</w>
     <w lemma="at" pos="acp" xml:id="B05361-001-a-1090">at</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-1100">the</w>
     <w lemma="mercat" pos="n1" xml:id="B05361-001-a-1110">Mercat</w>
     <w lemma="cross" pos="vvz" xml:id="B05361-001-a-1120">Crosses</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-1130">of</w>
     <w lemma="all" pos="d" xml:id="B05361-001-a-1140">all</w>
     <w lemma="burrough" pos="n2" xml:id="B05361-001-a-1150">Burroughs</w>
     <w lemma="royal" pos="j" xml:id="B05361-001-a-1160">Royal</w>
     <pc xml:id="B05361-001-a-1170">:</pc>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-1180">And</w>
     <w lemma="that" pos="cs" xml:id="B05361-001-a-1190">that</w>
     <w lemma="it" pos="pn" xml:id="B05361-001-a-1200">it</w>
     <w lemma="be" pos="vvz" xml:id="B05361-001-a-1210">is</w>
     <w lemma="statute" pos="n1" xml:id="B05361-001-a-1220">Statute</w>
     <w lemma="by" pos="acp" xml:id="B05361-001-a-1230">by</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-1240">the</w>
     <w lemma="first" pos="ord" xml:id="B05361-001-a-1250">first</w>
     <w lemma="act" pos="n1" xml:id="B05361-001-a-1260">Act</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-1270">of</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-1280">the</w>
     <w lemma="twenty" pos="crd" xml:id="B05361-001-a-1290">twenty</w>
     <w lemma="one" pos="crd" xml:id="B05361-001-a-1300">one</w>
     <w lemma="parliament" pos="n1" xml:id="B05361-001-a-1310">Parliament</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-1320">of</w>
     <w lemma="king" pos="n1" xml:id="B05361-001-a-1330">King</w>
     <w lemma="James" pos="nn1" rend="hi" xml:id="B05361-001-a-1340">James</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-1350">the</w>
     <w lemma="six" pos="ord" xml:id="B05361-001-a-1360">sixth</w>
     <pc xml:id="B05361-001-a-1370">,</pc>
     <w lemma="that" pos="cs" xml:id="B05361-001-a-1380">that</w>
     <w lemma="all" pos="d" xml:id="B05361-001-a-1390">all</w>
     <w lemma="presentation" pos="n2" xml:id="B05361-001-a-1400">Presentations</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-1410">to</w>
     <w lemma="benefice" pos="n2" xml:id="B05361-001-a-1420">Benefices</w>
     <w lemma="shall" pos="vmd" xml:id="B05361-001-a-1430">should</w>
     <w lemma="be" pos="vvi" xml:id="B05361-001-a-1440">be</w>
     <w lemma="direct" pos="vvn" xml:id="B05361-001-a-1450">directed</w>
     <w lemma="thereafter" pos="av" xml:id="B05361-001-a-1460">thereafter</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-1470">to</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-1480">the</w>
     <w lemma="archbishop" pos="n1" xml:id="B05361-001-a-1490">Archbishop</w>
     <pc xml:id="B05361-001-a-1500">,</pc>
     <w lemma="or" pos="cc" xml:id="B05361-001-a-1510">or</w>
     <w lemma="bishop" pos="n1" xml:id="B05361-001-a-1520">Bishop</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-1530">of</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-1540">the</w>
     <w lemma="diocese" pos="n1" reg="Diocese" xml:id="B05361-001-a-1550">Diocess</w>
     <pc xml:id="B05361-001-a-1560">,</pc>
     <w lemma="within" pos="acp" xml:id="B05361-001-a-1570">within</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-1580">the</w>
     <w lemma="bound" pos="n2" xml:id="B05361-001-a-1590">bounds</w>
     <w lemma="whereof" pos="crq" xml:id="B05361-001-a-1600">whereof</w>
     <w lemma="any" pos="d" xml:id="B05361-001-a-1610">any</w>
     <w lemma="vacant" pos="j" xml:id="B05361-001-a-1620">vacant</w>
     <w lemma="church" pos="n1" xml:id="B05361-001-a-1630">Church</w>
     <w lemma="lie" pos="vvz" reg="lieth" xml:id="B05361-001-a-1640">lyeth</w>
     <pc unit="sentence" xml:id="B05361-001-a-1650">.</pc>
     <w lemma="so" pos="av" xml:id="B05361-001-a-1660">So</w>
     <w lemma="that" pos="cs" xml:id="B05361-001-a-1670">that</w>
     <w lemma="since" pos="acp" xml:id="B05361-001-a-1680">since</w>
     <w lemma="their" pos="po" xml:id="B05361-001-a-1690">their</w>
     <w lemma="restitution" pos="n1" xml:id="B05361-001-a-1700">restitution</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-1710">to</w>
     <w lemma="their" pos="po" xml:id="B05361-001-a-1720">their</w>
     <w lemma="former" pos="j" xml:id="B05361-001-a-1730">former</w>
     <w lemma="dignity" pos="n2" xml:id="B05361-001-a-1740">Dignities</w>
     <pc xml:id="B05361-001-a-1750">,</pc>
     <w lemma="privilege" pos="n2" reg="Privileges" xml:id="B05361-001-a-1760">Priviledges</w>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-1770">and</w>
     <w lemma="power" pos="n2" xml:id="B05361-001-a-1780">Powers</w>
     <w lemma="settle" pos="vvn" reg="settled" xml:id="B05361-001-a-1790">setled</w>
     <w lemma="upon" pos="acp" xml:id="B05361-001-a-1800">upon</w>
     <w lemma="they" pos="pno" xml:id="B05361-001-a-1810">them</w>
     <w lemma="by" pos="acp" xml:id="B05361-001-a-1820">by</w>
     <w lemma="law" pos="n1" xml:id="B05361-001-a-1830">Law</w>
     <pc xml:id="B05361-001-a-1840">,</pc>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-1850">and</w>
     <w lemma="act" pos="n2" xml:id="B05361-001-a-1860">Acts</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-1870">of</w>
     <w lemma="parliament" pos="n1" xml:id="B05361-001-a-1880">Parliament</w>
     <pc xml:id="B05361-001-a-1890">,</pc>
     <w lemma="no" pos="dx" xml:id="B05361-001-a-1900">No</w>
     <w lemma="minister" pos="n1" xml:id="B05361-001-a-1910">Minister</w>
     <w lemma="within" pos="acp" xml:id="B05361-001-a-1920">within</w>
     <w lemma="this" pos="d" xml:id="B05361-001-a-1930">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05361-001-a-1940">Kingdom</w>
     <w lemma="shall" pos="vmd" xml:id="B05361-001-a-1950">should</w>
     <w lemma="be" pos="vvi" xml:id="B05361-001-a-1960">be</w>
     <w lemma="admit" pos="vvn" xml:id="B05361-001-a-1970">admitted</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-1980">to</w>
     <w lemma="any" pos="d" xml:id="B05361-001-a-1990">any</w>
     <w lemma="benefice" pos="n1" xml:id="B05361-001-a-2000">Benefice</w>
     <w lemma="but" pos="acp" xml:id="B05361-001-a-2010">but</w>
     <w lemma="upon" pos="acp" xml:id="B05361-001-a-2020">upon</w>
     <w lemma="presentation" pos="n2" xml:id="B05361-001-a-2030">Presentations</w>
     <w lemma="direct" pos="vvn" xml:id="B05361-001-a-2040">directed</w>
     <w lemma="as" pos="acp" xml:id="B05361-001-a-2050">as</w>
     <w lemma="say" pos="vvn" xml:id="B05361-001-a-2060">said</w>
     <w lemma="be" pos="vvz" xml:id="B05361-001-a-2070">is</w>
     <pc unit="sentence" xml:id="B05361-001-a-2080">.</pc>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-2090">And</w>
     <w lemma="yet" pos="av" xml:id="B05361-001-a-2100">yet</w>
     <pc xml:id="B05361-001-a-2110">,</pc>
     <w lemma="notwithstanding" pos="acp" xml:id="B05361-001-a-2120">notwithstanding</w>
     <w lemma="thereof" pos="av" xml:id="B05361-001-a-2130">thereof</w>
     <w lemma="it" pos="pn" xml:id="B05361-001-a-2140">it</w>
     <w lemma="be" pos="vvz" xml:id="B05361-001-a-2150">is</w>
     <w lemma="inform" pos="vvn" xml:id="B05361-001-a-2160">informed</w>
     <pc xml:id="B05361-001-a-2170">,</pc>
     <w lemma="that" pos="cs" xml:id="B05361-001-a-2180">that</w>
     <w lemma="upon" pos="acp" xml:id="B05361-001-a-2190">upon</w>
     <w lemma="presentation" pos="n2" xml:id="B05361-001-a-2200">Presentations</w>
     <w lemma="direct" pos="vvn" xml:id="B05361-001-a-2210">directed</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-2220">to</w>
     <w lemma="presbytery" pos="n2" xml:id="B05361-001-a-2230">Presbyteries</w>
     <pc xml:id="B05361-001-a-2240">,</pc>
     <w lemma="they" pos="pns" xml:id="B05361-001-a-2250">they</w>
     <w lemma="do" pos="vvb" xml:id="B05361-001-a-2260">do</w>
     <w lemma="daily" pos="av-j" xml:id="B05361-001-a-2270">daily</w>
     <w lemma="proceed" pos="vvi" xml:id="B05361-001-a-2280">proceed</w>
     <w lemma="to" pos="prt" xml:id="B05361-001-a-2290">to</w>
     <w lemma="admit" pos="vvi" xml:id="B05361-001-a-2300">admit</w>
     <w lemma="minister" pos="n2" xml:id="B05361-001-a-2310">Ministers</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-2320">to</w>
     <w lemma="kirk" pos="n2" xml:id="B05361-001-a-2330">Kirks</w>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-2340">and</w>
     <w lemma="benefice" pos="n2" xml:id="B05361-001-a-2350">Benefices</w>
     <pc xml:id="B05361-001-a-2360">,</pc>
     <w lemma="albeit" pos="cs" xml:id="B05361-001-a-2370">albeit</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-2380">the</w>
     <w lemma="archbishop" pos="n2" xml:id="B05361-001-a-2390">Archbishops</w>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-2400">and</w>
     <w lemma="bishop" pos="n2" xml:id="B05361-001-a-2410">Bishops</w>
     <w lemma="be" pos="vvb" xml:id="B05361-001-a-2420">are</w>
     <w lemma="restore" pos="vvn" xml:id="B05361-001-a-2430">restored</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-2440">to</w>
     <w lemma="their" pos="po" xml:id="B05361-001-a-2450">their</w>
     <w lemma="dignity" pos="n2" xml:id="B05361-001-a-2460">Dignities</w>
     <pc xml:id="B05361-001-a-2470">;</pc>
     <w lemma="some" pos="d" xml:id="B05361-001-a-2480">some</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-2490">of</w>
     <w lemma="they" pos="pno" xml:id="B05361-001-a-2500">them</w>
     <w lemma="already" pos="av" xml:id="B05361-001-a-2510">already</w>
     <w lemma="consecrate" pos="vvn" xml:id="B05361-001-a-2520">consecrated</w>
     <pc xml:id="B05361-001-a-2530">,</pc>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-2540">and</w>
     <w lemma="all" pos="d" xml:id="B05361-001-a-2550">all</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-2560">of</w>
     <w lemma="they" pos="pno" xml:id="B05361-001-a-2570">them</w>
     <w lemma="within" pos="acp" xml:id="B05361-001-a-2580">within</w>
     <w lemma="a" pos="d" xml:id="B05361-001-a-2590">a</w>
     <w lemma="very" pos="j" xml:id="B05361-001-a-2600">very</w>
     <w lemma="short" pos="j" xml:id="B05361-001-a-2610">short</w>
     <w lemma="time" pos="n1" xml:id="B05361-001-a-2620">time</w>
     <w lemma="will" pos="vmb" xml:id="B05361-001-a-2630">will</w>
     <w lemma="be" pos="vvi" xml:id="B05361-001-a-2640">be</w>
     <w lemma="invest" pos="vvn" xml:id="B05361-001-a-2650">invested</w>
     <w lemma="in" pos="acp" xml:id="B05361-001-a-2660">in</w>
     <w lemma="their" pos="po" xml:id="B05361-001-a-2670">their</w>
     <w lemma="right" pos="n2-j" xml:id="B05361-001-a-2680">Rights</w>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-2690">and</w>
     <w lemma="benefice" pos="n2" xml:id="B05361-001-a-2700">Benefices</w>
     <pc xml:id="B05361-001-a-2710">,</pc>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-2720">and</w>
     <w lemma="empower" pos="vvn" xml:id="B05361-001-a-2730">impowered</w>
     <w lemma="to" pos="prt" xml:id="B05361-001-a-2740">to</w>
     <w lemma="receive" pos="vvi" xml:id="B05361-001-a-2750">receive</w>
     <w lemma="presentation" pos="n2" xml:id="B05361-001-a-2760">Presentations</w>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-2770">and</w>
     <w lemma="grant" pos="vvb" xml:id="B05361-001-a-2780">grant</w>
     <w lemma="admission" pos="n2" xml:id="B05361-001-a-2790">Admissions</w>
     <w lemma="thereupon" pos="av" xml:id="B05361-001-a-2800">thereupon</w>
     <pc xml:id="B05361-001-a-2810">:</pc>
     <w lemma="therefore" pos="av" xml:id="B05361-001-a-2820">Therefore</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-2830">the</w>
     <w lemma="lord" pos="n2" xml:id="B05361-001-a-2840">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-2850">of</w>
     <w lemma="his" pos="po" xml:id="B05361-001-a-2860">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" rend="hi" xml:id="B05361-001-a-2870">Majesties</w>
     <w lemma="privy" pos="j" xml:id="B05361-001-a-2880">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05361-001-a-2890">Council</w>
     <w lemma="prohibit" pos="vvz" xml:id="B05361-001-a-2900">prohibits</w>
     <pc xml:id="B05361-001-a-2910">,</pc>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-2920">and</w>
     <w lemma="by" pos="acp" xml:id="B05361-001-a-2930">by</w>
     <w lemma="these" pos="d" xml:id="B05361-001-a-2940">these</w>
     <w lemma="present" pos="n2" xml:id="B05361-001-a-2950">presents</w>
     <w lemma="discharge" pos="vvz" xml:id="B05361-001-a-2960">discharges</w>
     <w lemma="all" pos="d" xml:id="B05361-001-a-2970">all</w>
     <w lemma="patron" pos="n2" xml:id="B05361-001-a-2980">Patrons</w>
     <w lemma="to" pos="prt" xml:id="B05361-001-a-2990">to</w>
     <w lemma="direct" pos="vvi" xml:id="B05361-001-a-3000">direct</w>
     <w lemma="any" pos="d" xml:id="B05361-001-a-3010">any</w>
     <w lemma="presentation" pos="n2" xml:id="B05361-001-a-3020">Presentations</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-3030">to</w>
     <w lemma="any" pos="d" xml:id="B05361-001-a-3040">any</w>
     <w lemma="presbytery" pos="n2" xml:id="B05361-001-a-3050">Presbyteries</w>
     <pc xml:id="B05361-001-a-3060">;</pc>
     <w lemma="as" pos="acp" xml:id="B05361-001-a-3070">as</w>
     <w lemma="also" pos="av" xml:id="B05361-001-a-3080">also</w>
     <pc xml:id="B05361-001-a-3090">,</pc>
     <w lemma="discharge" pos="vvz" xml:id="B05361-001-a-3100">discharges</w>
     <w lemma="all" pos="d" xml:id="B05361-001-a-3110">all</w>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-3120">and</w>
     <w lemma="sundry" pos="j" xml:id="B05361-001-a-3130">sundry</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-3140">the</w>
     <w lemma="presbytery" pos="n2" xml:id="B05361-001-a-3150">Presbyteries</w>
     <w lemma="within" pos="acp" xml:id="B05361-001-a-3160">within</w>
     <w lemma="this" pos="d" xml:id="B05361-001-a-3170">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05361-001-a-3180">Kingdom</w>
     <w lemma="to" pos="prt" xml:id="B05361-001-a-3190">to</w>
     <w lemma="proceed" pos="vvi" xml:id="B05361-001-a-3200">proceed</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-3210">to</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-3220">the</w>
     <w lemma="admission" pos="n1" xml:id="B05361-001-a-3230">Admission</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-3240">of</w>
     <w lemma="any" pos="d" xml:id="B05361-001-a-3250">any</w>
     <w lemma="minister" pos="n1" xml:id="B05361-001-a-3260">Minister</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-3270">to</w>
     <w lemma="any" pos="d" xml:id="B05361-001-a-3280">any</w>
     <w lemma="benefice" pos="n1" xml:id="B05361-001-a-3290">Benefice</w>
     <w lemma="or" pos="cc" xml:id="B05361-001-a-3300">or</w>
     <w lemma="kirk" pos="n1" xml:id="B05361-001-a-3310">Kirk</w>
     <w lemma="within" pos="acp" xml:id="B05361-001-a-3320">within</w>
     <w lemma="their" pos="po" xml:id="B05361-001-a-3330">their</w>
     <w lemma="respective" pos="j" xml:id="B05361-001-a-3340">respective</w>
     <w lemma="bound" pos="n2" xml:id="B05361-001-a-3350">bounds</w>
     <pc xml:id="B05361-001-a-3360">,</pc>
     <w lemma="upon" pos="acp" xml:id="B05361-001-a-3370">upon</w>
     <w lemma="any" pos="d" xml:id="B05361-001-a-3380">any</w>
     <w lemma="such" pos="d" xml:id="B05361-001-a-3390">such</w>
     <w lemma="presentation" pos="n2" xml:id="B05361-001-a-3400">Presentations</w>
     <pc xml:id="B05361-001-a-3410">,</pc>
     <w lemma="as" pos="acp" xml:id="B05361-001-a-3420">as</w>
     <w lemma="they" pos="pns" xml:id="B05361-001-a-3430">they</w>
     <w lemma="will" pos="vmb" xml:id="B05361-001-a-3440">will</w>
     <w lemma="be" pos="vvi" xml:id="B05361-001-a-3450">be</w>
     <w lemma="answerable" pos="j" xml:id="B05361-001-a-3460">answerable</w>
     <pc xml:id="B05361-001-a-3470">;</pc>
     <w lemma="with" pos="acp" xml:id="B05361-001-a-3480">with</w>
     <w lemma="certification" pos="n1" xml:id="B05361-001-a-3490">certification</w>
     <w lemma="that" pos="cs" xml:id="B05361-001-a-3500">that</w>
     <w lemma="if" pos="cs" xml:id="B05361-001-a-3510">if</w>
     <w lemma="they" pos="pns" xml:id="B05361-001-a-3520">they</w>
     <w lemma="do" pos="vvb" xml:id="B05361-001-a-3530">do</w>
     <w lemma="other" pos="d" xml:id="B05361-001-a-3540">other</w>
     <w lemma="way" pos="n2" reg="ways" xml:id="B05361-001-a-3550">wayes</w>
     <pc xml:id="B05361-001-a-3560">,</pc>
     <w lemma="the" pos="d" xml:id="B05361-001-a-3570">the</w>
     <w lemma="say" pos="j-vn" xml:id="B05361-001-a-3580">saids</w>
     <w lemma="presentation" pos="n1" xml:id="B05361-001-a-3590">Presentation</w>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-3600">and</w>
     <w lemma="admission" pos="n1" xml:id="B05361-001-a-3610">Admission</w>
     <w lemma="shall" pos="vmb" xml:id="B05361-001-a-3620">shall</w>
     <w lemma="be" pos="vvi" xml:id="B05361-001-a-3630">be</w>
     <w lemma="void" pos="j" xml:id="B05361-001-a-3640">void</w>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-3650">and</w>
     <w lemma="null" pos="j" xml:id="B05361-001-a-3660">null</w>
     <pc xml:id="B05361-001-a-3670">,</pc>
     <w lemma="as" pos="acp" xml:id="B05361-001-a-3680">as</w>
     <w lemma="if" pos="cs" xml:id="B05361-001-a-3690">if</w>
     <w lemma="they" pos="pns" xml:id="B05361-001-a-3700">they</w>
     <w lemma="never" pos="avx" xml:id="B05361-001-a-3710">never</w>
     <w lemma="have" pos="vvd" xml:id="B05361-001-a-3720">had</w>
     <w lemma="be" pos="vvn" xml:id="B05361-001-a-3730">been</w>
     <w lemma="grant" pos="vvn" xml:id="B05361-001-a-3740">granted</w>
     <pc unit="sentence" xml:id="B05361-001-a-3750">.</pc>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-3760">And</w>
     <w lemma="ordain" pos="vvz" xml:id="B05361-001-a-3770">ordains</w>
     <w lemma="these" pos="d" xml:id="B05361-001-a-3780">these</w>
     <w lemma="present" pos="n2" xml:id="B05361-001-a-3790">presents</w>
     <w lemma="to" pos="prt" xml:id="B05361-001-a-3800">to</w>
     <w lemma="be" pos="vvi" xml:id="B05361-001-a-3810">be</w>
     <w lemma="print" pos="vvn" xml:id="B05361-001-a-3820">printed</w>
     <pc xml:id="B05361-001-a-3830">,</pc>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-3840">and</w>
     <w lemma="publish" pos="vvn" xml:id="B05361-001-a-3850">published</w>
     <w lemma="at" pos="acp" xml:id="B05361-001-a-3860">at</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-3870">the</w>
     <w lemma="mercat" pos="n1" xml:id="B05361-001-a-3880">Mercat</w>
     <w lemma="cross" pos="n1" xml:id="B05361-001-a-3890">Cross</w>
     <w lemma="of" pos="acp" xml:id="B05361-001-a-3900">of</w>
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05361-001-a-3910">Edinburgh</w>
     <w lemma="and" pos="cc" xml:id="B05361-001-a-3920">and</w>
     <w lemma="other" pos="d" xml:id="B05361-001-a-3930">other</w>
     <w lemma="place" pos="n2" xml:id="B05361-001-a-3940">places</w>
     <w lemma="needful" pos="j" reg="needful" xml:id="B05361-001-a-3950">needfull</w>
     <pc xml:id="B05361-001-a-3960">,</pc>
     <w lemma="that" pos="cs" xml:id="B05361-001-a-3970">that</w>
     <w lemma="none" pos="pix" xml:id="B05361-001-a-3980">none</w>
     <w lemma="may" pos="vmb" xml:id="B05361-001-a-3990">may</w>
     <w lemma="pretend" pos="vvi" xml:id="B05361-001-a-4000">pretend</w>
     <w lemma="ignorance" pos="n1" xml:id="B05361-001-a-4010">ignorance</w>
     <pc unit="sentence" xml:id="B05361-001-a-4020">.</pc>
    </p>
    <closer xml:id="B05361-e10200">
     <signed xml:id="B05361-e10210">
      <hi xml:id="B05361-e10220">
       <w lemma="pet." pos="ab" xml:id="B05361-001-a-4030">Pet.</w>
       <w lemma="Wedderburne" pos="nn1" xml:id="B05361-001-a-4040">Wedderburne</w>
       <pc xml:id="B05361-001-a-4050">,</pc>
       <hi xml:id="B05361-e10230">
        <w lemma="150" pos="crd" xml:id="B05361-001-a-4060">Cl.</w>
        <w lemma="Sti." orig="Sᵗⁱ." pos="ab" xml:id="B05361-001-a-4070">Sti.</w>
        <w lemma="n/a" pos="fla" xml:id="B05361-001-a-4100">Concilii</w>
       </hi>
      </hi>
      <pc rend="follows-hi" unit="sentence" xml:id="B05361-001-a-4110">.</pc>
      <pc rend="follows-hi" unit="sentence" xml:id="B05361-001-a-4090">.</pc>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="B05361-e10250">
   <div type="colophon" xml:id="B05361-e10260">
    <p xml:id="B05361-e10270">
     <w lemma="Edinburgh" pos="nn1" reg="EDINBURGH" rend="hi" xml:id="B05361-001-a-4120">EDINBVRGH</w>
     <pc xml:id="B05361-001-a-4130">,</pc>
     <w lemma="print" pos="vvn" xml:id="B05361-001-a-4140">Printed</w>
     <w lemma="by" pos="acp" xml:id="B05361-001-a-4150">by</w>
     <hi xml:id="B05361-e10290">
      <w lemma="Evan" pos="nn1" xml:id="B05361-001-a-4160">Evan</w>
      <w lemma="Tyler" pos="nn1" xml:id="B05361-001-a-4170">Tyler</w>
     </hi>
     <pc rend="follows-hi" xml:id="B05361-001-a-4180">,</pc>
     <w lemma="printer" pos="n1" xml:id="B05361-001-a-4190">Printer</w>
     <w lemma="to" pos="acp" xml:id="B05361-001-a-4200">to</w>
     <w lemma="the" pos="d" xml:id="B05361-001-a-4210">the</w>
     <w lemma="king" pos="n2" xml:id="B05361-001-a-4220">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="B05361-001-a-4230">most</w>
     <w lemma="excellent" pos="j" xml:id="B05361-001-a-4240">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="B05361-001-a-4250">Majesty</w>
     <pc xml:id="B05361-001-a-4260">,</pc>
     <w lemma="1661." pos="crd" xml:id="B05361-001-a-4270">1661.</w>
     <pc unit="sentence" xml:id="B05361-001-a-4280"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
