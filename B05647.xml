<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B05647">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A proclamation, for calling of the Parliament. Edinburgh, the fifteenth day of July, one thousand six hundred and sixty nine.</title>
    <author>Scotland. Privy Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B05647 of text R183506 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing S1846A). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B05647</idno>
    <idno type="STC">Wing S1846A</idno>
    <idno type="STC">ESTC R183506</idno>
    <idno type="EEBO-CITATION">52612327</idno>
    <idno type="OCLC">ocm 52612327</idno>
    <idno type="VID">179631</idno>
    <idno type="PROQUESTGOID">2240865721</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B05647)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179631)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2794:25)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A proclamation, for calling of the Parliament. Edinburgh, the fifteenth day of July, one thousand six hundred and sixty nine.</title>
      <author>Scotland. Privy Council.</author>
      <author>Scotland. Sovereign (1649-1685 : Charles II)</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by Evan Tyler, Printer to the King's most excellent Majesty,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>1669.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Royal arms at head of text; initial letter.</note>
      <note>Text in black letter.</note>
      <note>Signed: Tho. Hay, Cls. Sti. Concilii.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Scotland. -- Parliament -- Early works to 1800.</term>
     <term>Scotland -- Politics and government -- 1660-1688 -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A proclamation, for calling of the Parliament. Edinburgh, the fifteenth day of July, one thousand six hundred and sixty nine.</ep:title>
    <ep:author>Scotland. Privy Council</ep:author>
    <ep:publicationYear>1669</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>438</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-01</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-01</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-03</date>
    <label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-03</date>
    <label>Elspeth Healey</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B05647-t">
  <body xml:id="B05647-e0">
   <div type="proclamation" xml:id="B05647-e10">
    <pb facs="tcp:179631:1" rend="simple:additions" xml:id="B05647-001-a"/>
    <head xml:id="B05647-e20">
     <figure xml:id="B05647-e30">
      <p xml:id="B05647-e40">
       <w lemma="n/a" pos="ffr" xml:id="B05647-001-a-0010">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="B05647-001-a-0020">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="B05647-001-a-0030">QUI</w>
       <w lemma="n/a" pos="ffr" xml:id="B05647-001-a-0040">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="B05647-001-a-0050">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="B05647-001-a-0060">PENSE</w>
      </p>
     </figure>
     <lb xml:id="B05647-e50"/>
     <w lemma="a" pos="d" xml:id="B05647-001-a-0070">A</w>
     <w lemma="proclamation" pos="n1" xml:id="B05647-001-a-0080">PROCLAMATION</w>
     <pc xml:id="B05647-001-a-0090">,</pc>
     <w lemma="for" pos="acp" xml:id="B05647-001-a-0100">For</w>
     <w lemma="call" pos="vvg" xml:id="B05647-001-a-0110">Calling</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-0120">of</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-0130">the</w>
     <w lemma="parliament" pos="n1" xml:id="B05647-001-a-0140">PARLIAMENT</w>
     <pc unit="sentence" xml:id="B05647-001-a-0150">.</pc>
     <w lemma="Edinburgh" pos="nn1" xml:id="B05647-001-a-0160">Edinburgh</w>
     <pc xml:id="B05647-001-a-0170">,</pc>
     <hi xml:id="B05647-e60">
      <w lemma="the" pos="d" xml:id="B05647-001-a-0180">the</w>
      <w lemma="fifteen" pos="ord" xml:id="B05647-001-a-0190">fifteenth</w>
      <w lemma="day" pos="n1" xml:id="B05647-001-a-0200">day</w>
      <w lemma="of" pos="acp" xml:id="B05647-001-a-0210">of</w>
     </hi>
     <w lemma="July" pos="nn1" xml:id="B05647-001-a-0220">July</w>
     <pc xml:id="B05647-001-a-0230">,</pc>
     <hi xml:id="B05647-e70">
      <w lemma="one" pos="crd" xml:id="B05647-001-a-0240">one</w>
      <w lemma="thousand" pos="crd" xml:id="B05647-001-a-0250">thousand</w>
      <w lemma="six" pos="crd" xml:id="B05647-001-a-0260">six</w>
      <w lemma="hundred" pos="crd" xml:id="B05647-001-a-0270">hundred</w>
      <w lemma="and" pos="cc" xml:id="B05647-001-a-0280">and</w>
      <w lemma="sixty" pos="crd" xml:id="B05647-001-a-0290">sixty</w>
      <w lemma="nine" pos="crd" xml:id="B05647-001-a-0300">nine</w>
      <pc unit="sentence" xml:id="B05647-001-a-0310">.</pc>
     </hi>
    </head>
    <p xml:id="B05647-e80">
     <w lemma="charles" pos="nng1" reg="CHARLES'" rend="hi" xml:id="B05647-001-a-0320">CHARLES</w>
     <pc xml:id="B05647-001-a-0330">,</pc>
     <w lemma="by" pos="acp" xml:id="B05647-001-a-0340">by</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-0350">the</w>
     <w lemma="grace" pos="n1" xml:id="B05647-001-a-0360">Grace</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-0370">of</w>
     <w lemma="GOD" pos="nn1" xml:id="B05647-001-a-0380">GOD</w>
     <pc xml:id="B05647-001-a-0390">,</pc>
     <w lemma="king" pos="n1" xml:id="B05647-001-a-0400">King</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-0410">of</w>
     <hi xml:id="B05647-e100">
      <w lemma="Scotland" pos="nn1" xml:id="B05647-001-a-0420">Scotland</w>
      <pc xml:id="B05647-001-a-0430">,</pc>
      <w lemma="England" pos="nn1" xml:id="B05647-001-a-0440">England</w>
      <pc xml:id="B05647-001-a-0450">,</pc>
      <w lemma="France" pos="nn1" xml:id="B05647-001-a-0460">France</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-0470">and</w>
     <w lemma="Ireland" pos="nn1" rend="hi" xml:id="B05647-001-a-0480">Ireland</w>
     <pc xml:id="B05647-001-a-0490">,</pc>
     <w lemma="defender" pos="n1" xml:id="B05647-001-a-0500">Defender</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-0510">of</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-0520">the</w>
     <w lemma="faith" pos="n1" xml:id="B05647-001-a-0530">Faith</w>
     <pc xml:id="B05647-001-a-0540">;</pc>
     <w lemma="to" pos="acp" xml:id="B05647-001-a-0550">To</w>
     <w lemma="all" pos="d" xml:id="B05647-001-a-0560">all</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-0570">and</w>
     <w lemma="sundry" pos="j" xml:id="B05647-001-a-0580">sundry</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-0590">Our</w>
     <w lemma="good" pos="j" xml:id="B05647-001-a-0600">good</w>
     <w lemma="subject" pos="n2" xml:id="B05647-001-a-0610">Subjects</w>
     <pc xml:id="B05647-001-a-0620">,</pc>
     <w lemma="who" pos="crq" xml:id="B05647-001-a-0630">whom</w>
     <w lemma="these" pos="d" xml:id="B05647-001-a-0640">these</w>
     <w lemma="present" pos="n2" xml:id="B05647-001-a-0650">presents</w>
     <w lemma="do" pos="vvb" xml:id="B05647-001-a-0660">do</w>
     <w lemma="or" pos="cc" xml:id="B05647-001-a-0670">or</w>
     <w lemma="may" pos="vmb" xml:id="B05647-001-a-0680">may</w>
     <w lemma="concern" pos="vvi" xml:id="B05647-001-a-0690">concern</w>
     <pc xml:id="B05647-001-a-0700">,</pc>
     <w lemma="greeting" pos="n1" xml:id="B05647-001-a-0710">Greeting</w>
     <pc unit="sentence" xml:id="B05647-001-a-0720">.</pc>
     <w lemma="forasmuch" pos="av" xml:id="B05647-001-a-0730">Forasmuch</w>
     <w lemma="as" pos="acp" xml:id="B05647-001-a-0740">as</w>
     <pc xml:id="B05647-001-a-0750">,</pc>
     <w lemma="upon" pos="acp" xml:id="B05647-001-a-0760">upon</w>
     <w lemma="divers" pos="j" xml:id="B05647-001-a-0770">divers</w>
     <w lemma="great" pos="j" xml:id="B05647-001-a-0780">great</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-0790">and</w>
     <w lemma="weighty" pos="j" xml:id="B05647-001-a-0800">weighty</w>
     <w lemma="consideration" pos="n2" xml:id="B05647-001-a-0810">considerations</w>
     <pc xml:id="B05647-001-a-0820">,</pc>
     <w lemma="relate" pos="vvg" xml:id="B05647-001-a-0830">relating</w>
     <w lemma="to" pos="acp" xml:id="B05647-001-a-0840">to</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-0850">the</w>
     <w lemma="establishment" pos="n1" xml:id="B05647-001-a-0860">establishment</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-0870">of</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-0880">the</w>
     <w lemma="quiet" pos="j" xml:id="B05647-001-a-0890">quiet</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-0900">and</w>
     <w lemma="happiness" pos="n1" xml:id="B05647-001-a-0910">happiness</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-0920">of</w>
     <w lemma="this" pos="d" xml:id="B05647-001-a-0930">this</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-0940">Our</w>
     <w lemma="ancient" pos="j" xml:id="B05647-001-a-0950">ancient</w>
     <w lemma="kingdom" pos="n1" xml:id="B05647-001-a-0960">Kingdom</w>
     <w lemma="in" pos="acp" xml:id="B05647-001-a-0970">in</w>
     <w lemma="all" pos="d" xml:id="B05647-001-a-0980">all</w>
     <w lemma="its" pos="po" xml:id="B05647-001-a-0990">its</w>
     <w lemma="interest" pos="n2" xml:id="B05647-001-a-1000">Interests</w>
     <pc xml:id="B05647-001-a-1010">,</pc>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-1020">and</w>
     <w lemma="for" pos="acp" xml:id="B05647-001-a-1030">for</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-1040">the</w>
     <w lemma="good" pos="j" xml:id="B05647-001-a-1050">good</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-1060">of</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-1070">Our</w>
     <w lemma="service" pos="n1" xml:id="B05647-001-a-1080">Service</w>
     <pc xml:id="B05647-001-a-1090">;</pc>
     <w lemma="we" pos="pns" xml:id="B05647-001-a-1100">We</w>
     <w lemma="do" pos="vvb" xml:id="B05647-001-a-1110">do</w>
     <w lemma="think" pos="vvi" xml:id="B05647-001-a-1120">think</w>
     <w lemma="it" pos="pn" xml:id="B05647-001-a-1130">it</w>
     <w lemma="necessary" pos="j" xml:id="B05647-001-a-1140">necessary</w>
     <w lemma="to" pos="prt" xml:id="B05647-001-a-1150">to</w>
     <w lemma="call" pos="vvi" xml:id="B05647-001-a-1160">call</w>
     <w lemma="a" pos="d" xml:id="B05647-001-a-1170">a</w>
     <w lemma="parliament" pos="n1" xml:id="B05647-001-a-1180">Parliament</w>
     <pc xml:id="B05647-001-a-1190">,</pc>
     <w lemma="to" pos="prt" xml:id="B05647-001-a-1200">to</w>
     <w lemma="be" pos="vvi" xml:id="B05647-001-a-1210">be</w>
     <w lemma="hold" pos="vvn" xml:id="B05647-001-a-1220">held</w>
     <w lemma="at" pos="acp" xml:id="B05647-001-a-1230">at</w>
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05647-001-a-1240">Edinburgh</w>
     <pc xml:id="B05647-001-a-1250">,</pc>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-1260">and</w>
     <w lemma="to" pos="prt" xml:id="B05647-001-a-1270">to</w>
     <w lemma="begin" pos="vvi" xml:id="B05647-001-a-1280">begin</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-1290">the</w>
     <w lemma="nineteen" pos="ord" xml:id="B05647-001-a-1300">nineteenth</w>
     <w lemma="day" pos="n1" xml:id="B05647-001-a-1310">day</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-1320">of</w>
     <w lemma="October" pos="nn1" rend="hi" xml:id="B05647-001-a-1330">October</w>
     <w lemma="next" pos="ord" xml:id="B05647-001-a-1340">next</w>
     <pc xml:id="B05647-001-a-1350">,</pc>
     <w lemma="at" pos="acp" xml:id="B05647-001-a-1360">at</w>
     <w lemma="which" pos="crq" xml:id="B05647-001-a-1370">which</w>
     <w lemma="time" pos="n1" xml:id="B05647-001-a-1380">time</w>
     <pc xml:id="B05647-001-a-1390">,</pc>
     <w lemma="our" pos="po" xml:id="B05647-001-a-1400">Our</w>
     <w lemma="commissioner" pos="n1" xml:id="B05647-001-a-1410">Commissioner</w>
     <pc xml:id="B05647-001-a-1420">,</pc>
     <w lemma="sufficient" pos="av-j" xml:id="B05647-001-a-1430">sufficiently</w>
     <w lemma="authorize" pos="vvn" xml:id="B05647-001-a-1440">authorized</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-1450">and</w>
     <w lemma="instruct" pos="vvn" xml:id="B05647-001-a-1460">instructed</w>
     <w lemma="by" pos="acp" xml:id="B05647-001-a-1470">by</w>
     <w lemma="we" pos="pno" xml:id="B05647-001-a-1480">Us</w>
     <pc xml:id="B05647-001-a-1490">,</pc>
     <w lemma="shall" pos="vmb" xml:id="B05647-001-a-1500">shall</w>
     <w lemma="be" pos="vvi" xml:id="B05647-001-a-1510">be</w>
     <w lemma="present" pos="j" xml:id="B05647-001-a-1520">present</w>
     <pc unit="sentence" xml:id="B05647-001-a-1530">.</pc>
     <w lemma="therefore" pos="av" xml:id="B05647-001-a-1540">Therefore</w>
     <pc xml:id="B05647-001-a-1550">,</pc>
     <w lemma="we" pos="pns" xml:id="B05647-001-a-1560">We</w>
     <pc xml:id="B05647-001-a-1570">,</pc>
     <w lemma="with" pos="acp" xml:id="B05647-001-a-1580">with</w>
     <w lemma="advice" pos="n1" xml:id="B05647-001-a-1590">advice</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-1600">of</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-1610">the</w>
     <w lemma="lord" pos="n2" xml:id="B05647-001-a-1620">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-1630">of</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-1640">Our</w>
     <w lemma="privy" pos="j" xml:id="B05647-001-a-1650">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05647-001-a-1660">Council</w>
     <pc xml:id="B05647-001-a-1670">,</pc>
     <w lemma="do" pos="vvb" xml:id="B05647-001-a-1680">do</w>
     <w lemma="hereby" pos="av" xml:id="B05647-001-a-1690">hereby</w>
     <w lemma="require" pos="vvi" xml:id="B05647-001-a-1700">require</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-1710">and</w>
     <w lemma="command" pos="vvi" xml:id="B05647-001-a-1720">command</w>
     <pc xml:id="B05647-001-a-1730">,</pc>
     <w lemma="all" pos="d" xml:id="B05647-001-a-1740">all</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-1750">the</w>
     <w lemma="lord" pos="n2" xml:id="B05647-001-a-1760">Lords</w>
     <w lemma="spiritual" pos="j" xml:id="B05647-001-a-1770">Spiritual</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-1780">and</w>
     <w lemma="temporal" pos="j" xml:id="B05647-001-a-1790">Temporal</w>
     <pc xml:id="B05647-001-a-1800">,</pc>
     <w lemma="the" pos="d" xml:id="B05647-001-a-1810">the</w>
     <w lemma="archbishop" pos="n2" xml:id="B05647-001-a-1820">Archbishops</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-1830">and</w>
     <w lemma="bishop" pos="n2" xml:id="B05647-001-a-1840">Bishops</w>
     <pc xml:id="B05647-001-a-1850">,</pc>
     <w lemma="the" pos="d" xml:id="B05647-001-a-1860">the</w>
     <w lemma="duke" pos="n2" xml:id="B05647-001-a-1870">Dukes</w>
     <pc xml:id="B05647-001-a-1880">,</pc>
     <w lemma="marquess" pos="n2" xml:id="B05647-001-a-1890">Marquesses</w>
     <pc xml:id="B05647-001-a-1900">,</pc>
     <w lemma="earl" pos="n2" xml:id="B05647-001-a-1910">Earls</w>
     <pc xml:id="B05647-001-a-1920">,</pc>
     <w lemma="discount" pos="n2" xml:id="B05647-001-a-1930">Discounts</w>
     <pc xml:id="B05647-001-a-1940">,</pc>
     <w lemma="lord" pos="n2" xml:id="B05647-001-a-1950">Lords</w>
     <pc xml:id="B05647-001-a-1960">,</pc>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-1970">and</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-1980">Our</w>
     <w lemma="officer" pos="n2" xml:id="B05647-001-a-1990">Officers</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-2000">of</w>
     <w lemma="estate" pos="n1" xml:id="B05647-001-a-2010">Estate</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-2020">of</w>
     <w lemma="this" pos="d" xml:id="B05647-001-a-2030">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05647-001-a-2040">Kingdom</w>
     <pc xml:id="B05647-001-a-2050">,</pc>
     <w lemma="to" pos="prt" xml:id="B05647-001-a-2060">to</w>
     <w lemma="be" pos="vvi" xml:id="B05647-001-a-2070">be</w>
     <w lemma="present" pos="j" xml:id="B05647-001-a-2080">present</w>
     <w lemma="at" pos="acp" xml:id="B05647-001-a-2090">at</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-2100">Our</w>
     <w lemma="parliament" pos="n1" xml:id="B05647-001-a-2110">Parliament</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-2120">the</w>
     <w lemma="say" pos="j-vn" xml:id="B05647-001-a-2130">said</w>
     <w lemma="day" pos="n1" xml:id="B05647-001-a-2140">day</w>
     <pc xml:id="B05647-001-a-2150">:</pc>
     <w lemma="as" pos="acp" xml:id="B05647-001-a-2160">As</w>
     <w lemma="also" pos="av" xml:id="B05647-001-a-2170">also</w>
     <pc xml:id="B05647-001-a-2180">,</pc>
     <w lemma="we" pos="pns" xml:id="B05647-001-a-2190">We</w>
     <w lemma="do" pos="vvb" xml:id="B05647-001-a-2200">do</w>
     <w lemma="require" pos="vvi" xml:id="B05647-001-a-2210">require</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-2220">and</w>
     <w lemma="command" pos="vvi" xml:id="B05647-001-a-2230">command</w>
     <w lemma="all" pos="d" xml:id="B05647-001-a-2240">all</w>
     <w lemma="those" pos="d" xml:id="B05647-001-a-2250">those</w>
     <w lemma="who" pos="crq" xml:id="B05647-001-a-2260">who</w>
     <w lemma="have" pos="vvb" xml:id="B05647-001-a-2270">have</w>
     <w lemma="right" pos="j" xml:id="B05647-001-a-2280">right</w>
     <w lemma="to" pos="prt" xml:id="B05647-001-a-2290">to</w>
     <w lemma="choose" pos="vvi" xml:id="B05647-001-a-2300">choose</w>
     <w lemma="commissioner" pos="n2" xml:id="B05647-001-a-2310">Commissioners</w>
     <w lemma="for" pos="acp" xml:id="B05647-001-a-2320">for</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-2330">the</w>
     <w lemma="several" pos="j" xml:id="B05647-001-a-2340">several</w>
     <w lemma="shire" pos="n2" xml:id="B05647-001-a-2350">Shires</w>
     <pc xml:id="B05647-001-a-2360">,</pc>
     <w lemma="to" pos="prt" xml:id="B05647-001-a-2370">to</w>
     <w lemma="meet" pos="vvi" xml:id="B05647-001-a-2380">meet</w>
     <w lemma="within" pos="acp" xml:id="B05647-001-a-2390">within</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-2400">the</w>
     <w lemma="respective" pos="j" xml:id="B05647-001-a-2410">respective</w>
     <w lemma="shire" pos="n2" xml:id="B05647-001-a-2420">Shires</w>
     <w lemma="at" pos="acp" xml:id="B05647-001-a-2430">at</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-2440">the</w>
     <w lemma="Michaelmas" pos="nn1" rend="hi" xml:id="B05647-001-a-2450">Michaelmas</w>
     <w lemma="head" pos="n1" xml:id="B05647-001-a-2460">head</w>
     <w lemma="court" pos="n1" xml:id="B05647-001-a-2470">Court</w>
     <w lemma="next" pos="ord" xml:id="B05647-001-a-2480">next</w>
     <w lemma="ensue" pos="vvg" xml:id="B05647-001-a-2490">ensuing</w>
     <pc xml:id="B05647-001-a-2500">,</pc>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-2510">and</w>
     <w lemma="make" pos="vvi" xml:id="B05647-001-a-2520">make</w>
     <w lemma="their" pos="po" xml:id="B05647-001-a-2530">their</w>
     <w lemma="election" pos="n2" xml:id="B05647-001-a-2540">Elections</w>
     <w lemma="according" pos="j" xml:id="B05647-001-a-2550">according</w>
     <w lemma="to" pos="acp" xml:id="B05647-001-a-2560">to</w>
     <w lemma="law" pos="n1" xml:id="B05647-001-a-2570">Law</w>
     <pc xml:id="B05647-001-a-2580">;</pc>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-2590">And</w>
     <w lemma="sicklike" pos="j" xml:id="B05647-001-a-2600">sicklike</w>
     <pc xml:id="B05647-001-a-2610">,</pc>
     <w lemma="we" pos="pns" xml:id="B05647-001-a-2620">We</w>
     <w lemma="require" pos="vvb" xml:id="B05647-001-a-2630">require</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-2640">and</w>
     <w lemma="command" pos="vvi" xml:id="B05647-001-a-2650">command</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-2660">Our</w>
     <w lemma="royal" pos="j" xml:id="B05647-001-a-2670">Royal</w>
     <w lemma="burrough" pos="n2" xml:id="B05647-001-a-2680">Burroughs</w>
     <w lemma="to" pos="prt" xml:id="B05647-001-a-2690">to</w>
     <w lemma="meet" pos="vvi" xml:id="B05647-001-a-2700">meet</w>
     <w lemma="in" pos="acp" xml:id="B05647-001-a-2710">in</w>
     <w lemma="due" pos="j" xml:id="B05647-001-a-2720">due</w>
     <w lemma="time" pos="n1" xml:id="B05647-001-a-2730">time</w>
     <w lemma="for" pos="acp" xml:id="B05647-001-a-2740">for</w>
     <w lemma="choose" pos="vvg" xml:id="B05647-001-a-2750">choosing</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-2760">of</w>
     <w lemma="their" pos="po" xml:id="B05647-001-a-2770">their</w>
     <w lemma="commissioner" pos="n2" xml:id="B05647-001-a-2780">Commissioners</w>
     <pc xml:id="B05647-001-a-2790">;</pc>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-2800">And</w>
     <w lemma="that" pos="cs" xml:id="B05647-001-a-2810">that</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-2820">the</w>
     <w lemma="lord" pos="n2" xml:id="B05647-001-a-2830">Lords</w>
     <w lemma="spiritual" pos="j" xml:id="B05647-001-a-2840">Spiritual</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-2850">and</w>
     <w lemma="temporal" pos="j" xml:id="B05647-001-a-2860">Temporal</w>
     <pc xml:id="B05647-001-a-2870">,</pc>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-2880">and</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-2890">Our</w>
     <w lemma="officer" pos="n2" xml:id="B05647-001-a-2900">Officers</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-2910">of</w>
     <w lemma="estate" pos="n1" xml:id="B05647-001-a-2920">Estate</w>
     <w lemma="aforementioned" pos="j" xml:id="B05647-001-a-2930">aforementioned</w>
     <pc xml:id="B05647-001-a-2940">,</pc>
     <w lemma="commissioner" pos="n2" xml:id="B05647-001-a-2950">Commissioners</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-2960">of</w>
     <w lemma="shire" pos="n2" xml:id="B05647-001-a-2970">Shires</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-2980">and</w>
     <w lemma="burrough" pos="n2" xml:id="B05647-001-a-2990">Burroughs</w>
     <pc xml:id="B05647-001-a-3000">,</pc>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-3010">and</w>
     <w lemma="all" pos="d" xml:id="B05647-001-a-3020">all</w>
     <w lemma="other" pos="d" xml:id="B05647-001-a-3030">other</w>
     <w lemma="person" pos="n2" xml:id="B05647-001-a-3040">persons</w>
     <w lemma="concern" pos="vvn" xml:id="B05647-001-a-3050">concerned</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-3060">and</w>
     <w lemma="have" pos="vvg" xml:id="B05647-001-a-3070">having</w>
     <w lemma="interest" pos="n1" xml:id="B05647-001-a-3080">interest</w>
     <pc xml:id="B05647-001-a-3090">,</pc>
     <w lemma="be" pos="vvb" xml:id="B05647-001-a-3100">be</w>
     <w lemma="present" pos="j" xml:id="B05647-001-a-3110">present</w>
     <w lemma="at" pos="acp" xml:id="B05647-001-a-3120">at</w>
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05647-001-a-3130">Edinburgh</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-3140">the</w>
     <w lemma="foresay" pos="j-vn" xml:id="B05647-001-a-3150">foresaid</w>
     <w lemma="nineteen" pos="ord" xml:id="B05647-001-a-3160">nineteenth</w>
     <w lemma="day" pos="n1" xml:id="B05647-001-a-3170">day</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-3180">of</w>
     <w lemma="October" pos="nn1" rend="hi" xml:id="B05647-001-a-3190">October</w>
     <pc xml:id="B05647-001-a-3200">,</pc>
     <w lemma="to" pos="prt" xml:id="B05647-001-a-3210">to</w>
     <w lemma="keep" pos="vvi" xml:id="B05647-001-a-3220">keep</w>
     <w lemma="this" pos="d" xml:id="B05647-001-a-3230">this</w>
     <w lemma="meeting" pos="n1" xml:id="B05647-001-a-3240">meeting</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-3250">of</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-3260">Our</w>
     <w lemma="parliament" pos="n1" xml:id="B05647-001-a-3270">Parliament</w>
     <pc xml:id="B05647-001-a-3280">,</pc>
     <w lemma="under" pos="acp" xml:id="B05647-001-a-3290">under</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-3300">the</w>
     <w lemma="pain" pos="n2" xml:id="B05647-001-a-3310">pains</w>
     <w lemma="contain" pos="vvn" xml:id="B05647-001-a-3320">contained</w>
     <w lemma="in" pos="acp" xml:id="B05647-001-a-3330">in</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-3340">Our</w>
     <w lemma="act" pos="n2" xml:id="B05647-001-a-3350">Acts</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-3360">of</w>
     <w lemma="parliament" pos="n1" xml:id="B05647-001-a-3370">Parliament</w>
     <w lemma="make" pos="vvd" xml:id="B05647-001-a-3380">made</w>
     <w lemma="thereanent" pos="av" xml:id="B05647-001-a-3390">thereanent</w>
     <pc unit="sentence" xml:id="B05647-001-a-3400">.</pc>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-3410">And</w>
     <w lemma="that" pos="cs" xml:id="B05647-001-a-3420">that</w>
     <w lemma="all" pos="d" xml:id="B05647-001-a-3430">all</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-3440">Our</w>
     <w lemma="good" pos="j" xml:id="B05647-001-a-3450">good</w>
     <w lemma="subject" pos="n2" xml:id="B05647-001-a-3460">Subjects</w>
     <w lemma="may" pos="vmb" xml:id="B05647-001-a-3470">may</w>
     <w lemma="have" pos="vvi" xml:id="B05647-001-a-3480">have</w>
     <w lemma="notice" pos="n1" xml:id="B05647-001-a-3490">notice</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-3500">of</w>
     <w lemma="this" pos="d" xml:id="B05647-001-a-3510">this</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-3520">Our</w>
     <w lemma="royal" pos="j" xml:id="B05647-001-a-3530">Royal</w>
     <w lemma="will" pos="n1" xml:id="B05647-001-a-3540">Will</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-3550">and</w>
     <w lemma="pleasure" pos="n1" xml:id="B05647-001-a-3560">Pleasure</w>
     <pc xml:id="B05647-001-a-3570">,</pc>
     <w lemma="we" pos="pns" xml:id="B05647-001-a-3580">We</w>
     <w lemma="do" pos="vvb" xml:id="B05647-001-a-3590">do</w>
     <w lemma="hereby" pos="av" xml:id="B05647-001-a-3600">hereby</w>
     <w lemma="command" pos="vvi" xml:id="B05647-001-a-3610">command</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-3620">Our</w>
     <w lemma="lion" pos="n1" reg="Lion" xml:id="B05647-001-a-3630">Lyon</w>
     <w lemma="king" pos="n1" xml:id="B05647-001-a-3640">King</w>
     <w lemma="at" pos="acp" xml:id="B05647-001-a-3650">at</w>
     <w lemma="arm" pos="n2" xml:id="B05647-001-a-3660">Arms</w>
     <pc xml:id="B05647-001-a-3670">,</pc>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-3680">and</w>
     <w lemma="his" pos="po" xml:id="B05647-001-a-3690">his</w>
     <w lemma="brethren" pos="n2" xml:id="B05647-001-a-3700">brethren</w>
     <w lemma="herald" pos="n2" reg="Heralds" xml:id="B05647-001-a-3710">Heraulds</w>
     <pc xml:id="B05647-001-a-3720">,</pc>
     <w lemma="macer" pos="n2" xml:id="B05647-001-a-3730">Macers</w>
     <pc xml:id="B05647-001-a-3740">,</pc>
     <w lemma="pursuivant" pos="n2" reg="Pursuivants" xml:id="B05647-001-a-3750">Pursevants</w>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-3760">and</w>
     <w lemma="messenger" pos="n2" xml:id="B05647-001-a-3770">Messengers</w>
     <w lemma="at" pos="acp" xml:id="B05647-001-a-3780">at</w>
     <w lemma="arm" pos="n2" xml:id="B05647-001-a-3790">Arms</w>
     <pc xml:id="B05647-001-a-3800">,</pc>
     <w lemma="to" pos="prt" xml:id="B05647-001-a-3810">to</w>
     <w lemma="make" pos="vvi" xml:id="B05647-001-a-3820">make</w>
     <w lemma="timeous" pos="j" xml:id="B05647-001-a-3830">timeous</w>
     <w lemma="proclamation" pos="n1" xml:id="B05647-001-a-3840">Proclamation</w>
     <w lemma="hereof" pos="av" xml:id="B05647-001-a-3850">hereof</w>
     <w lemma="at" pos="acp" xml:id="B05647-001-a-3860">at</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-3870">the</w>
     <w lemma="mercat-crosse" pos="j" xml:id="B05647-001-a-3880">Mercat-crosse</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-3890">of</w>
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05647-001-a-3900">Edinburgh</w>
     <pc xml:id="B05647-001-a-3910">,</pc>
     <w lemma="and" pos="cc" xml:id="B05647-001-a-3920">and</w>
     <w lemma="at" pos="acp" xml:id="B05647-001-a-3930">at</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-3940">the</w>
     <w lemma="mercat-cross" pos="n2" xml:id="B05647-001-a-3950">Mercat-crosses</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-3960">of</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-3970">the</w>
     <w lemma="head" pos="n1" xml:id="B05647-001-a-3980">head</w>
     <w lemma="burrough" pos="n2" xml:id="B05647-001-a-3990">Burroughs</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-4000">of</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-4010">the</w>
     <w lemma="several" pos="j" xml:id="B05647-001-a-4020">several</w>
     <w lemma="shire" pos="n2" xml:id="B05647-001-a-4030">Shires</w>
     <w lemma="of" pos="acp" xml:id="B05647-001-a-4040">of</w>
     <w lemma="this" pos="d" xml:id="B05647-001-a-4050">this</w>
     <w lemma="our" pos="po" xml:id="B05647-001-a-4060">Our</w>
     <w lemma="kingdom" pos="n1" xml:id="B05647-001-a-4070">Kingdom</w>
     <pc xml:id="B05647-001-a-4080">,</pc>
     <w lemma="that" pos="cs" xml:id="B05647-001-a-4090">that</w>
     <w lemma="none" pos="pix" xml:id="B05647-001-a-4100">none</w>
     <w lemma="pretend" pos="vvb" xml:id="B05647-001-a-4110">pretend</w>
     <w lemma="ignorance" pos="n1" xml:id="B05647-001-a-4120">ignorance</w>
     <pc unit="sentence" xml:id="B05647-001-a-4130">.</pc>
    </p>
    <closer xml:id="B05647-e180">
     <signed xml:id="B05647-e190">
      <hi xml:id="B05647-e200">
       <w lemma="tho." pos="ab" xml:id="B05647-001-a-4140">Tho.</w>
       <w lemma="hay" pos="uh" xml:id="B05647-001-a-4150">Hay</w>
      </hi>
      <pc rend="follows-hi" xml:id="B05647-001-a-4160">,</pc>
      <w lemma="Cls." pos="nn1" xml:id="B05647-001-a-4170">Cls.</w>
      <w lemma="n/a" pos="fla" xml:id="B05647-001-a-4180">Sti</w>
      <w lemma="n/a" pos="fla" xml:id="B05647-001-a-4190">Concilii</w>
      <pc unit="sentence" xml:id="B05647-001-a-4200">.</pc>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="B05647-e210">
   <div type="colophon" xml:id="B05647-e220">
    <p xml:id="B05647-e230">
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05647-001-a-4210">EDINBURGH</w>
     <pc xml:id="B05647-001-a-4220">,</pc>
     <w lemma="print" pos="vvn" xml:id="B05647-001-a-4230">Printed</w>
     <w lemma="by" pos="acp" xml:id="B05647-001-a-4240">by</w>
     <hi xml:id="B05647-e250">
      <w lemma="Evan" pos="nn1" xml:id="B05647-001-a-4250">Evan</w>
      <w lemma="Tyler" pos="nn1" xml:id="B05647-001-a-4260">Tyler</w>
     </hi>
     <pc rend="follows-hi" xml:id="B05647-001-a-4270">,</pc>
     <w lemma="printer" pos="n1" xml:id="B05647-001-a-4280">Printer</w>
     <w lemma="to" pos="acp" xml:id="B05647-001-a-4290">to</w>
     <w lemma="the" pos="d" xml:id="B05647-001-a-4300">the</w>
     <w join="right" lemma="king" pos="n1" xml:id="B05647-001-a-4310">King</w>
     <w join="left" lemma="be" pos="vvz" xml:id="B05647-001-a-4311">'s</w>
     <w lemma="most" pos="avs-d" xml:id="B05647-001-a-4320">most</w>
     <w lemma="excellent" pos="j" xml:id="B05647-001-a-4330">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="B05647-001-a-4340">Majesty</w>
     <pc xml:id="B05647-001-a-4350">,</pc>
     <w lemma="1669." pos="crd" xml:id="B05647-001-a-4360">1669.</w>
     <pc unit="sentence" xml:id="B05647-001-a-4370"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
