<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B05707">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Proclamation indemnifying deserters, who shall return betwixt and the first day of January next to come.</title>
    <author>Scotland. Privy Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B05707 of text R183565 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing S1941). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B05707</idno>
    <idno type="STC">Wing S1941</idno>
    <idno type="STC">ESTC R183565</idno>
    <idno type="EEBO-CITATION">52529315</idno>
    <idno type="OCLC">ocm 52529315</idno>
    <idno type="VID">179102</idno>
    <idno type="PROQUESTGOID">2240873728</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B05707)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179102)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2776:91)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Proclamation indemnifying deserters, who shall return betwixt and the first day of January next to come.</title>
      <author>Scotland. Privy Council.</author>
      <author>Scotland. Sovereign (1694-1702 : William II)</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by the heirs and successors of Andrew Anderson, Printer to his most excellent Majesty,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>Anno Dom. 1696.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Royal arms at head of text; initial letter.</note>
      <note>Intentional blank spaces in text.</note>
      <note>Dated: Given under Our Signet at Edinburgh, the twelfth day of November, and of Our Reign the eighth year, 1696.</note>
      <note>Signed: Gilb. Eliot Cls. Sti. Concilii.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Military deserters -- Legal status, laws, etc. -- Scotland -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>Proclamation : indemnifying deserters, who shall return betwixt and the first day of January next to come.</ep:title>
    <ep:author>Scotland. Privy Council</ep:author>
    <ep:publicationYear>1696</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>419</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-01</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-01</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-03</date>
    <label>John Pas</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-10</date>
    <label>SPi Global</label>
        Rekeyed and resubmitted
      </change>
   <change>
    <date>2008-12</date>
    <label>John Pas</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-12</date>
    <label>John Pas</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B05707-t">
  <body xml:id="B05707-e0">
   <div type="proclamation" xml:id="B05707-e10">
    <pb facs="tcp:179102:1" rend="simple:additions" xml:id="B05707-001-a"/>
    <head xml:id="B05707-e20">
     <w lemma="proclamation" pos="n1" xml:id="B05707-001-a-0010">PROCLAMATION</w>
     <pc xml:id="B05707-001-a-0020">,</pc>
     <w lemma="indemnify" pos="vvg" xml:id="B05707-001-a-0030">Indemnifying</w>
     <w lemma="deserter" pos="n2" xml:id="B05707-001-a-0040">Deserters</w>
     <pc xml:id="B05707-001-a-0050">,</pc>
     <w lemma="who" pos="crq" xml:id="B05707-001-a-0060">who</w>
     <w lemma="shall" pos="vmb" xml:id="B05707-001-a-0070">shall</w>
     <w lemma="return" pos="vvi" xml:id="B05707-001-a-0080">return</w>
     <w lemma="betwixt" pos="acp" xml:id="B05707-001-a-0090">betwixt</w>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-0100">and</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-0110">the</w>
     <w lemma="first" pos="ord" xml:id="B05707-001-a-0120">First</w>
     <w lemma="day" pos="n1" xml:id="B05707-001-a-0130">Day</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-0140">of</w>
     <w lemma="January" pos="nn1" rend="hi" xml:id="B05707-001-a-0150">January</w>
     <w lemma="next" pos="ord" xml:id="B05707-001-a-0160">next</w>
     <w lemma="to" pos="prt" xml:id="B05707-001-a-0170">to</w>
     <w lemma="come" pos="vvi" xml:id="B05707-001-a-0180">come</w>
     <pc unit="sentence" xml:id="B05707-001-a-0190">.</pc>
    </head>
    <p xml:id="B05707-e40">
     <w lemma="william" pos="nn1" rend="hi" xml:id="B05707-001-a-0200">WILLIAM</w>
     <w lemma="by" pos="acp" xml:id="B05707-001-a-0210">by</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-0220">the</w>
     <w lemma="grace" pos="n1" xml:id="B05707-001-a-0230">Grace</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-0240">of</w>
     <w lemma="GOD" pos="nn1" xml:id="B05707-001-a-0250">GOD</w>
     <pc xml:id="B05707-001-a-0260">,</pc>
     <w lemma="king" pos="n1" xml:id="B05707-001-a-0270">King</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-0280">of</w>
     <hi xml:id="B05707-e60">
      <w lemma="great-britain" pos="nn1" xml:id="B05707-001-a-0290">Great-Britain</w>
      <pc xml:id="B05707-001-a-0300">,</pc>
      <w lemma="France" pos="nn1" xml:id="B05707-001-a-0310">France</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-0320">and</w>
     <hi xml:id="B05707-e70">
      <w lemma="Ireland" pos="nn1" xml:id="B05707-001-a-0330">Ireland</w>
      <pc xml:id="B05707-001-a-0340">,</pc>
     </hi>
     <w lemma="defender" pos="n1" xml:id="B05707-001-a-0350">Defender</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-0360">of</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-0370">the</w>
     <w lemma="faith" pos="n1" xml:id="B05707-001-a-0380">Faith</w>
     <pc xml:id="B05707-001-a-0390">,</pc>
     <w lemma="to" pos="prt" xml:id="B05707-001-a-0400">to</w>
     <seg type="fill-in-blank" xml:id="B05707-e80">
      <w lemma="" pos="sy" xml:id="B05707-001-a-0410">_____</w>
     </seg>
     <w lemma="macer" pos="n2" xml:id="B05707-001-a-0420">Macers</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-0430">of</w>
     <w lemma="our" pos="po" xml:id="B05707-001-a-0440">Our</w>
     <w lemma="privy" pos="j" xml:id="B05707-001-a-0450">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05707-001-a-0460">Council</w>
     <pc xml:id="B05707-001-a-0470">,</pc>
     <w lemma="messenger" pos="n2" xml:id="B05707-001-a-0480">Messengers</w>
     <w lemma="at" pos="acp" xml:id="B05707-001-a-0490">at</w>
     <w lemma="arm" pos="n2" xml:id="B05707-001-a-0500">Arms</w>
     <w lemma="our" pos="po" xml:id="B05707-001-a-0510">Our</w>
     <w lemma="sheriff" pos="n2" xml:id="B05707-001-a-0520">Sheriffs</w>
     <w lemma="in" pos="acp" xml:id="B05707-001-a-0530">in</w>
     <w lemma="that" pos="d" xml:id="B05707-001-a-0540">that</w>
     <w lemma="part" pos="n1" xml:id="B05707-001-a-0550">part</w>
     <pc xml:id="B05707-001-a-0560">,</pc>
     <w lemma="conjunct" pos="av-j" xml:id="B05707-001-a-0570">conjunctly</w>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-0580">and</w>
     <w lemma="several" pos="av-j" xml:id="B05707-001-a-0590">severally</w>
     <pc xml:id="B05707-001-a-0600">,</pc>
     <w lemma="special" pos="av-j" xml:id="B05707-001-a-0610">specially</w>
     <w lemma="constitute" pos="vvi" xml:id="B05707-001-a-0620">constitute</w>
     <pc xml:id="B05707-001-a-0630">,</pc>
     <w lemma="greeting" pos="n1" xml:id="B05707-001-a-0640">Greeting</w>
     <pc xml:id="B05707-001-a-0650">;</pc>
     <w lemma="forasmuch" pos="av" xml:id="B05707-001-a-0660">Forasmuch</w>
     <w lemma="as" pos="acp" xml:id="B05707-001-a-0670">as</w>
     <w lemma="we" pos="pns" xml:id="B05707-001-a-0680">We</w>
     <w lemma="be" pos="vvb" xml:id="B05707-001-a-0690">are</w>
     <w lemma="inform" pos="vvn" xml:id="B05707-001-a-0700">informed</w>
     <pc xml:id="B05707-001-a-0710">,</pc>
     <w lemma="that" pos="cs" xml:id="B05707-001-a-0720">that</w>
     <w lemma="several" pos="j" xml:id="B05707-001-a-0730">several</w>
     <w lemma="soldier" pos="n2" reg="Soldiers" xml:id="B05707-001-a-0740">Souldiers</w>
     <w lemma="belong" pos="vvg" xml:id="B05707-001-a-0750">belonging</w>
     <w lemma="to" pos="acp" xml:id="B05707-001-a-0760">to</w>
     <w lemma="our" pos="po" xml:id="B05707-001-a-0770">Our</w>
     <w lemma="force" pos="n2" xml:id="B05707-001-a-0780">Forces</w>
     <w lemma="within" pos="acp" xml:id="B05707-001-a-0790">within</w>
     <w lemma="this" pos="d" xml:id="B05707-001-a-0800">this</w>
     <w lemma="our" pos="po" xml:id="B05707-001-a-0810">Our</w>
     <w lemma="ancient" pos="j" xml:id="B05707-001-a-0820">Ancient</w>
     <w lemma="kingdom" pos="n1" xml:id="B05707-001-a-0830">Kingdom</w>
     <pc xml:id="B05707-001-a-0840">,</pc>
     <w lemma="have" pos="vvb" xml:id="B05707-001-a-0850">have</w>
     <w lemma="desert" pos="vvd" xml:id="B05707-001-a-0860">Deserted</w>
     <pc xml:id="B05707-001-a-0870">;</pc>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-0880">And</w>
     <w lemma="we" pos="pns" xml:id="B05707-001-a-0890">We</w>
     <w lemma="be" pos="vvg" xml:id="B05707-001-a-0900">being</w>
     <w lemma="desirous" pos="j" xml:id="B05707-001-a-0910">desirous</w>
     <w lemma="rather" pos="av" xml:id="B05707-001-a-0920">rather</w>
     <w lemma="to" pos="prt" xml:id="B05707-001-a-0930">to</w>
     <w lemma="reclaim" pos="vvi" xml:id="B05707-001-a-0940">reclaim</w>
     <w lemma="transgressor" pos="n2" reg="Transgressors" xml:id="B05707-001-a-0950">Transgressours</w>
     <w lemma="by" pos="acp" xml:id="B05707-001-a-0960">by</w>
     <w lemma="clemency" pos="n1" xml:id="B05707-001-a-0970">Clemency</w>
     <pc xml:id="B05707-001-a-0980">,</pc>
     <w lemma="than" pos="cs" xml:id="B05707-001-a-0990">than</w>
     <w lemma="to" pos="prt" xml:id="B05707-001-a-1000">to</w>
     <w lemma="punish" pos="vvi" xml:id="B05707-001-a-1010">punish</w>
     <w lemma="they" pos="pno" xml:id="B05707-001-a-1020">them</w>
     <w lemma="with" pos="acp" xml:id="B05707-001-a-1030">with</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-1040">the</w>
     <w lemma="outmost" pos="j" xml:id="B05707-001-a-1050">outmost</w>
     <w lemma="severity" pos="n1" xml:id="B05707-001-a-1060">Severity</w>
     <pc xml:id="B05707-001-a-1070">;</pc>
     <w lemma="therefore" pos="av" xml:id="B05707-001-a-1080">Therefore</w>
     <w lemma="we" pos="pns" xml:id="B05707-001-a-1090">We</w>
     <pc xml:id="B05707-001-a-1100">,</pc>
     <w lemma="with" pos="acp" xml:id="B05707-001-a-1110">with</w>
     <w lemma="advice" pos="n1" xml:id="B05707-001-a-1120">Advice</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-1130">of</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-1140">the</w>
     <w lemma="lord" pos="n2" xml:id="B05707-001-a-1150">Lords</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-1160">of</w>
     <w lemma="our" pos="po" xml:id="B05707-001-a-1170">Our</w>
     <w lemma="privy" pos="j" xml:id="B05707-001-a-1180">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05707-001-a-1190">Council</w>
     <pc xml:id="B05707-001-a-1200">,</pc>
     <w lemma="have" pos="vvb" xml:id="B05707-001-a-1210">have</w>
     <w lemma="think" pos="vvn" xml:id="B05707-001-a-1220">thought</w>
     <w lemma="fit" pos="j" xml:id="B05707-001-a-1230">fit</w>
     <w lemma="to" pos="prt" xml:id="B05707-001-a-1240">to</w>
     <w lemma="require" pos="vvi" xml:id="B05707-001-a-1250">Require</w>
     <pc xml:id="B05707-001-a-1260">,</pc>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-1270">and</w>
     <w lemma="do" pos="vvb" xml:id="B05707-001-a-1280">do</w>
     <w lemma="hereby" pos="av" xml:id="B05707-001-a-1290">hereby</w>
     <w lemma="require" pos="vvi" xml:id="B05707-001-a-1300">Require</w>
     <w lemma="all" pos="d" xml:id="B05707-001-a-1310">all</w>
     <w lemma="soldier" pos="n2" reg="Soldiers" xml:id="B05707-001-a-1320">Souldiers</w>
     <w lemma="that" pos="cs" xml:id="B05707-001-a-1330">that</w>
     <w lemma="have" pos="vvb" xml:id="B05707-001-a-1340">have</w>
     <w lemma="desert" pos="vvd" xml:id="B05707-001-a-1350">Deserted</w>
     <pc xml:id="B05707-001-a-1360">,</pc>
     <w lemma="to" pos="prt" xml:id="B05707-001-a-1370">to</w>
     <w lemma="return" pos="vvi" xml:id="B05707-001-a-1380">return</w>
     <w lemma="to" pos="acp" xml:id="B05707-001-a-1390">to</w>
     <w lemma="their" pos="po" xml:id="B05707-001-a-1400">their</w>
     <w lemma="colour" pos="n2" xml:id="B05707-001-a-1410">Colours</w>
     <pc xml:id="B05707-001-a-1420">,</pc>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-1430">and</w>
     <w lemma="to" pos="acp" xml:id="B05707-001-a-1440">to</w>
     <w lemma="our" pos="po" xml:id="B05707-001-a-1450">Our</w>
     <w lemma="service" pos="n1" xml:id="B05707-001-a-1460">Service</w>
     <pc xml:id="B05707-001-a-1470">,</pc>
     <w lemma="betwixt" pos="acp" xml:id="B05707-001-a-1480">betwixt</w>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-1490">and</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-1500">the</w>
     <w lemma="first" pos="ord" xml:id="B05707-001-a-1510">First</w>
     <w lemma="day" pos="n1" xml:id="B05707-001-a-1520">day</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-1530">of</w>
     <w lemma="January" pos="nn1" rend="hi" xml:id="B05707-001-a-1540">January</w>
     <w lemma="next" pos="ord" xml:id="B05707-001-a-1550">next</w>
     <w lemma="to" pos="prt" xml:id="B05707-001-a-1560">to</w>
     <w lemma="come" pos="vvi" xml:id="B05707-001-a-1570">come</w>
     <pc xml:id="B05707-001-a-1580">,</pc>
     <w lemma="promise" pos="vvg" xml:id="B05707-001-a-1590">promising</w>
     <w lemma="to" pos="prt" xml:id="B05707-001-a-1600">to</w>
     <w lemma="indemnify" pos="vvi" reg="Indemnify" xml:id="B05707-001-a-1610">Indemnifie</w>
     <pc xml:id="B05707-001-a-1620">,</pc>
     <w lemma="likea" pos="n2" xml:id="B05707-001-a-1630">likeas</w>
     <w lemma="we" pos="pns" xml:id="B05707-001-a-1640">We</w>
     <w lemma="do" pos="vvb" xml:id="B05707-001-a-1650">do</w>
     <w lemma="hereby" pos="av" xml:id="B05707-001-a-1660">hereby</w>
     <w lemma="full" pos="av-j" xml:id="B05707-001-a-1670">fully</w>
     <w lemma="indemnify" pos="vvi" reg="Indemnify" xml:id="B05707-001-a-1680">Indemnifie</w>
     <w lemma="for" pos="acp" xml:id="B05707-001-a-1690">for</w>
     <w lemma="their" pos="po" xml:id="B05707-001-a-1700">their</w>
     <w lemma="by" pos="acp" xml:id="B05707-001-a-1710">by</w>
     <w lemma="past" pos="j" xml:id="B05707-001-a-1720">past</w>
     <w lemma="desert" pos="vvg" xml:id="B05707-001-a-1730">deserting</w>
     <pc xml:id="B05707-001-a-1740">,</pc>
     <w lemma="all" pos="d" xml:id="B05707-001-a-1750">all</w>
     <w lemma="that" pos="cs" xml:id="B05707-001-a-1760">that</w>
     <w lemma="shall" pos="vmb" xml:id="B05707-001-a-1770">shall</w>
     <w lemma="return" pos="vvi" xml:id="B05707-001-a-1780">return</w>
     <w lemma="in" pos="acp" xml:id="B05707-001-a-1790">in</w>
     <w lemma="manner" pos="n1" xml:id="B05707-001-a-1800">manner</w>
     <w lemma="foresay" pos="j-vn" xml:id="B05707-001-a-1810">foresaid</w>
     <pc xml:id="B05707-001-a-1820">;</pc>
     <w lemma="but" pos="acp" xml:id="B05707-001-a-1830">But</w>
     <w lemma="on" pos="acp" xml:id="B05707-001-a-1840">on</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-1850">the</w>
     <w lemma="other" pos="d" xml:id="B05707-001-a-1860">other</w>
     <w lemma="hand" pos="n1" xml:id="B05707-001-a-1870">hand</w>
     <pc xml:id="B05707-001-a-1880">,</pc>
     <w lemma="certify" pos="vvg" xml:id="B05707-001-a-1890">certifying</w>
     <w lemma="such" pos="d" xml:id="B05707-001-a-1900">such</w>
     <w lemma="as" pos="acp" xml:id="B05707-001-a-1910">as</w>
     <w lemma="shall" pos="vmb" xml:id="B05707-001-a-1920">shall</w>
     <w lemma="not" pos="xx" xml:id="B05707-001-a-1930">not</w>
     <w lemma="return" pos="vvi" xml:id="B05707-001-a-1940">return</w>
     <w lemma="betwixt" pos="acp" xml:id="B05707-001-a-1950">betwixt</w>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-1960">and</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-1970">the</w>
     <w lemma="day" pos="n1" xml:id="B05707-001-a-1980">day</w>
     <w lemma="foresay" pos="j-vn" xml:id="B05707-001-a-1990">foresaid</w>
     <pc xml:id="B05707-001-a-2000">,</pc>
     <w lemma="that" pos="cs" xml:id="B05707-001-a-2010">that</w>
     <w lemma="both" pos="d" xml:id="B05707-001-a-2020">both</w>
     <w lemma="they" pos="pns" xml:id="B05707-001-a-2030">they</w>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-2040">and</w>
     <w lemma="their" pos="po" xml:id="B05707-001-a-2050">their</w>
     <w lemma="resetter" pos="n2" xml:id="B05707-001-a-2060">Resetters</w>
     <w lemma="shall" pos="vmb" xml:id="B05707-001-a-2070">shall</w>
     <w lemma="be" pos="vvi" xml:id="B05707-001-a-2080">be</w>
     <w lemma="prosecute" pos="vvi" xml:id="B05707-001-a-2090">prosecute</w>
     <w lemma="with" pos="acp" xml:id="B05707-001-a-2100">with</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-2110">the</w>
     <w lemma="outmost" pos="j" xml:id="B05707-001-a-2120">outmost</w>
     <w lemma="severity" pos="n1" xml:id="B05707-001-a-2130">severity</w>
     <pc xml:id="B05707-001-a-2140">,</pc>
     <w lemma="conform" pos="vvb" xml:id="B05707-001-a-2150">conform</w>
     <w lemma="to" pos="acp" xml:id="B05707-001-a-2160">to</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-2170">the</w>
     <w lemma="law" pos="n2" xml:id="B05707-001-a-2180">Laws</w>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-2190">and</w>
     <w lemma="proclamation" pos="n2" xml:id="B05707-001-a-2200">Proclamations</w>
     <w lemma="emit" pos="vvn" xml:id="B05707-001-a-2210">emitted</w>
     <w lemma="against" pos="acp" xml:id="B05707-001-a-2220">against</w>
     <w lemma="they" pos="pno" xml:id="B05707-001-a-2230">them</w>
     <pc unit="sentence" xml:id="B05707-001-a-2240">.</pc>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-2250">And</w>
     <w lemma="far" pos="avc-j" reg="farther" xml:id="B05707-001-a-2260">farder</w>
     <pc xml:id="B05707-001-a-2270">,</pc>
     <w lemma="we" pos="pns" xml:id="B05707-001-a-2280">We</w>
     <w lemma="do" pos="vvb" xml:id="B05707-001-a-2290">do</w>
     <w lemma="hereby" pos="av" xml:id="B05707-001-a-2300">hereby</w>
     <w lemma="empower" pos="vvi" reg="Empower" xml:id="B05707-001-a-2310">Impower</w>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-2320">and</w>
     <w lemma="require" pos="vvi" xml:id="B05707-001-a-2330">Require</w>
     <w lemma="all" pos="d" xml:id="B05707-001-a-2340">all</w>
     <w lemma="officer" pos="n2" xml:id="B05707-001-a-2350">Officers</w>
     <w lemma="whatsoever" pos="crq" xml:id="B05707-001-a-2360">whatsomever</w>
     <pc xml:id="B05707-001-a-2370">,</pc>
     <w lemma="belong" pos="vvg" xml:id="B05707-001-a-2380">belonging</w>
     <w lemma="to" pos="acp" xml:id="B05707-001-a-2390">to</w>
     <w lemma="our" pos="po" xml:id="B05707-001-a-2400">Our</w>
     <w lemma="force" pos="n2" xml:id="B05707-001-a-2410">Forces</w>
     <pc xml:id="B05707-001-a-2420">,</pc>
     <w lemma="either" pos="av-d" xml:id="B05707-001-a-2430">either</w>
     <w lemma="at" pos="acp" xml:id="B05707-001-a-2440">at</w>
     <w lemma="home" pos="n1" xml:id="B05707-001-a-2450">home</w>
     <w lemma="or" pos="cc" xml:id="B05707-001-a-2460">or</w>
     <w lemma="abroad" pos="av" xml:id="B05707-001-a-2470">abroad</w>
     <pc xml:id="B05707-001-a-2480">,</pc>
     <w lemma="to" pos="prt" xml:id="B05707-001-a-2490">to</w>
     <w lemma="seize" pos="vvi" xml:id="B05707-001-a-2500">seize</w>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-2510">and</w>
     <w lemma="apprehend" pos="vvi" xml:id="B05707-001-a-2520">apprehend</w>
     <w lemma="after" pos="acp" xml:id="B05707-001-a-2530">after</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-2540">the</w>
     <w lemma="day" pos="n1" xml:id="B05707-001-a-2550">day</w>
     <w lemma="foresay" pos="j-vn" xml:id="B05707-001-a-2560">foresaid</w>
     <pc xml:id="B05707-001-a-2570">,</pc>
     <w lemma="all" pos="d" xml:id="B05707-001-a-2580">all</w>
     <w lemma="such" pos="d" xml:id="B05707-001-a-2590">such</w>
     <w lemma="deserter" pos="n2" xml:id="B05707-001-a-2600">Deserters</w>
     <w lemma="as" pos="acp" xml:id="B05707-001-a-2610">as</w>
     <w lemma="shall" pos="vmb" xml:id="B05707-001-a-2620">shall</w>
     <w lemma="not" pos="xx" xml:id="B05707-001-a-2630">not</w>
     <w lemma="accept" pos="vvi" xml:id="B05707-001-a-2640">accept</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-2650">of</w>
     <w lemma="this" pos="d" xml:id="B05707-001-a-2660">this</w>
     <w lemma="our" pos="po" xml:id="B05707-001-a-2670">Our</w>
     <w lemma="gracious" pos="j" xml:id="B05707-001-a-2680">gracious</w>
     <w lemma="offer" pos="n1" xml:id="B05707-001-a-2690">offer</w>
     <pc xml:id="B05707-001-a-2700">,</pc>
     <w lemma="to" pos="prt" xml:id="B05707-001-a-2710">to</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-2720">the</w>
     <w lemma="effect" pos="n1" xml:id="B05707-001-a-2730">effect</w>
     <w lemma="they" pos="pns" xml:id="B05707-001-a-2740">they</w>
     <w lemma="may" pos="vmb" xml:id="B05707-001-a-2750">may</w>
     <w lemma="be" pos="vvi" xml:id="B05707-001-a-2760">be</w>
     <w lemma="condign" pos="av-j" xml:id="B05707-001-a-2770">condignly</w>
     <w lemma="punish" pos="vvn" xml:id="B05707-001-a-2780">punished</w>
     <pc xml:id="B05707-001-a-2790">,</pc>
     <w lemma="as" pos="acp" xml:id="B05707-001-a-2800">as</w>
     <w lemma="say" pos="vvn" xml:id="B05707-001-a-2810">said</w>
     <w lemma="be" pos="vvz" xml:id="B05707-001-a-2820">is</w>
     <pc unit="sentence" xml:id="B05707-001-a-2830">.</pc>
     <w lemma="our" pos="po" xml:id="B05707-001-a-2840">Our</w>
     <w lemma="will" pos="n1" xml:id="B05707-001-a-2850">Will</w>
     <w lemma="be" pos="vvz" xml:id="B05707-001-a-2860">is</w>
     <w lemma="herefore" pos="av" xml:id="B05707-001-a-2870">herefore</w>
     <pc xml:id="B05707-001-a-2880">,</pc>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-2890">and</w>
     <w lemma="we" pos="pns" xml:id="B05707-001-a-2900">We</w>
     <w lemma="charge" pos="vvb" xml:id="B05707-001-a-2910">Charge</w>
     <w lemma="you" pos="pn" xml:id="B05707-001-a-2920">you</w>
     <w lemma="strict" pos="av-j" xml:id="B05707-001-a-2930">strictly</w>
     <pc xml:id="B05707-001-a-2940">,</pc>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-2950">and</w>
     <w lemma="command" pos="vvb" xml:id="B05707-001-a-2960">Command</w>
     <w lemma="that" pos="d" xml:id="B05707-001-a-2970">that</w>
     <w lemma="incontinent" pos="j" xml:id="B05707-001-a-2980">incontinent</w>
     <w lemma="these" pos="d" xml:id="B05707-001-a-2990">these</w>
     <w lemma="our" pos="po" xml:id="B05707-001-a-3000">Our</w>
     <w lemma="letter" pos="n2" xml:id="B05707-001-a-3010">Letters</w>
     <w lemma="see" pos="vvn" xml:id="B05707-001-a-3020">seen</w>
     <pc xml:id="B05707-001-a-3030">,</pc>
     <w lemma="you" pos="pn" xml:id="B05707-001-a-3040">ye</w>
     <w lemma="pass" pos="vvb" xml:id="B05707-001-a-3050">pass</w>
     <w lemma="to" pos="acp" xml:id="B05707-001-a-3060">to</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-3070">the</w>
     <w lemma="mercat-cross" pos="n1" xml:id="B05707-001-a-3080">Mercat-Cross</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-3090">of</w>
     <hi xml:id="B05707-e100">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05707-001-a-3100">Edinburgh</w>
      <pc xml:id="B05707-001-a-3110">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-3120">and</w>
     <w lemma="to" pos="acp" xml:id="B05707-001-a-3130">to</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-3140">the</w>
     <w lemma="mercat-cross" pos="n2" xml:id="B05707-001-a-3150">Mercat-Crosses</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-3160">of</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-3170">the</w>
     <w lemma="n/a" pos="fla" xml:id="B05707-001-a-3180">remanent</w>
     <w lemma="headburgh" pos="n2" xml:id="B05707-001-a-3190">Head-Burghs</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-3200">of</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-3210">the</w>
     <w lemma="several" pos="j" xml:id="B05707-001-a-3220">several</w>
     <w lemma="shire" pos="n2" xml:id="B05707-001-a-3230">Shires</w>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-3240">and</w>
     <w lemma="stewartry" pos="n2" xml:id="B05707-001-a-3250">Stewartries</w>
     <w lemma="within" pos="acp" xml:id="B05707-001-a-3260">within</w>
     <w lemma="this" pos="d" xml:id="B05707-001-a-3270">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05707-001-a-3280">Kingdom</w>
     <pc xml:id="B05707-001-a-3290">,</pc>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-3300">and</w>
     <w lemma="there" pos="av" xml:id="B05707-001-a-3310">there</w>
     <w lemma="in" pos="acp" xml:id="B05707-001-a-3320">in</w>
     <w lemma="our" pos="po" xml:id="B05707-001-a-3330">Our</w>
     <w lemma="name" pos="n1" xml:id="B05707-001-a-3340">Name</w>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-3350">and</w>
     <w lemma="authority" pos="n1" xml:id="B05707-001-a-3360">Authority</w>
     <pc xml:id="B05707-001-a-3370">,</pc>
     <w lemma="by" pos="acp" xml:id="B05707-001-a-3380">by</w>
     <w lemma="open" pos="j" xml:id="B05707-001-a-3390">open</w>
     <w lemma="proclamation" pos="n1" xml:id="B05707-001-a-3400">Proclamation</w>
     <pc xml:id="B05707-001-a-3410">,</pc>
     <w lemma="make" pos="vvb" xml:id="B05707-001-a-3420">make</w>
     <w lemma="publication" pos="n1" xml:id="B05707-001-a-3430">Publication</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-3440">of</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-3450">the</w>
     <w lemma="premise" pos="n2" reg="Premises" xml:id="B05707-001-a-3460">Premisses</w>
     <pc xml:id="B05707-001-a-3470">,</pc>
     <w lemma="that" pos="cs" xml:id="B05707-001-a-3480">that</w>
     <w lemma="none" pos="pix" xml:id="B05707-001-a-3490">none</w>
     <w lemma="pretend" pos="vvb" xml:id="B05707-001-a-3500">pretend</w>
     <w lemma="ignorance" pos="n1" xml:id="B05707-001-a-3510">ignorance</w>
     <pc xml:id="B05707-001-a-3520">;</pc>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-3530">And</w>
     <w lemma="ordain" pos="vvz" xml:id="B05707-001-a-3540">Ordains</w>
     <w lemma="these" pos="d" xml:id="B05707-001-a-3550">these</w>
     <w lemma="present" pos="n2" xml:id="B05707-001-a-3560">presents</w>
     <w lemma="to" pos="prt" xml:id="B05707-001-a-3570">to</w>
     <w lemma="be" pos="vvi" xml:id="B05707-001-a-3580">be</w>
     <w lemma="print" pos="vvn" xml:id="B05707-001-a-3590">Printed</w>
     <pc unit="sentence" xml:id="B05707-001-a-3600">.</pc>
    </p>
    <closer xml:id="B05707-e110">
     <dateline xml:id="B05707-e120">
      <w lemma="give" pos="vvn" xml:id="B05707-001-a-3610">Given</w>
      <w lemma="under" pos="acp" xml:id="B05707-001-a-3620">under</w>
      <w lemma="our" pos="po" xml:id="B05707-001-a-3630">Our</w>
      <w lemma="signet" pos="n1" xml:id="B05707-001-a-3640">Signet</w>
      <w lemma="at" pos="acp" xml:id="B05707-001-a-3650">at</w>
      <hi xml:id="B05707-e130">
       <w lemma="Edinburgh" pos="nn1" xml:id="B05707-001-a-3660">Edinburgh</w>
       <pc xml:id="B05707-001-a-3670">,</pc>
      </hi>
      <date xml:id="B05707-e140">
       <w lemma="the" pos="d" xml:id="B05707-001-a-3680">the</w>
       <w lemma="twelve" pos="ord" xml:id="B05707-001-a-3690">Twelfth</w>
       <w lemma="day" pos="n1" xml:id="B05707-001-a-3700">day</w>
       <w lemma="of" pos="acp" xml:id="B05707-001-a-3710">of</w>
       <hi xml:id="B05707-e150">
        <w lemma="November" pos="nn1" xml:id="B05707-001-a-3720">November</w>
        <pc xml:id="B05707-001-a-3730">,</pc>
       </hi>
       <w lemma="and" pos="cc" xml:id="B05707-001-a-3740">and</w>
       <w lemma="of" pos="acp" xml:id="B05707-001-a-3750">of</w>
       <w lemma="our" pos="po" xml:id="B05707-001-a-3760">Our</w>
       <w lemma="reign" pos="n1" xml:id="B05707-001-a-3770">Reign</w>
       <w lemma="the" pos="d" xml:id="B05707-001-a-3780">the</w>
       <w lemma="eight" pos="ord" xml:id="B05707-001-a-3790">Eighth</w>
       <w lemma="year" pos="n1" xml:id="B05707-001-a-3800">Year</w>
       <pc xml:id="B05707-001-a-3810">,</pc>
       <w lemma="1696." pos="crd" xml:id="B05707-001-a-3820">1696.</w>
       <pc unit="sentence" xml:id="B05707-001-a-3830"/>
      </date>
     </dateline>
     <hi xml:id="B05707-e160">
      <w lemma="n/a" pos="fla" xml:id="B05707-001-a-3840">Per</w>
      <w lemma="n/a" pos="fla" xml:id="B05707-001-a-3850">Actum</w>
      <w lemma="n/a" pos="fla" xml:id="B05707-001-a-3860">Dominorum</w>
      <w lemma="n/a" pos="fla" xml:id="B05707-001-a-3870">Secreti</w>
      <w lemma="n/a" pos="fla" xml:id="B05707-001-a-3880">Concilii</w>
      <pc unit="sentence" xml:id="B05707-001-a-3890">.</pc>
     </hi>
     <signed xml:id="B05707-e170">
      <w lemma="GILB" pos="nn1" xml:id="B05707-001-a-3900">GILB</w>
      <pc unit="sentence" xml:id="B05707-001-a-3910">.</pc>
      <w lemma="eliot" pos="nn1" xml:id="B05707-001-a-3920">ELIOT</w>
      <hi xml:id="B05707-e180">
       <w lemma="Cls." pos="nn1" xml:id="B05707-001-a-3930">Cls.</w>
       <w lemma="sti." pos="ab" xml:id="B05707-001-a-3940">Sti.</w>
       <w lemma="n/a" pos="fla" xml:id="B05707-001-a-3950">Concilii</w>
       <pc unit="sentence" xml:id="B05707-001-a-3960">.</pc>
      </hi>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="B05707-e190">
   <div type="colophon" xml:id="B05707-e200">
    <p xml:id="B05707-e210">
     <hi xml:id="B05707-e220">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05707-001-a-3970">Edinburgh</w>
      <pc xml:id="B05707-001-a-3980">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="B05707-001-a-3990">Printed</w>
     <w lemma="by" pos="acp" xml:id="B05707-001-a-4000">by</w>
     <w lemma="the" pos="d" xml:id="B05707-001-a-4010">the</w>
     <w lemma="heir" pos="n2" xml:id="B05707-001-a-4020">Heirs</w>
     <w lemma="and" pos="cc" xml:id="B05707-001-a-4030">and</w>
     <w lemma="successor" pos="n2" xml:id="B05707-001-a-4040">Successors</w>
     <w lemma="of" pos="acp" xml:id="B05707-001-a-4050">of</w>
     <hi xml:id="B05707-e230">
      <w lemma="Andrew" pos="nn1" xml:id="B05707-001-a-4060">Andrew</w>
      <w lemma="Anderson" pos="nn1" xml:id="B05707-001-a-4070">Anderson</w>
      <pc xml:id="B05707-001-a-4080">,</pc>
     </hi>
     <w lemma="printer" pos="n1" xml:id="B05707-001-a-4090">Printer</w>
     <w lemma="to" pos="acp" xml:id="B05707-001-a-4100">to</w>
     <w lemma="his" pos="po" xml:id="B05707-001-a-4110">His</w>
     <w lemma="most" pos="avs-d" xml:id="B05707-001-a-4120">most</w>
     <w lemma="excellent" pos="j" xml:id="B05707-001-a-4130">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="B05707-001-a-4140">Majesty</w>
     <pc xml:id="B05707-001-a-4150">,</pc>
     <hi xml:id="B05707-e240">
      <w lemma="n/a" pos="fla" xml:id="B05707-001-a-4160">Anno</w>
      <w lemma="dom." pos="ab" xml:id="B05707-001-a-4170">DOM.</w>
     </hi>
     <w lemma="1696." pos="crd" xml:id="B05707-001-a-4180">1696.</w>
     <pc unit="sentence" xml:id="B05707-001-a-4190"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
