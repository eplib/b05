<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B05239">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Edinburgh, the 13 of December. 1644</title>
    <author>Scotland. Parliament. Committee of Estates.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B05239 of text R183975 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing S1195B). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B05239</idno>
    <idno type="STC">Wing S1195B</idno>
    <idno type="STC">ESTC R183975</idno>
    <idno type="EEBO-CITATION">51617461</idno>
    <idno type="OCLC">ocm 51617461</idno>
    <idno type="VID">175356</idno>
    <idno type="PROQUESTGOID">2240909089</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. B05239)</note>
    <note>Transcribed from: (Early English Books Online ; image set 175356)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2730:26)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Edinburgh, the 13 of December. 1644</title>
      <author>Scotland. Parliament. Committee of Estates.</author>
      <author>Primrose, Archibald, Sir, 1616-1679.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>By Evan Tyler, Printer to the Kings most excellent Majestie,</publisher>
      <pubPlace>Printed at Edinburgh :</pubPlace>
      <date>1644.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Imprint from colophon.</note>
      <note>Text begins: The Committee of Estates, taking to their consideration the present condition &amp; estate of this kingdom ....</note>
      <note>Signed at foot of text: Arch. Primerose.</note>
      <note>Provides for relief for sufferers from the rebels.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Public welfare -- Scotland -- Early works to 1800.</term>
     <term>Scotland -- Politics and government -- 1642-1649 -- Early works to 1800.</term>
     <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>Edinburgh, the 13 of December. 1644. The Committee of Estates, taking to their consideration the present condition &amp; estate of this kingdom ...</ep:title>
    <ep:author>Scotland. Convention of Estates</ep:author>
    <ep:publicationYear>1644</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>319</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-10</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-12</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2009-05</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-05</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B05239-e10010">
  <body xml:id="B05239-e10020">
   <div type="text" xml:id="B05239-e10030">
    <pb facs="tcp:175356:1" rend="simple:additions" xml:id="B05239-001-a"/>
    <opener xml:id="B05239-e10040">
     <dateline xml:id="B05239-e10050">
      <w lemma="Edinburgh" pos="nn1" xml:id="B05239-001-a-0010">EDINBURGH</w>
      <pc xml:id="B05239-001-a-0020">,</pc>
      <date xml:id="B05239-e10060">
       <w lemma="the" pos="d" xml:id="B05239-001-a-0030">the</w>
       <w lemma="13" pos="crd" xml:id="B05239-001-a-0040">13</w>
       <w lemma="of" pos="acp" xml:id="B05239-001-a-0050">of</w>
       <w lemma="December" pos="nn1" xml:id="B05239-001-a-0060">December</w>
       <pc unit="sentence" xml:id="B05239-001-a-0070">.</pc>
       <w lemma="1644." pos="crd" xml:id="B05239-001-a-0080">1644.</w>
       <pc unit="sentence" xml:id="B05239-001-a-0090"/>
      </date>
     </dateline>
    </opener>
    <p xml:id="B05239-e10070">
     <w lemma="the" pos="d" rend="decorinit" xml:id="B05239-001-a-0100">THe</w>
     <w lemma="committee" pos="n1" xml:id="B05239-001-a-0110">Committee</w>
     <w lemma="of" pos="acp" xml:id="B05239-001-a-0120">of</w>
     <w lemma="estate" pos="n2" xml:id="B05239-001-a-0130">Estates</w>
     <pc xml:id="B05239-001-a-0140">,</pc>
     <w lemma="take" pos="vvg" xml:id="B05239-001-a-0150">Taking</w>
     <w lemma="to" pos="acp" xml:id="B05239-001-a-0160">to</w>
     <w lemma="their" pos="po" xml:id="B05239-001-a-0170">their</w>
     <w lemma="consideration" pos="n1" xml:id="B05239-001-a-0180">consideration</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-0190">the</w>
     <w lemma="present" pos="j" xml:id="B05239-001-a-0200">present</w>
     <w lemma="condition" pos="n1" xml:id="B05239-001-a-0210">condition</w>
     <w lemma="&amp;" pos="cc" xml:id="B05239-001-a-0220">&amp;</w>
     <w lemma="estate" pos="n1" xml:id="B05239-001-a-0230">estate</w>
     <w lemma="of" pos="acp" xml:id="B05239-001-a-0240">of</w>
     <w lemma="this" pos="d" xml:id="B05239-001-a-0250">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05239-001-a-0260">Kingdom</w>
     <pc xml:id="B05239-001-a-0270">,</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-0280">and</w>
     <w lemma="how" pos="crq" xml:id="B05239-001-a-0290">how</w>
     <w lemma="that" pos="cs" xml:id="B05239-001-a-0300">that</w>
     <w lemma="many" pos="d" xml:id="B05239-001-a-0310">many</w>
     <w lemma="of" pos="acp" xml:id="B05239-001-a-0320">of</w>
     <w lemma="his" pos="po" xml:id="B05239-001-a-0330">his</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="B05239-001-a-0340">Majesties</w>
     <w lemma="good" pos="j" xml:id="B05239-001-a-0350">good</w>
     <w lemma="subject" pos="n2" xml:id="B05239-001-a-0360">Subjects</w>
     <w lemma="have" pos="vvb" xml:id="B05239-001-a-0370">have</w>
     <w lemma="be" pos="vvn" xml:id="B05239-001-a-0380">been</w>
     <w lemma="pursue" pos="vvn" xml:id="B05239-001-a-0390">pursued</w>
     <w lemma="by" pos="acp" xml:id="B05239-001-a-0400">by</w>
     <w lemma="some" pos="d" xml:id="B05239-001-a-0410">some</w>
     <w lemma="unnatural" pos="j" reg="unnatural" xml:id="B05239-001-a-0420">unnaturall</w>
     <w lemma="countryman" pos="n2" reg="Countrymen" xml:id="B05239-001-a-0430">Countrey-men</w>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-0440">and</w>
     <w lemma="irish" pos="jnn" xml:id="B05239-001-a-0450">Irish</w>
     <w lemma="rebel" pos="n2" xml:id="B05239-001-a-0460">Rebels</w>
     <pc xml:id="B05239-001-a-0470">;</pc>
     <w lemma="their" pos="po" xml:id="B05239-001-a-0480">Their</w>
     <w lemma="house" pos="n2" xml:id="B05239-001-a-0490">Houses</w>
     <w lemma="burn" pos="vvn" xml:id="B05239-001-a-0500">burnt</w>
     <pc xml:id="B05239-001-a-0510">,</pc>
     <w lemma="their" pos="po" xml:id="B05239-001-a-0520">Their</w>
     <w lemma="land" pos="n2" xml:id="B05239-001-a-0530">Lands</w>
     <w lemma="waste" pos="vvn" xml:id="B05239-001-a-0540">wasted</w>
     <pc xml:id="B05239-001-a-0550">,</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-0560">and</w>
     <pc xml:id="B05239-001-a-0570">,</pc>
     <w lemma="their" pos="po" xml:id="B05239-001-a-0580">Their</w>
     <w lemma="corn" pos="n2" xml:id="B05239-001-a-0590">Corns</w>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-0600">and</w>
     <w lemma="good" pos="n2-j" xml:id="B05239-001-a-0610">Goods</w>
     <w lemma="destroy" pos="vvn" xml:id="B05239-001-a-0620">destroyed</w>
     <pc xml:id="B05239-001-a-0630">;</pc>
     <w lemma="so" pos="av" xml:id="B05239-001-a-0640">so</w>
     <w lemma="as" pos="acp" xml:id="B05239-001-a-0650">as</w>
     <w lemma="they" pos="pns" xml:id="B05239-001-a-0660">they</w>
     <w lemma="have" pos="vvb" xml:id="B05239-001-a-0670">have</w>
     <w lemma="nothing" pos="pix" xml:id="B05239-001-a-0680">nothing</w>
     <w lemma="leave" pos="vvn" xml:id="B05239-001-a-0690">left</w>
     <w lemma="to" pos="prt" xml:id="B05239-001-a-0700">to</w>
     <w lemma="live" pos="vvi" xml:id="B05239-001-a-0710">live</w>
     <w lemma="on" pos="acp" xml:id="B05239-001-a-0720">on</w>
     <w lemma="to" pos="prt" xml:id="B05239-001-a-0730">to</w>
     <w lemma="maintain" pos="vvi" xml:id="B05239-001-a-0740">maintain</w>
     <w lemma="their" pos="po" xml:id="B05239-001-a-0750">their</w>
     <w lemma="wife" pos="n2" xml:id="B05239-001-a-0760">Wives</w>
     <pc xml:id="B05239-001-a-0770">,</pc>
     <w lemma="child" pos="n2" xml:id="B05239-001-a-0780">Children</w>
     <pc xml:id="B05239-001-a-0790">,</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-0800">and</w>
     <w lemma="family" pos="n2" xml:id="B05239-001-a-0810">Families</w>
     <pc unit="sentence" xml:id="B05239-001-a-0820">.</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-0830">And</w>
     <w lemma="find" pos="vvg" xml:id="B05239-001-a-0840">finding</w>
     <w lemma="it" pos="pn" xml:id="B05239-001-a-0850">it</w>
     <w lemma="just" pos="j" xml:id="B05239-001-a-0860">just</w>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-0870">and</w>
     <w lemma="necessary" pos="j" xml:id="B05239-001-a-0880">necessar</w>
     <w lemma="that" pos="cs" xml:id="B05239-001-a-0890">That</w>
     <w lemma="such" pos="d" xml:id="B05239-001-a-0900">such</w>
     <w lemma="as" pos="acp" xml:id="B05239-001-a-0910">as</w>
     <w lemma="have" pos="vvi" xml:id="B05239-001-a-0920">have</w>
     <w lemma="suffer" pos="vvn" xml:id="B05239-001-a-0930">suffered</w>
     <pc xml:id="B05239-001-a-0940">,</pc>
     <w lemma="or" pos="cc" xml:id="B05239-001-a-0950">or</w>
     <w lemma="shall" pos="vmb" xml:id="B05239-001-a-0960">shall</w>
     <w lemma="suffer" pos="vvi" xml:id="B05239-001-a-0970">suffer</w>
     <pc xml:id="B05239-001-a-0980">,</pc>
     <w lemma="for" pos="acp" xml:id="B05239-001-a-0990">for</w>
     <w lemma="their" pos="po" xml:id="B05239-001-a-1000">their</w>
     <w lemma="love" pos="n1" xml:id="B05239-001-a-1010">love</w>
     <w lemma="to" pos="acp" xml:id="B05239-001-a-1020">to</w>
     <w lemma="religion" pos="n1" xml:id="B05239-001-a-1030">Religion</w>
     <pc xml:id="B05239-001-a-1040">,</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-1050">and</w>
     <w lemma="safety" pos="n1" reg="safety" xml:id="B05239-001-a-1060">safetie</w>
     <w lemma="of" pos="acp" xml:id="B05239-001-a-1070">of</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-1080">the</w>
     <w lemma="kingdom" pos="n1" xml:id="B05239-001-a-1090">Kingdom</w>
     <pc xml:id="B05239-001-a-1100">,</pc>
     <w lemma="shall" pos="vmd" xml:id="B05239-001-a-1110">should</w>
     <w lemma="be" pos="vvi" xml:id="B05239-001-a-1120">be</w>
     <w lemma="take" pos="vvn" reg="ta'en" xml:id="B05239-001-a-1130">tane</w>
     <w lemma="notice" pos="n1" xml:id="B05239-001-a-1140">notice</w>
     <w lemma="of" pos="acp" xml:id="B05239-001-a-1150">of</w>
     <w lemma="by" pos="acp" xml:id="B05239-001-a-1160">by</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-1170">the</w>
     <w lemma="public" pos="j" reg="Public" xml:id="B05239-001-a-1180">Publike</w>
     <pc xml:id="B05239-001-a-1190">;</pc>
     <w lemma="the" pos="d" xml:id="B05239-001-a-1200">The</w>
     <w lemma="committee" pos="n1" xml:id="B05239-001-a-1210">Committee</w>
     <w lemma="do" pos="vvz" xml:id="B05239-001-a-1220">doth</w>
     <w lemma="therefore" pos="av" xml:id="B05239-001-a-1230">therefore</w>
     <w lemma="declare" pos="vvi" xml:id="B05239-001-a-1240">declare</w>
     <pc xml:id="B05239-001-a-1250">,</pc>
     <w lemma="that" pos="cs" xml:id="B05239-001-a-1260">That</w>
     <w lemma="every" pos="d" xml:id="B05239-001-a-1270">every</w>
     <w lemma="gentleman" pos="n1" xml:id="B05239-001-a-1280">Gentleman</w>
     <w lemma="that" pos="cs" xml:id="B05239-001-a-1290">that</w>
     <w lemma="have" pos="vvz" xml:id="B05239-001-a-1300">hath</w>
     <w lemma="or" pos="cc" xml:id="B05239-001-a-1310">or</w>
     <w lemma="shall" pos="vmb" xml:id="B05239-001-a-1320">shall</w>
     <w lemma="be" pos="vvi" xml:id="B05239-001-a-1330">be</w>
     <w lemma="spoil" pos="vvn" xml:id="B05239-001-a-1340">spoiled</w>
     <w lemma="by" pos="acp" xml:id="B05239-001-a-1350">by</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-1360">the</w>
     <w lemma="rebel" pos="n2" xml:id="B05239-001-a-1370">Rebels</w>
     <pc xml:id="B05239-001-a-1380">,</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-1390">and</w>
     <w lemma="will" pos="vmb" xml:id="B05239-001-a-1400">will</w>
     <w lemma="be" pos="vvi" xml:id="B05239-001-a-1410">be</w>
     <w lemma="content" pos="j" xml:id="B05239-001-a-1420">content</w>
     <w lemma="to" pos="prt" xml:id="B05239-001-a-1430">to</w>
     <w lemma="take" pos="vvi" xml:id="B05239-001-a-1440">take</w>
     <w lemma="service" pos="n1" xml:id="B05239-001-a-1450">Service</w>
     <pc xml:id="B05239-001-a-1460">,</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-1470">and</w>
     <w lemma="shall" pos="vmb" xml:id="B05239-001-a-1480">shall</w>
     <w lemma="have" pos="vvi" xml:id="B05239-001-a-1490">have</w>
     <w lemma="employment" pos="n1" reg="employment" xml:id="B05239-001-a-1500">imployment</w>
     <w lemma="according" pos="j" xml:id="B05239-001-a-1510">according</w>
     <w lemma="to" pos="acp" xml:id="B05239-001-a-1520">to</w>
     <w lemma="their" pos="po" xml:id="B05239-001-a-1530">their</w>
     <w lemma="several" pos="j" xml:id="B05239-001-a-1540">several</w>
     <w lemma="condition" pos="n2" xml:id="B05239-001-a-1550">conditions</w>
     <w lemma="&amp;" pos="cc" xml:id="B05239-001-a-1560">&amp;</w>
     <w lemma="ability" pos="n2" xml:id="B05239-001-a-1570">abilities</w>
     <pc xml:id="B05239-001-a-1580">:</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-1590">And</w>
     <w lemma="for" pos="acp" xml:id="B05239-001-a-1600">for</w>
     <w lemma="such" pos="d" xml:id="B05239-001-a-1610">such</w>
     <w lemma="as" pos="acp" xml:id="B05239-001-a-1620">as</w>
     <w lemma="be" pos="vvb" xml:id="B05239-001-a-1630">are</w>
     <w lemma="poor" pos="j" xml:id="B05239-001-a-1640">poor</w>
     <w lemma="&amp;" pos="cc" xml:id="B05239-001-a-1650">&amp;</w>
     <w lemma="mean" pos="j" xml:id="B05239-001-a-1660">mean</w>
     <w lemma="man" pos="n2" xml:id="B05239-001-a-1670">men</w>
     <pc xml:id="B05239-001-a-1680">,</pc>
     <w lemma="they" pos="pns" xml:id="B05239-001-a-1690">they</w>
     <w lemma="shall" pos="vmb" reg="shall" xml:id="B05239-001-a-1700">shal</w>
     <w lemma="be" pos="vvi" xml:id="B05239-001-a-1710">be</w>
     <w lemma="take" pos="vvn" reg="ta'en" xml:id="B05239-001-a-1720">tane</w>
     <w lemma="on" pos="acp" xml:id="B05239-001-a-1730">on</w>
     <w lemma="in" pos="acp" xml:id="B05239-001-a-1740">in</w>
     <w lemma="service" pos="n1" xml:id="B05239-001-a-1750">Service</w>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-1760">and</w>
     <w lemma="maintain" pos="vvn" xml:id="B05239-001-a-1770">maintained</w>
     <w lemma="in" pos="acp" xml:id="B05239-001-a-1780">in</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-1790">the</w>
     <w lemma="army" pos="n1" reg="Army" xml:id="B05239-001-a-1800">Armie</w>
     <pc xml:id="B05239-001-a-1810">:</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-1820">And</w>
     <w lemma="for" pos="acp" xml:id="B05239-001-a-1830">for</w>
     <w lemma="woman" pos="n2" xml:id="B05239-001-a-1840">women</w>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-1850">and</w>
     <w lemma="child" pos="n2" xml:id="B05239-001-a-1860">children</w>
     <pc xml:id="B05239-001-a-1870">,</pc>
     <w lemma="course" pos="n1" xml:id="B05239-001-a-1880">course</w>
     <w lemma="shall" pos="vmb" xml:id="B05239-001-a-1890">shall</w>
     <w lemma="be" pos="vvi" xml:id="B05239-001-a-1900">be</w>
     <w lemma="take" pos="vvn" reg="ta'en" xml:id="B05239-001-a-1910">tane</w>
     <w lemma="how" pos="crq" xml:id="B05239-001-a-1920">how</w>
     <w lemma="they" pos="pns" xml:id="B05239-001-a-1930">they</w>
     <w lemma="may" pos="vmb" xml:id="B05239-001-a-1940">may</w>
     <w lemma="be" pos="vvi" xml:id="B05239-001-a-1950">be</w>
     <w lemma="supply" pos="vvn" xml:id="B05239-001-a-1960">supplied</w>
     <pc unit="sentence" xml:id="B05239-001-a-1970">.</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-1980">And</w>
     <w lemma="how" pos="crq" xml:id="B05239-001-a-1990">how</w>
     <w lemma="soon" pos="av" xml:id="B05239-001-a-2000">soon</w>
     <w lemma="it" pos="pn" xml:id="B05239-001-a-2010">it</w>
     <w lemma="shall" pos="vmb" xml:id="B05239-001-a-2020">shall</w>
     <w lemma="please" pos="vvi" xml:id="B05239-001-a-2030">please</w>
     <w lemma="God" pos="nn1" xml:id="B05239-001-a-2040">God</w>
     <w lemma="to" pos="prt" xml:id="B05239-001-a-2050">to</w>
     <w lemma="put" pos="vvi" xml:id="B05239-001-a-2060">put</w>
     <w lemma="a" pos="d" xml:id="B05239-001-a-2070">an</w>
     <w lemma="end" pos="n1" xml:id="B05239-001-a-2080">end</w>
     <w lemma="to" pos="acp" xml:id="B05239-001-a-2090">to</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-2100">the</w>
     <w lemma="trouble" pos="n2" xml:id="B05239-001-a-2110">troubles</w>
     <w lemma="of" pos="acp" xml:id="B05239-001-a-2120">of</w>
     <w lemma="this" pos="d" xml:id="B05239-001-a-2130">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05239-001-a-2140">Kingdom</w>
     <pc xml:id="B05239-001-a-2150">,</pc>
     <w lemma="then" pos="av" xml:id="B05239-001-a-2160">Then</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-2170">the</w>
     <w lemma="committee" pos="n1" xml:id="B05239-001-a-2180">Committee</w>
     <w lemma="will" pos="vmb" xml:id="B05239-001-a-2190">will</w>
     <w lemma="take" pos="vvi" xml:id="B05239-001-a-2200">take</w>
     <w lemma="their" pos="po" xml:id="B05239-001-a-2210">their</w>
     <w lemma="several" pos="n1" reg="several" xml:id="B05239-001-a-2220">severell</w>
     <w lemma="loss" pos="n2" xml:id="B05239-001-a-2230">losses</w>
     <w lemma="into" pos="acp" xml:id="B05239-001-a-2240">into</w>
     <w lemma="consideration" pos="n1" xml:id="B05239-001-a-2250">consideration</w>
     <pc xml:id="B05239-001-a-2260">;</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-2270">and</w>
     <w lemma="according" pos="j" xml:id="B05239-001-a-2280">according</w>
     <w lemma="to" pos="acp" xml:id="B05239-001-a-2290">to</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-2300">the</w>
     <w lemma="deserve" pos="n1-vg" xml:id="B05239-001-a-2310">deserving</w>
     <w lemma="of" pos="acp" xml:id="B05239-001-a-2320">of</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-2330">the</w>
     <w lemma="party" pos="n2" xml:id="B05239-001-a-2340">parties</w>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-2350">and</w>
     <w lemma="n/a" pos="fla" xml:id="B05239-001-a-2360">prejudices</w>
     <w lemma="sustain" pos="vvn" xml:id="B05239-001-a-2370">sustained</w>
     <w lemma="by" pos="acp" xml:id="B05239-001-a-2380">by</w>
     <w lemma="they" pos="pno" xml:id="B05239-001-a-2390">them</w>
     <pc xml:id="B05239-001-a-2400">,</pc>
     <w lemma="be" pos="vvb" xml:id="B05239-001-a-2410">be</w>
     <w lemma="careful" pos="j" reg="careful" xml:id="B05239-001-a-2420">carefull</w>
     <w lemma="that" pos="cs" xml:id="B05239-001-a-2430">that</w>
     <w lemma="they" pos="pns" xml:id="B05239-001-a-2440">they</w>
     <w lemma="be" pos="vvb" xml:id="B05239-001-a-2450">be</w>
     <w lemma="supply" pos="vvn" xml:id="B05239-001-a-2460">supplied</w>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-2470">and</w>
     <w lemma="repair" pos="vvn" xml:id="B05239-001-a-2480">repaired</w>
     <w lemma="of" pos="acp" xml:id="B05239-001-a-2490">of</w>
     <w lemma="their" pos="po" xml:id="B05239-001-a-2500">their</w>
     <w lemma="loss" pos="n2" xml:id="B05239-001-a-2510">losses</w>
     <pc xml:id="B05239-001-a-2520">;</pc>
     <w lemma="as" pos="acp" xml:id="B05239-001-a-2530">as</w>
     <w lemma="may" pos="vmb" xml:id="B05239-001-a-2540">may</w>
     <w lemma="best" pos="avs-j" xml:id="B05239-001-a-2550">best</w>
     <w lemma="witness" pos="vvi" reg="witness" xml:id="B05239-001-a-2560">witnesse</w>
     <w lemma="that" pos="cs" xml:id="B05239-001-a-2570">that</w>
     <w lemma="this" pos="d" xml:id="B05239-001-a-2580">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05239-001-a-2590">Kingdom</w>
     <w lemma="acknowledge" pos="vvi" xml:id="B05239-001-a-2600">acknowledge</w>
     <w lemma="themselves" pos="pr" xml:id="B05239-001-a-2610">themselves</w>
     <w lemma="bind" pos="vvn" xml:id="B05239-001-a-2620">bound</w>
     <w lemma="in" pos="acp" xml:id="B05239-001-a-2630">in</w>
     <w lemma="duty" pos="n1" reg="duty" xml:id="B05239-001-a-2640">dutie</w>
     <pc xml:id="B05239-001-a-2650">,</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-2660">and</w>
     <w lemma="by" pos="acp" xml:id="B05239-001-a-2670">by</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-2680">the</w>
     <w lemma="covenant" pos="n1" rend="hi" xml:id="B05239-001-a-2690">Covenant</w>
     <pc xml:id="B05239-001-a-2700">,</pc>
     <w lemma="to" pos="prt" xml:id="B05239-001-a-2710">to</w>
     <w lemma="be" pos="vvi" xml:id="B05239-001-a-2720">be</w>
     <w lemma="sensible" pos="j" xml:id="B05239-001-a-2730">sensible</w>
     <w lemma="of" pos="acp" xml:id="B05239-001-a-2740">of</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-2750">the</w>
     <w lemma="suffering" pos="n2" xml:id="B05239-001-a-2760">sufferings</w>
     <w lemma="of" pos="acp" xml:id="B05239-001-a-2770">of</w>
     <w lemma="their" pos="po" xml:id="B05239-001-a-2780">their</w>
     <w lemma="brethren" pos="n2" xml:id="B05239-001-a-2790">Brethren</w>
     <w lemma="in" pos="acp" xml:id="B05239-001-a-2800">in</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-2810">the</w>
     <w lemma="defence" pos="n1" xml:id="B05239-001-a-2820">defence</w>
     <w lemma="thereof" pos="av" xml:id="B05239-001-a-2830">thereof</w>
     <pc unit="sentence" xml:id="B05239-001-a-2840">.</pc>
     <w lemma="and" pos="cc" xml:id="B05239-001-a-2850">And</w>
     <w lemma="ordain" pos="vvz" xml:id="B05239-001-a-2860">ordains</w>
     <w lemma="publication" pos="n1" xml:id="B05239-001-a-2870">Publication</w>
     <w lemma="to" pos="prt" xml:id="B05239-001-a-2880">to</w>
     <w lemma="be" pos="vvi" xml:id="B05239-001-a-2890">be</w>
     <w lemma="make" pos="vvn" xml:id="B05239-001-a-2900">made</w>
     <w lemma="hereof" pos="av" xml:id="B05239-001-a-2910">hereof</w>
     <w lemma="by" pos="acp" xml:id="B05239-001-a-2920">by</w>
     <w lemma="open" pos="j" xml:id="B05239-001-a-2930">open</w>
     <w lemma="proclamation" pos="n1" xml:id="B05239-001-a-2940">Proclamation</w>
     <w lemma="at" pos="acp" xml:id="B05239-001-a-2950">at</w>
     <w lemma="all" pos="d" xml:id="B05239-001-a-2960">all</w>
     <w lemma="place" pos="n2" xml:id="B05239-001-a-2970">places</w>
     <w lemma="needful" pos="j" reg="needful" xml:id="B05239-001-a-2980">needfull</w>
     <pc unit="sentence" xml:id="B05239-001-a-2990">.</pc>
    </p>
    <closer xml:id="B05239-e10090">
     <signed xml:id="B05239-e10100">
      <w lemma="arch." pos="ab" xml:id="B05239-001-a-3000">Arch.</w>
      <w lemma="primrose" pos="n1" reg="Primrose" xml:id="B05239-001-a-3010">Primerose</w>
      <pc unit="sentence" xml:id="B05239-001-a-3020">.</pc>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="B05239-e10110">
   <div type="colophon" xml:id="B05239-e10120">
    <p xml:id="B05239-e10130">
     <w lemma="print" pos="j-vn" xml:id="B05239-001-a-3030">Printed</w>
     <w lemma="at" pos="acp" xml:id="B05239-001-a-3040">at</w>
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05239-001-a-3050">Edinburgh</w>
     <w lemma="by" pos="acp" xml:id="B05239-001-a-3060">by</w>
     <hi xml:id="B05239-e10150">
      <w lemma="Evan" pos="nn1" xml:id="B05239-001-a-3070">Evan</w>
      <w lemma="Tyler" pos="nn1" xml:id="B05239-001-a-3080">Tyler</w>
     </hi>
     <pc rend="follows-hi" xml:id="B05239-001-a-3090">,</pc>
     <w lemma="printer" pos="n1" xml:id="B05239-001-a-3100">Printer</w>
     <w lemma="to" pos="acp" xml:id="B05239-001-a-3110">to</w>
     <w lemma="the" pos="d" xml:id="B05239-001-a-3120">the</w>
     <w lemma="king" pos="n2" xml:id="B05239-001-a-3130">Kings</w>
     <w lemma="most" pos="avs-d" xml:id="B05239-001-a-3140">most</w>
     <w lemma="excellent" pos="j" xml:id="B05239-001-a-3150">Excellent</w>
     <w lemma="majesty" pos="n1" reg="Majesty" xml:id="B05239-001-a-3160">Majestie</w>
     <pc unit="sentence" xml:id="B05239-001-a-3170">.</pc>
     <w lemma="1644." pos="crd" rend="hi" xml:id="B05239-001-a-3180">1644.</w>
     <pc unit="sentence" xml:id="B05239-001-a-3190"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
