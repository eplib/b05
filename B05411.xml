<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B05411">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A proclamation, adjourning the Parliament from the eighth of April, to the ninth of September. 1684</title>
    <author>Scotland. Privy Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B05411 of text R183291 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing S1544). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B05411</idno>
    <idno type="STC">Wing S1544</idno>
    <idno type="STC">ESTC R183291</idno>
    <idno type="EEBO-CITATION">52612279</idno>
    <idno type="OCLC">ocm 52612279</idno>
    <idno type="VID">179582</idno>
    <idno type="PROQUESTGOID">2240873550</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. B05411)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179582)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2793:54)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A proclamation, adjourning the Parliament from the eighth of April, to the ninth of September. 1684</title>
      <author>Scotland. Privy Council.</author>
      <author>Scotland. Sovereign (1649-1685 : Charles II)</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by the heir of Andrew Anderson, Printer to his most sacred Majesty,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>Anno Dom. 1684.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Royal arms at head of text; initial letter.</note>
      <note>Dated: Given under Our Signet at Edinburgh, the twenty seventh day of March, one thousand, six hundreth and eighty four years, and of Our Reign the thirtieth six year.</note>
      <note>Signed: Will. Paterson, Cls. Sti. Concilii.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Scotland. -- Parliament. -- Early works to 1800.</term>
     <term>Scotland -- Politics and government -- 1689-1745 -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A proclamation, adjourning the Parliament from the eighth of April, to the ninth of September. 1684.</ep:title>
    <ep:author>Scotland. Privy Council</ep:author>
    <ep:publicationYear>1684</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>488</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-10</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-12</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2009-03</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-03</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B05411-e10010">
  <body xml:id="B05411-e10020">
   <div type="royal_proclamation" xml:id="B05411-e10030">
    <pb facs="tcp:179582:1" rend="simple:additions" xml:id="B05411-001-a"/>
    <head xml:id="B05411-e10040">
     <w lemma="a" pos="d" xml:id="B05411-001-a-0010">A</w>
     <w lemma="proclamation" pos="n1" xml:id="B05411-001-a-0020">PROCLAMATION</w>
     <pc xml:id="B05411-001-a-0030">,</pc>
     <w lemma="adjourn" pos="vvg" xml:id="B05411-001-a-0040">Adjourning</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-0050">the</w>
     <w lemma="parliament" pos="n1" xml:id="B05411-001-a-0060">Parliament</w>
     <w lemma="from" pos="acp" xml:id="B05411-001-a-0070">from</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-0080">the</w>
     <w lemma="eight" pos="ord" xml:id="B05411-001-a-0090">eighth</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-0100">of</w>
     <w lemma="April" pos="nn1" rend="hi" xml:id="B05411-001-a-0110">April</w>
     <pc xml:id="B05411-001-a-0120">,</pc>
     <w lemma="to" pos="acp" xml:id="B05411-001-a-0130">to</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-0140">the</w>
     <w lemma="nine" pos="ord" xml:id="B05411-001-a-0150">ninth</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-0160">of</w>
     <w lemma="September" pos="nn1" rend="hi" xml:id="B05411-001-a-0170">September</w>
     <pc unit="sentence" xml:id="B05411-001-a-0180">.</pc>
     <w lemma="1684." pos="crd" xml:id="B05411-001-a-0190">1684.</w>
     <pc unit="sentence" xml:id="B05411-001-a-0200"/>
    </head>
    <opener xml:id="B05411-e10070">
     <signed xml:id="B05411-e10080">
      <w lemma="charles" pos="nn1" rend="hi" xml:id="B05411-001-a-0210">CHARLES</w>
      <pc xml:id="B05411-001-a-0220">,</pc>
      <w lemma="by" pos="acp" xml:id="B05411-001-a-0230">By</w>
      <w lemma="the" pos="d" xml:id="B05411-001-a-0240">the</w>
      <w lemma="grace" pos="n1" xml:id="B05411-001-a-0250">Grace</w>
      <w lemma="of" pos="acp" xml:id="B05411-001-a-0260">of</w>
      <w lemma="GOD" pos="nn1" xml:id="B05411-001-a-0270">GOD</w>
      <pc xml:id="B05411-001-a-0280">,</pc>
      <w lemma="king" pos="n1" xml:id="B05411-001-a-0290">King</w>
      <w lemma="of" pos="acp" xml:id="B05411-001-a-0300">of</w>
      <w lemma="great" pos="j" xml:id="B05411-001-a-0310">Great</w>
      <hi xml:id="B05411-e10100">
       <w lemma="Britain" pos="nn1" xml:id="B05411-001-a-0320">Britain</w>
       <pc xml:id="B05411-001-a-0330">,</pc>
       <w lemma="France" pos="nn1" xml:id="B05411-001-a-0340">France</w>
      </hi>
      <w lemma="and" pos="cc" xml:id="B05411-001-a-0350">and</w>
      <w lemma="Ireland" pos="nn1" rend="hi" xml:id="B05411-001-a-0360">Ireland</w>
      <pc xml:id="B05411-001-a-0370">,</pc>
      <w lemma="defender" pos="n1" xml:id="B05411-001-a-0380">Defender</w>
      <w lemma="of" pos="acp" xml:id="B05411-001-a-0390">of</w>
      <w lemma="the" pos="d" xml:id="B05411-001-a-0400">the</w>
      <w lemma="faith" pos="n1" xml:id="B05411-001-a-0410">Faith</w>
      <pc xml:id="B05411-001-a-0420">;</pc>
     </signed>
     <salute xml:id="B05411-e10120">
      <w lemma="to" pos="acp" xml:id="B05411-001-a-0430">To</w>
      <w lemma="our" pos="po" xml:id="B05411-001-a-0440">Our</w>
      <w lemma="lion" pos="n1" reg="Lion" xml:id="B05411-001-a-0450">Lyon</w>
      <w lemma="king" pos="n1" xml:id="B05411-001-a-0460">King</w>
      <w lemma="at" pos="acp" xml:id="B05411-001-a-0470">at</w>
      <w lemma="arm" pos="n2" xml:id="B05411-001-a-0480">Arms</w>
      <pc xml:id="B05411-001-a-0490">,</pc>
      <w lemma="and" pos="cc" xml:id="B05411-001-a-0500">and</w>
      <w lemma="his" pos="po" xml:id="B05411-001-a-0510">his</w>
      <w lemma="brethren" pos="n2" xml:id="B05411-001-a-0520">Brethren</w>
      <w lemma="herald" pos="n2" reg="Heralds" xml:id="B05411-001-a-0530">Heraulds</w>
      <pc xml:id="B05411-001-a-0540">,</pc>
      <w lemma="macer" pos="n2" xml:id="B05411-001-a-0550">Macers</w>
      <w lemma="of" pos="acp" xml:id="B05411-001-a-0560">of</w>
      <w lemma="our" pos="po" xml:id="B05411-001-a-0570">Our</w>
      <w lemma="privy" pos="j" xml:id="B05411-001-a-0580">Privy</w>
      <w lemma="council" pos="n1" xml:id="B05411-001-a-0590">Council</w>
      <pc xml:id="B05411-001-a-0600">,</pc>
      <w lemma="pursuivant" pos="n2" reg="Pursuivants" xml:id="B05411-001-a-0610">Pursevants</w>
      <pc xml:id="B05411-001-a-0620">,</pc>
      <w lemma="and" pos="cc" xml:id="B05411-001-a-0630">and</w>
      <w lemma="messenger" pos="n2" xml:id="B05411-001-a-0640">Messengers</w>
      <w lemma="at" pos="acp" xml:id="B05411-001-a-0650">at</w>
      <w lemma="arm" pos="n2" xml:id="B05411-001-a-0660">Arms</w>
      <pc xml:id="B05411-001-a-0670">:</pc>
      <w lemma="our" pos="po" xml:id="B05411-001-a-0680">Our</w>
      <w lemma="sheriff" pos="n2" xml:id="B05411-001-a-0690">Sheriffs</w>
      <w lemma="in" pos="acp" xml:id="B05411-001-a-0700">in</w>
      <w lemma="that" pos="d" xml:id="B05411-001-a-0710">that</w>
      <w lemma="part" pos="n1" xml:id="B05411-001-a-0720">part</w>
      <pc xml:id="B05411-001-a-0730">,</pc>
      <w lemma="conjunct" pos="av-j" xml:id="B05411-001-a-0740">conjunctly</w>
      <w lemma="and" pos="cc" xml:id="B05411-001-a-0750">and</w>
      <w lemma="several" pos="av-j" xml:id="B05411-001-a-0760">severally</w>
      <pc xml:id="B05411-001-a-0770">,</pc>
      <w lemma="special" pos="av-j" xml:id="B05411-001-a-0780">specially</w>
      <w lemma="constitute" pos="vvi" xml:id="B05411-001-a-0790">constitute</w>
      <pc xml:id="B05411-001-a-0800">,</pc>
      <w lemma="greeting" pos="n1" xml:id="B05411-001-a-0810">Greeting</w>
      <pc xml:id="B05411-001-a-0820">:</pc>
     </salute>
    </opener>
    <p xml:id="B05411-e10130">
     <w lemma="whereas" pos="cs" xml:id="B05411-001-a-0830">Whereas</w>
     <pc xml:id="B05411-001-a-0840">,</pc>
     <w lemma="we" pos="pns" xml:id="B05411-001-a-0850">We</w>
     <pc xml:id="B05411-001-a-0860">,</pc>
     <w lemma="by" pos="acp" xml:id="B05411-001-a-0870">by</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-0880">Our</w>
     <w lemma="royal" pos="j" xml:id="B05411-001-a-0890">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="B05411-001-a-0900">Proclamation</w>
     <pc xml:id="B05411-001-a-0910">,</pc>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-0920">of</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-0930">the</w>
     <w lemma="five" pos="ord" xml:id="B05411-001-a-0940">fifth</w>
     <w lemma="day" pos="n1" xml:id="B05411-001-a-0950">day</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-0960">of</w>
     <w lemma="November" pos="nn1" rend="hi" xml:id="B05411-001-a-0970">November</w>
     <w lemma="last" pos="ord" xml:id="B05411-001-a-0980">last</w>
     <pc xml:id="B05411-001-a-0990">,</pc>
     <w lemma="adjourn" pos="vvn" xml:id="B05411-001-a-1000">Adjourned</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-1010">the</w>
     <w lemma="parliament" pos="n1" xml:id="B05411-001-a-1020">Parliament</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-1030">of</w>
     <w lemma="this" pos="d" xml:id="B05411-001-a-1040">this</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-1050">Our</w>
     <w lemma="ancient" pos="j" xml:id="B05411-001-a-1060">Ancient</w>
     <w lemma="kingdom" pos="n1" xml:id="B05411-001-a-1070">Kingdom</w>
     <pc xml:id="B05411-001-a-1080">,</pc>
     <w lemma="to" pos="prt" xml:id="B05411-001-a-1090">to</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-1100">the</w>
     <w lemma="eight" pos="ord" xml:id="B05411-001-a-1110">eighth</w>
     <w lemma="day" pos="n1" xml:id="B05411-001-a-1120">day</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-1130">of</w>
     <w lemma="April" pos="nn1" rend="hi" xml:id="B05411-001-a-1140">April</w>
     <w lemma="thereafter" pos="av" xml:id="B05411-001-a-1150">thereafter</w>
     <pc xml:id="B05411-001-a-1160">,</pc>
     <w lemma="next" pos="ord" xml:id="B05411-001-a-1170">next</w>
     <w lemma="ensue" pos="vvg" xml:id="B05411-001-a-1180">ensuing</w>
     <pc xml:id="B05411-001-a-1190">,</pc>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-1200">and</w>
     <w lemma="see" pos="vvg" reg="seeing" xml:id="B05411-001-a-1210">seing</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-1220">the</w>
     <w lemma="present" pos="j" xml:id="B05411-001-a-1230">present</w>
     <w lemma="state" pos="n1" xml:id="B05411-001-a-1240">state</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-1250">of</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-1260">Our</w>
     <w lemma="affair" pos="n2" xml:id="B05411-001-a-1270">Affairs</w>
     <w lemma="do" pos="vvb" xml:id="B05411-001-a-1280">do</w>
     <w lemma="not" pos="xx" xml:id="B05411-001-a-1290">not</w>
     <w lemma="yet" pos="av" xml:id="B05411-001-a-1300">yet</w>
     <w lemma="require" pos="vvi" xml:id="B05411-001-a-1310">require</w>
     <w lemma="their" pos="po" xml:id="B05411-001-a-1320">their</w>
     <w lemma="meeting" pos="n1" xml:id="B05411-001-a-1330">meeting</w>
     <w lemma="so" pos="av" xml:id="B05411-001-a-1340">so</w>
     <w lemma="soon" pos="av" xml:id="B05411-001-a-1350">soon</w>
     <pc xml:id="B05411-001-a-1360">,</pc>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-1370">and</w>
     <w lemma="we" pos="pns" xml:id="B05411-001-a-1380">We</w>
     <w lemma="be" pos="vvg" xml:id="B05411-001-a-1390">being</w>
     <w lemma="unwilling" pos="j" xml:id="B05411-001-a-1400">unwilling</w>
     <pc xml:id="B05411-001-a-1410">,</pc>
     <w lemma="that" pos="cs" xml:id="B05411-001-a-1420">that</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-1430">the</w>
     <w lemma="member" pos="n2" xml:id="B05411-001-a-1440">Members</w>
     <w lemma="shall" pos="vmd" xml:id="B05411-001-a-1450">should</w>
     <w lemma="be" pos="vvi" xml:id="B05411-001-a-1460">be</w>
     <w lemma="put" pos="vvn" xml:id="B05411-001-a-1470">put</w>
     <w lemma="to" pos="acp" xml:id="B05411-001-a-1480">to</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-1490">the</w>
     <w lemma="trouble" pos="n1" xml:id="B05411-001-a-1500">trouble</w>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-1510">and</w>
     <w lemma="charge" pos="n1" xml:id="B05411-001-a-1520">charge</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-1530">of</w>
     <w lemma="meet" pos="vvg" xml:id="B05411-001-a-1540">Meeting</w>
     <w lemma="at" pos="acp" xml:id="B05411-001-a-1550">at</w>
     <w lemma="that" pos="d" xml:id="B05411-001-a-1560">that</w>
     <w lemma="time" pos="n1" xml:id="B05411-001-a-1570">time</w>
     <pc xml:id="B05411-001-a-1580">,</pc>
     <w lemma="have" pos="vvb" xml:id="B05411-001-a-1590">Have</w>
     <w lemma="therefore" pos="av" xml:id="B05411-001-a-1600">therefore</w>
     <pc xml:id="B05411-001-a-1610">,</pc>
     <w lemma="with" pos="acp" xml:id="B05411-001-a-1620">with</w>
     <w lemma="advice" pos="n1" xml:id="B05411-001-a-1630">advice</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-1640">of</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-1650">Our</w>
     <w lemma="privy" pos="j" xml:id="B05411-001-a-1660">Privy</w>
     <w lemma="council" pos="n1" xml:id="B05411-001-a-1670">Council</w>
     <pc xml:id="B05411-001-a-1680">,</pc>
     <w lemma="think" pos="vvd" xml:id="B05411-001-a-1690">thought</w>
     <w lemma="fit" pos="j" xml:id="B05411-001-a-1700">fit</w>
     <w lemma="by" pos="acp" xml:id="B05411-001-a-1710">by</w>
     <w lemma="this" pos="d" xml:id="B05411-001-a-1720">this</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-1730">Our</w>
     <w lemma="royal" pos="j" xml:id="B05411-001-a-1740">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="B05411-001-a-1750">Proclamation</w>
     <pc xml:id="B05411-001-a-1760">,</pc>
     <w lemma="to" pos="prt" xml:id="B05411-001-a-1770">to</w>
     <w lemma="continue" pos="vvi" xml:id="B05411-001-a-1780">continue</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-1790">the</w>
     <w lemma="adjournment" pos="n1" xml:id="B05411-001-a-1800">Adjournment</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-1810">of</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-1820">Our</w>
     <w lemma="say" pos="j-vn" xml:id="B05411-001-a-1830">said</w>
     <w lemma="parliament" pos="n1" xml:id="B05411-001-a-1840">Parliament</w>
     <w lemma="from" pos="acp" xml:id="B05411-001-a-1850">from</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-1860">the</w>
     <w lemma="say" pos="vvd" xml:id="B05411-001-a-1870">said</w>
     <w lemma="eight" pos="ord" xml:id="B05411-001-a-1880">eighth</w>
     <w lemma="day" pos="n1" xml:id="B05411-001-a-1890">day</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-1900">of</w>
     <w lemma="April" pos="nn1" rend="hi" xml:id="B05411-001-a-1910">April</w>
     <pc xml:id="B05411-001-a-1920">,</pc>
     <w lemma="to" pos="acp" xml:id="B05411-001-a-1930">to</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-1940">the</w>
     <w lemma="nine" pos="ord" xml:id="B05411-001-a-1950">ninth</w>
     <w lemma="day" pos="n1" xml:id="B05411-001-a-1960">day</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-1970">of</w>
     <w lemma="September" pos="nn1" rend="hi" xml:id="B05411-001-a-1980">September</w>
     <w lemma="next" pos="ord" xml:id="B05411-001-a-1990">next</w>
     <w lemma="ensue" pos="vvg" xml:id="B05411-001-a-2000">ensuing</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-2010">the</w>
     <w lemma="date" pos="n1" xml:id="B05411-001-a-2020">Date</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-2030">of</w>
     <w lemma="these" pos="d" xml:id="B05411-001-a-2040">these</w>
     <w lemma="present" pos="n2" xml:id="B05411-001-a-2050">Presents</w>
     <pc xml:id="B05411-001-a-2060">:</pc>
     <w lemma="we" pos="pns" xml:id="B05411-001-a-2070">We</w>
     <w lemma="be" pos="vvg" xml:id="B05411-001-a-2080">being</w>
     <w lemma="always" pos="av" reg="always" xml:id="B05411-001-a-2090">alwayes</w>
     <w lemma="resolve" pos="vvn" xml:id="B05411-001-a-2100">resolved</w>
     <w lemma="to" pos="prt" xml:id="B05411-001-a-2110">to</w>
     <w lemma="continue" pos="vvi" xml:id="B05411-001-a-2120">continue</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-2130">Our</w>
     <w lemma="dear" pos="js" xml:id="B05411-001-a-2140">dearest</w>
     <w lemma="royal" pos="j" xml:id="B05411-001-a-2150">Royal</w>
     <w lemma="brother" pos="n1" xml:id="B05411-001-a-2160">Brother</w>
     <w lemma="to" pos="prt" xml:id="B05411-001-a-2170">to</w>
     <w lemma="be" pos="vvi" xml:id="B05411-001-a-2180">be</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-2190">Our</w>
     <w lemma="high" pos="j" xml:id="B05411-001-a-2200">High</w>
     <w lemma="commissioner" pos="n1" xml:id="B05411-001-a-2210">Commissioner</w>
     <w lemma="in" pos="acp" xml:id="B05411-001-a-2220">in</w>
     <w lemma="all" pos="d" xml:id="B05411-001-a-2230">all</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-2240">the</w>
     <w lemma="session" pos="n2" xml:id="B05411-001-a-2250">Sessions</w>
     <pc xml:id="B05411-001-a-2260">,</pc>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-2270">and</w>
     <w lemma="until" pos="acp" xml:id="B05411-001-a-2280">until</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-2290">the</w>
     <w lemma="end" pos="n1" xml:id="B05411-001-a-2300">end</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-2310">of</w>
     <w lemma="that" pos="d" xml:id="B05411-001-a-2320">that</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-2330">Our</w>
     <w lemma="parliament" pos="n1" xml:id="B05411-001-a-2340">Parliament</w>
     <pc xml:id="B05411-001-a-2350">:</pc>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-2360">And</w>
     <w lemma="to" pos="acp" xml:id="B05411-001-a-2370">to</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-2380">the</w>
     <w lemma="effect" pos="n1" xml:id="B05411-001-a-2390">effect</w>
     <pc xml:id="B05411-001-a-2400">,</pc>
     <w lemma="all" pos="d" xml:id="B05411-001-a-2410">all</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-2420">the</w>
     <w lemma="member" pos="n2" xml:id="B05411-001-a-2430">Members</w>
     <w lemma="thereof" pos="av" xml:id="B05411-001-a-2440">thereof</w>
     <w lemma="may" pos="vmb" xml:id="B05411-001-a-2450">may</w>
     <w lemma="attend" pos="vvi" xml:id="B05411-001-a-2460">attend</w>
     <w lemma="that" pos="d" xml:id="B05411-001-a-2470">that</w>
     <w lemma="day" pos="n1" xml:id="B05411-001-a-2480">day</w>
     <pc xml:id="B05411-001-a-2490">,</pc>
     <w lemma="in" pos="acp" xml:id="B05411-001-a-2500">in</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-2510">the</w>
     <w lemma="usual" pos="j" xml:id="B05411-001-a-2520">usual</w>
     <w lemma="way" pos="n1" xml:id="B05411-001-a-2530">way</w>
     <pc xml:id="B05411-001-a-2540">,</pc>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-2550">and</w>
     <w lemma="upon" pos="acp" xml:id="B05411-001-a-2560">upon</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-2570">the</w>
     <w lemma="accustom" pos="j-vn" xml:id="B05411-001-a-2580">accustomed</w>
     <w lemma="certification" pos="n2" xml:id="B05411-001-a-2590">Certifications</w>
     <pc xml:id="B05411-001-a-2600">,</pc>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-2610">and</w>
     <w lemma="that" pos="cs" xml:id="B05411-001-a-2620">that</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-2630">Our</w>
     <w lemma="pleasure" pos="n1" xml:id="B05411-001-a-2640">Pleasure</w>
     <w lemma="may" pos="vmb" xml:id="B05411-001-a-2650">may</w>
     <w lemma="be" pos="vvi" xml:id="B05411-001-a-2660">be</w>
     <w lemma="know" pos="vvn" xml:id="B05411-001-a-2670">known</w>
     <w lemma="in" pos="acp" xml:id="B05411-001-a-2680">in</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-2690">the</w>
     <w lemma="premise" pos="n2" xml:id="B05411-001-a-2700">Premises</w>
     <pc xml:id="B05411-001-a-2710">,</pc>
     <w lemma="our" pos="po" xml:id="B05411-001-a-2720">OUR</w>
     <w lemma="will" pos="n1" xml:id="B05411-001-a-2730">WILL</w>
     <w lemma="be" pos="vvz" xml:id="B05411-001-a-2740">IS</w>
     <pc xml:id="B05411-001-a-2750">,</pc>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-2760">and</w>
     <w lemma="we" pos="pns" xml:id="B05411-001-a-2770">We</w>
     <w lemma="charge" pos="vvb" xml:id="B05411-001-a-2780">Charge</w>
     <w lemma="you" pos="pn" xml:id="B05411-001-a-2790">you</w>
     <w lemma="strict" pos="av-j" xml:id="B05411-001-a-2800">strictly</w>
     <pc xml:id="B05411-001-a-2810">,</pc>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-2820">and</w>
     <w lemma="command" pos="vvb" xml:id="B05411-001-a-2830">Command</w>
     <pc xml:id="B05411-001-a-2840">,</pc>
     <w lemma="that" pos="cs" xml:id="B05411-001-a-2850">that</w>
     <w lemma="incontinent" pos="j" xml:id="B05411-001-a-2860">incontinent</w>
     <w lemma="these" pos="d" xml:id="B05411-001-a-2870">these</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-2880">Our</w>
     <w lemma="letter" pos="n2" xml:id="B05411-001-a-2890">Letters</w>
     <w lemma="see" pos="vvn" xml:id="B05411-001-a-2900">seen</w>
     <pc xml:id="B05411-001-a-2910">,</pc>
     <w lemma="you" pos="pn" xml:id="B05411-001-a-2920">ye</w>
     <w lemma="pass" pos="vvb" xml:id="B05411-001-a-2930">pass</w>
     <w lemma="to" pos="acp" xml:id="B05411-001-a-2940">to</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-2950">the</w>
     <w lemma="mercat" pos="n1" xml:id="B05411-001-a-2960">Mercat</w>
     <w lemma="cross" pos="n1" xml:id="B05411-001-a-2970">Cross</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-2980">of</w>
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05411-001-a-2990">Edinburgh</w>
     <pc xml:id="B05411-001-a-3000">,</pc>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-3010">and</w>
     <w lemma="n/a" pos="fla" xml:id="B05411-001-a-3020">remanent</w>
     <w lemma="mercat" pos="n1" xml:id="B05411-001-a-3030">Mercat</w>
     <w lemma="cross" pos="n2" xml:id="B05411-001-a-3040">Crosses</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-3050">of</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-3060">the</w>
     <w lemma="burgh" pos="nng1" reg="burgh's" xml:id="B05411-001-a-3070">Burghs</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-3080">of</w>
     <w lemma="this" pos="d" xml:id="B05411-001-a-3090">this</w>
     <w lemma="kingdom" pos="n1" xml:id="B05411-001-a-3100">Kingdom</w>
     <pc xml:id="B05411-001-a-3110">,</pc>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-3120">and</w>
     <w lemma="thereat" pos="av" xml:id="B05411-001-a-3130">thereat</w>
     <pc xml:id="B05411-001-a-3140">,</pc>
     <w lemma="in" pos="acp" xml:id="B05411-001-a-3150">in</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-3160">Our</w>
     <w lemma="name" pos="n1" xml:id="B05411-001-a-3170">Name</w>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-3180">and</w>
     <w lemma="authority" pos="n1" xml:id="B05411-001-a-3190">Authority</w>
     <pc xml:id="B05411-001-a-3200">,</pc>
     <w lemma="by" pos="acp" xml:id="B05411-001-a-3210">by</w>
     <w lemma="open" pos="j" xml:id="B05411-001-a-3220">open</w>
     <w lemma="proclamation" pos="n1" xml:id="B05411-001-a-3230">Proclamation</w>
     <pc xml:id="B05411-001-a-3240">,</pc>
     <w lemma="make" pos="vvb" xml:id="B05411-001-a-3250">make</w>
     <w lemma="publication" pos="n1" xml:id="B05411-001-a-3260">Publication</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-3270">of</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-3280">the</w>
     <w lemma="say" pos="j-vn" xml:id="B05411-001-a-3290">said</w>
     <w lemma="adjournment" pos="n1" xml:id="B05411-001-a-3300">Adjournment</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-3310">of</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-3320">Our</w>
     <w lemma="say" pos="j-vn" xml:id="B05411-001-a-3330">said</w>
     <w lemma="parliament" pos="n1" xml:id="B05411-001-a-3340">Parliament</w>
     <pc xml:id="B05411-001-a-3350">,</pc>
     <w lemma="from" pos="acp" xml:id="B05411-001-a-3360">from</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-3370">the</w>
     <w lemma="say" pos="vvd" xml:id="B05411-001-a-3380">said</w>
     <w lemma="eight" pos="ord" xml:id="B05411-001-a-3390">eighth</w>
     <w lemma="day" pos="n1" xml:id="B05411-001-a-3400">day</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-3410">of</w>
     <w lemma="April" pos="nn1" rend="hi" xml:id="B05411-001-a-3420">April</w>
     <pc xml:id="B05411-001-a-3430">,</pc>
     <w lemma="to" pos="acp" xml:id="B05411-001-a-3440">to</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-3450">the</w>
     <w lemma="say" pos="vvd" xml:id="B05411-001-a-3460">said</w>
     <w lemma="nine" pos="ord" xml:id="B05411-001-a-3470">ninth</w>
     <w lemma="day" pos="n1" xml:id="B05411-001-a-3480">day</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-3490">of</w>
     <w lemma="September" pos="nn1" rend="hi" xml:id="B05411-001-a-3500">September</w>
     <pc xml:id="B05411-001-a-3510">,</pc>
     <w lemma="next" pos="ord" xml:id="B05411-001-a-3520">next</w>
     <w lemma="ensue" pos="vvg" xml:id="B05411-001-a-3530">ensuing</w>
     <pc xml:id="B05411-001-a-3540">;</pc>
     <w lemma="require" pos="vvg" xml:id="B05411-001-a-3550">Requiring</w>
     <w lemma="hereby" pos="av" xml:id="B05411-001-a-3560">hereby</w>
     <w lemma="all" pos="d" xml:id="B05411-001-a-3570">all</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-3580">the</w>
     <w lemma="member" pos="n2" xml:id="B05411-001-a-3590">Members</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-3600">of</w>
     <w lemma="that" pos="d" xml:id="B05411-001-a-3610">that</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-3620">Our</w>
     <w lemma="parliament" pos="n1" xml:id="B05411-001-a-3630">Parliament</w>
     <w lemma="to" pos="prt" xml:id="B05411-001-a-3640">to</w>
     <w lemma="attend" pos="vvi" xml:id="B05411-001-a-3650">attend</w>
     <w lemma="that" pos="d" xml:id="B05411-001-a-3660">that</w>
     <w lemma="day" pos="n1" xml:id="B05411-001-a-3670">day</w>
     <pc xml:id="B05411-001-a-3680">,</pc>
     <w lemma="in" pos="acp" xml:id="B05411-001-a-3690">in</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-3700">the</w>
     <w lemma="usual" pos="j" xml:id="B05411-001-a-3710">usual</w>
     <w lemma="way" pos="n1" xml:id="B05411-001-a-3720">way</w>
     <pc xml:id="B05411-001-a-3730">,</pc>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-3740">and</w>
     <w lemma="upon" pos="acp" xml:id="B05411-001-a-3750">upon</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-3760">the</w>
     <w lemma="accustom" pos="j-vn" xml:id="B05411-001-a-3770">accustomed</w>
     <w lemma="certification" pos="n2" xml:id="B05411-001-a-3780">Certifications</w>
     <pc unit="sentence" xml:id="B05411-001-a-3790">.</pc>
     <w lemma="the" pos="d" xml:id="B05411-001-a-3800">The</w>
     <w lemma="which" pos="crq" xml:id="B05411-001-a-3810">which</w>
     <w lemma="to" pos="prt" xml:id="B05411-001-a-3820">to</w>
     <w lemma="do" pos="vvi" xml:id="B05411-001-a-3830">do</w>
     <pc xml:id="B05411-001-a-3840">,</pc>
     <w lemma="we" pos="pns" xml:id="B05411-001-a-3850">We</w>
     <w lemma="commit" pos="vvb" xml:id="B05411-001-a-3860">commit</w>
     <w lemma="to" pos="acp" xml:id="B05411-001-a-3870">to</w>
     <w lemma="you" pos="pn" xml:id="B05411-001-a-3880">you</w>
     <w lemma="conjunct" pos="av-j" xml:id="B05411-001-a-3890">conjunctly</w>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-3900">and</w>
     <w lemma="several" pos="av-j" xml:id="B05411-001-a-3910">severally</w>
     <pc xml:id="B05411-001-a-3920">,</pc>
     <w lemma="our" pos="po" xml:id="B05411-001-a-3930">Our</w>
     <w lemma="full" pos="j" xml:id="B05411-001-a-3940">full</w>
     <w lemma="power" pos="n1" xml:id="B05411-001-a-3950">Power</w>
     <pc xml:id="B05411-001-a-3960">,</pc>
     <w lemma="by" pos="acp" xml:id="B05411-001-a-3970">by</w>
     <w lemma="these" pos="d" xml:id="B05411-001-a-3980">these</w>
     <w lemma="our" pos="po" xml:id="B05411-001-a-3990">Our</w>
     <w lemma="letter" pos="n2" xml:id="B05411-001-a-4000">Letters</w>
     <pc xml:id="B05411-001-a-4010">,</pc>
     <w lemma="deliver" pos="vvg" xml:id="B05411-001-a-4020">delivering</w>
     <w lemma="they" pos="pno" xml:id="B05411-001-a-4030">them</w>
     <w lemma="by" pos="acp" xml:id="B05411-001-a-4040">by</w>
     <w lemma="you" pos="pn" xml:id="B05411-001-a-4050">you</w>
     <w lemma="due" pos="av-j" reg="duly" xml:id="B05411-001-a-4060">duely</w>
     <w lemma="execute" pos="vvb" xml:id="B05411-001-a-4070">Execute</w>
     <pc xml:id="B05411-001-a-4080">,</pc>
     <w lemma="and" pos="cc" xml:id="B05411-001-a-4090">and</w>
     <w lemma="Indorsed" pos="nn1" xml:id="B05411-001-a-4100">Indorsed</w>
     <w lemma="again" pos="av" xml:id="B05411-001-a-4110">again</w>
     <w lemma="to" pos="acp" xml:id="B05411-001-a-4120">to</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-4130">the</w>
     <w lemma="bearer" pos="n1" xml:id="B05411-001-a-4140">Bearer</w>
     <pc unit="sentence" xml:id="B05411-001-a-4150">.</pc>
    </p>
    <closer xml:id="B05411-e10210">
     <dateline xml:id="B05411-e10220">
      <w lemma="give" pos="vvn" xml:id="B05411-001-a-4160">Given</w>
      <w lemma="under" pos="acp" xml:id="B05411-001-a-4170">under</w>
      <w lemma="our" pos="po" xml:id="B05411-001-a-4180">Our</w>
      <w lemma="signet" pos="n1" xml:id="B05411-001-a-4190">Signet</w>
      <pc xml:id="B05411-001-a-4200">,</pc>
      <date xml:id="B05411-e10230">
       <w lemma="at" pos="acp" xml:id="B05411-001-a-4210">at</w>
       <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B05411-001-a-4220">Edinburgh</w>
       <pc xml:id="B05411-001-a-4230">,</pc>
       <w lemma="the" pos="d" xml:id="B05411-001-a-4240">the</w>
       <w lemma="twenty" pos="ord" xml:id="B05411-001-a-4250">twentieth</w>
       <w lemma="seven" pos="ord" xml:id="B05411-001-a-4260">seventh</w>
       <w lemma="day" pos="n1" xml:id="B05411-001-a-4270">day</w>
       <w lemma="of" pos="acp" xml:id="B05411-001-a-4280">of</w>
       <w lemma="march" pos="n1" rend="hi" xml:id="B05411-001-a-4290">March</w>
       <pc xml:id="B05411-001-a-4300">,</pc>
       <w lemma="one" pos="crd" xml:id="B05411-001-a-4310">one</w>
       <w lemma="thousand" pos="crd" xml:id="B05411-001-a-4320">thousand</w>
       <pc xml:id="B05411-001-a-4330">,</pc>
       <w lemma="six" pos="crd" xml:id="B05411-001-a-4340">six</w>
       <w lemma="hundred" pos="ord" xml:id="B05411-001-a-4350">hundreth</w>
       <w lemma="and" pos="cc" xml:id="B05411-001-a-4360">and</w>
       <w lemma="eighty" pos="crd" xml:id="B05411-001-a-4370">eighty</w>
       <w lemma="four" pos="crd" xml:id="B05411-001-a-4380">four</w>
       <w lemma="year" pos="n2" xml:id="B05411-001-a-4390">Years</w>
       <pc xml:id="B05411-001-a-4400">,</pc>
       <w lemma="and" pos="cc" xml:id="B05411-001-a-4410">and</w>
       <w lemma="of" pos="acp" xml:id="B05411-001-a-4420">of</w>
       <w lemma="our" pos="po" xml:id="B05411-001-a-4430">Our</w>
       <w lemma="reign" pos="n1" xml:id="B05411-001-a-4440">Reign</w>
       <w lemma="the" pos="d" xml:id="B05411-001-a-4450">the</w>
       <w lemma="thirty" pos="ord" xml:id="B05411-001-a-4460">thirtieth</w>
       <w lemma="six" pos="crd" xml:id="B05411-001-a-4470">six</w>
       <w lemma="year" pos="n1" xml:id="B05411-001-a-4480">year</w>
       <pc unit="sentence" xml:id="B05411-001-a-4490">.</pc>
      </date>
     </dateline>
     <signed xml:id="B05411-e10260">
      <w lemma="n/a" pos="fla" xml:id="B05411-001-a-4500">Per</w>
      <w lemma="n/a" pos="fla" xml:id="B05411-001-a-4510">Actum</w>
      <w lemma="n/a" pos="fla" xml:id="B05411-001-a-4520">Dominorum</w>
      <w lemma="n/a" pos="fla" xml:id="B05411-001-a-4530">Secreti</w>
      <w lemma="n/a" pos="fla" xml:id="B05411-001-a-4540">Concilij</w>
      <pc unit="sentence" xml:id="B05411-001-a-4550">.</pc>
      <hi xml:id="B05411-e10270">
       <w lemma="WILL" pos="n1" xml:id="B05411-001-a-4560">WILL</w>
       <pc unit="sentence" xml:id="B05411-001-a-4561">.</pc>
       <w lemma="paterson" pos="nn1" xml:id="B05411-001-a-4570">PATERSON</w>
      </hi>
      <pc rend="follows-hi" xml:id="B05411-001-a-4580">,</pc>
      <w lemma="150" pos="crd" xml:id="B05411-001-a-4590">Cl.</w>
      <w lemma="sti." pos="ab" xml:id="B05411-001-a-4600">Sti.</w>
      <w lemma="n/a" pos="fla" xml:id="B05411-001-a-4610">Concilij</w>
      <pc unit="sentence" xml:id="B05411-001-a-4620">.</pc>
     </signed>
     <lb xml:id="B05411-e10280"/>
     <w lemma="GOD" pos="nn1" xml:id="B05411-001-a-4630">GOD</w>
     <w lemma="save" pos="acp" xml:id="B05411-001-a-4640">Save</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-4650">the</w>
     <w lemma="king" pos="n1" xml:id="B05411-001-a-4660">KING</w>
     <pc unit="sentence" xml:id="B05411-001-a-4670">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="B05411-e10290">
   <div type="colophon" xml:id="B05411-e10300">
    <p xml:id="B05411-e10310">
     <w lemma="Edinburgh" pos="nn1" reg="EDINBURGH" rend="hi" xml:id="B05411-001-a-4680">EDINBVRGH</w>
     <pc xml:id="B05411-001-a-4690">,</pc>
     <w lemma="print" pos="vvn" xml:id="B05411-001-a-4700">Printed</w>
     <w lemma="by" pos="acp" xml:id="B05411-001-a-4710">by</w>
     <w lemma="the" pos="d" xml:id="B05411-001-a-4720">the</w>
     <w lemma="heir" pos="n1" xml:id="B05411-001-a-4730">Heir</w>
     <w lemma="of" pos="acp" xml:id="B05411-001-a-4740">of</w>
     <hi xml:id="B05411-e10330">
      <w lemma="Andrew" pos="nn1" xml:id="B05411-001-a-4750">Andrew</w>
      <w lemma="Anderson" pos="nn1" xml:id="B05411-001-a-4760">Anderson</w>
     </hi>
     <pc rend="follows-hi" xml:id="B05411-001-a-4770">,</pc>
     <w lemma="printer" pos="n1" xml:id="B05411-001-a-4780">Printer</w>
     <w lemma="to" pos="acp" xml:id="B05411-001-a-4790">to</w>
     <w lemma="his" pos="po" xml:id="B05411-001-a-4800">His</w>
     <w lemma="most" pos="avs-d" xml:id="B05411-001-a-4810">Most</w>
     <w lemma="sacred" pos="j" xml:id="B05411-001-a-4820">Sacred</w>
     <w lemma="majesty" pos="n1" xml:id="B05411-001-a-4830">Majesty</w>
     <pc xml:id="B05411-001-a-4840">:</pc>
     <hi xml:id="B05411-e10340">
      <w lemma="n/a" pos="fla" xml:id="B05411-001-a-4850">Anno</w>
      <w lemma="dom." pos="ab" xml:id="B05411-001-a-4860">DOM.</w>
     </hi>
     <w lemma="1684." pos="crd" xml:id="B05411-001-a-4870">1684.</w>
     <pc unit="sentence" xml:id="B05411-001-a-4880"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
